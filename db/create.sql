SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema chatters
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `chatters` ;
CREATE SCHEMA IF NOT EXISTS `chatters`;

-- -----------------------------------------------------
-- Table `chatters`.`grupo_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`grupo_usuario` ;

CREATE TABLE IF NOT EXISTS `chatters`.`grupo_usuario` (
  `id_grupo_usuario` INT NOT NULL AUTO_INCREMENT COMMENT 'ID do grupo de usuários.',
  `nome_grupo_usuario` VARCHAR(45) NULL COMMENT 'Nome do grupo de usuários.',
  PRIMARY KEY (`id_grupo_usuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`escola`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`escola` ;

CREATE TABLE IF NOT EXISTS `chatters`.`escola` (
  `id_escola` INT NOT NULL,
  `nome` VARCHAR(45) NULL,
  `sigla` VARCHAR(45) NULL,
  PRIMARY KEY (`id_escola`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`usuario` ;

CREATE TABLE IF NOT EXISTS `chatters`.`usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT COMMENT 'ID do usuário.',
  `id_grupo_usuario_usuario` INT NULL COMMENT 'ID do grupo ao qual o usuário pertence.',
  `cpf_usuario` VARCHAR(11) NULL COMMENT 'CPF do usuário.',
  `num_matricula_usuario` INT NULL COMMENT 'Número de matrícula do usuário na instituição.',
  `senha_usuario` VARCHAR(255) NULL COMMENT 'Senha do Usuário.',
  `data_hora_criacao_usuario` DATETIME NULL COMMENT 'Data e hora da criação do usuário.',
  `cod_status_usuario` CHAR(1) NULL COMMENT 'Código do status da conta do usuário.',
  `nome_usuario` VARCHAR(45) NULL COMMENT 'Nome de usuário usado para acesso.',
  `email_usuario` VARCHAR(45) NULL COMMENT 'E-mail do usuário usado para acesso.',
  `data_hora_online` DATETIME NULL COMMENT 'Última data e hora que o usuário esteve ativo.',
  `data_reativacao_usuario` DATE NULL COMMENT 'Data da reativação da conta do usuário, se este for desativado for tempo determinado.',
  `cod_recupera_senha` VARCHAR(255) NULL COMMENT 'Código de recuperação de senha.',
  `id_escola` INT NOT NULL,
  PRIMARY KEY (`id_usuario`),
  INDEX `idx_id_grupo_usuario_usuario` (`id_grupo_usuario_usuario` ASC),
  INDEX `fk_usuario_escola1_idx` (`id_escola` ASC),
  CONSTRAINT `fk_id_grupo_usuario_usuario`
    FOREIGN KEY (`id_grupo_usuario_usuario`)
    REFERENCES `chatters`.`grupo_usuario` (`id_grupo_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_escola1`
    FOREIGN KEY (`id_escola`)
    REFERENCES `chatters`.`escola` (`id_escola`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`conversa`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`conversa` ;

CREATE TABLE IF NOT EXISTS `chatters`.`conversa` (
  `id_conversa` INT NOT NULL AUTO_INCREMENT COMMENT 'ID da conversa.',
  `data_hora_conversa` DATETIME NOT NULL COMMENT 'Data e hora da conversa.',
  `id_remetente_conversa` INT NULL COMMENT 'ID do usuário que enviou a mensagem.',
  `id_destinatario_conversa` INT NULL COMMENT 'ID do usuário que recebeu a mensagem.',
  `mensagem_conversa` TEXT NULL COMMENT 'Mensagem ou texto enviado na conversa.',
  `data_hora_visualizacao_conversa` DATETIME NULL COMMENT 'Data e hora em que o destinatário visualizou a conversa.',
  PRIMARY KEY (`id_conversa`, `data_hora_conversa`),
  INDEX `idx_id_remetente_conversa` (`id_remetente_conversa` ASC),
  INDEX `idx_id_destinatario_conversa` (`id_destinatario_conversa` ASC),
  CONSTRAINT `fk_id_remetente_conversa`
    FOREIGN KEY (`id_remetente_conversa`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_destinatario_conversa`
    FOREIGN KEY (`id_destinatario_conversa`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`curso`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`curso` ;

CREATE TABLE IF NOT EXISTS `chatters`.`curso` (
  `id_curso` INT NOT NULL AUTO_INCREMENT COMMENT 'ID do curso',
  `nome_curso` VARCHAR(45) NULL COMMENT 'Nome do curso.',
  `cod_status_curso` CHAR(1) NULL COMMENT 'Código de status do curso.',
  `data_hora_criacao_curso` DATETIME NULL COMMENT 'Data e hora da criação do curso.',
  `id_escola` INT NOT NULL,
  PRIMARY KEY (`id_curso`),
  INDEX `fk_curso_escola1_idx` (`id_escola` ASC),
  CONSTRAINT `fk_curso_escola1`
    FOREIGN KEY (`id_escola`)
    REFERENCES `chatters`.`escola` (`id_escola`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`sala`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`sala` ;

CREATE TABLE IF NOT EXISTS `chatters`.`sala` (
  `id_sala` INT NOT NULL AUTO_INCREMENT COMMENT 'ID da sala.',
  `id_curso_sala` INT NULL COMMENT 'ID do curso a qual a sala pertence.',
  `nome_sala` VARCHAR(45) NULL COMMENT 'Nome da sala.',
  `data_hora_criacao_sala` DATETIME NULL COMMENT 'Data e hora da criação da sala.',
  `url_imagem_sala` VARCHAR(255) NULL COMMENT 'URL da imagem/ícone da sala.',
  `cod_status_sala` CHAR(1) NULL COMMENT 'Código de status da sala.',
  `cod_tipo_sala` CHAR(1) NULL COMMENT 'Código do tipo da sala.',
  PRIMARY KEY (`id_sala`),
  INDEX `idx_id_curso_sala` (`id_curso_sala` ASC),
  CONSTRAINT `fk_id_curso_sala`
    FOREIGN KEY (`id_curso_sala`)
    REFERENCES `chatters`.`curso` (`id_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`publicacao`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`publicacao` ;

CREATE TABLE IF NOT EXISTS `chatters`.`publicacao` (
  `id_publicacao` INT NOT NULL AUTO_INCREMENT COMMENT 'ID da publicação.',
  `id_sala_publicacao` INT NOT NULL COMMENT 'ID da sala a qual a notificação pertence.',
  `mensagem_publicacao` TEXT NULL COMMENT 'Mensagem ou texto da publicação.',
  `data_hora_criacao_publicacao` DATETIME NULL COMMENT 'Data e hora da publicação.',
  `url_arquivo_publicacao` VARCHAR(255) NULL COMMENT 'Link do arquivo anexado a publicação, se ela contiver um arquivo.',
  `id_usuario_publicacao` INT NULL COMMENT 'ID do usuário que publicou.',
  PRIMARY KEY (`id_publicacao`, `id_sala_publicacao`),
  INDEX `idx_id_sala_publicacao` (`id_sala_publicacao` ASC),
  INDEX `idx_id_usuario_publicacao` (`id_usuario_publicacao` ASC),
  CONSTRAINT `fk_id_sala_publicacao`
    FOREIGN KEY (`id_sala_publicacao`)
    REFERENCES `chatters`.`sala` (`id_sala`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_publicacao`
    FOREIGN KEY (`id_usuario_publicacao`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`notificacao_publicacao`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`notificacao_publicacao` ;

CREATE TABLE IF NOT EXISTS `chatters`.`notificacao_publicacao` (
  `id_notificacao_publicacao` INT NOT NULL AUTO_INCREMENT COMMENT 'ID da notificação da publicação.',
  `id_publicacao_notificacao_publicacao` INT NOT NULL COMMENT 'ID da publicação a qual a notificação pertence.',
  `id_usuario_notificacao_publicacao` INT NOT NULL COMMENT 'ID do usuário que recebeu a notificação da publicação.',
  `visualizada_notificacao_publicacao` TINYINT(1) NULL COMMENT 'Verifica se a notificação foi visualizada.',
  `tipo_notificacao_publicacao` CHAR(1) NULL COMMENT 'Tipo da notificação.',
  INDEX `idx_id_usuario_notificacao_publicacao` (`id_usuario_notificacao_publicacao` ASC),
  INDEX `idx_id_publicacao_notificacao_publicacao` (`id_publicacao_notificacao_publicacao` ASC),
  PRIMARY KEY (`id_notificacao_publicacao`, `id_publicacao_notificacao_publicacao`, `id_usuario_notificacao_publicacao`),
  CONSTRAINT `fk_id_usuario_notificacao_publicacao`
    FOREIGN KEY (`id_usuario_notificacao_publicacao`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_publicacao_notificacao_publicacao`
    FOREIGN KEY (`id_publicacao_notificacao_publicacao`)
    REFERENCES `chatters`.`publicacao` (`id_publicacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`curso_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`curso_usuario` ;

CREATE TABLE IF NOT EXISTS `chatters`.`curso_usuario` (
  `id_curso_curso_usuario` INT NOT NULL COMMENT 'ID do curso que pertence ao usuário.',
  `id_usuario_curso_usuario` INT NOT NULL COMMENT 'ID do usuário que pertence ao curso.',
  PRIMARY KEY (`id_curso_curso_usuario`, `id_usuario_curso_usuario`),
  INDEX `idx_id_usuario_curso_usuario` (`id_usuario_curso_usuario` ASC),
  INDEX `idx_id_curso_curso_usuario` (`id_curso_curso_usuario` ASC),
  CONSTRAINT `fk_id_curso_curso_usuario`
    FOREIGN KEY (`id_curso_curso_usuario`)
    REFERENCES `chatters`.`curso` (`id_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_curso_usuario`
    FOREIGN KEY (`id_usuario_curso_usuario`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`sala_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`sala_usuario` ;

CREATE TABLE IF NOT EXISTS `chatters`.`sala_usuario` (
  `id_sala_sala_usuario` INT NOT NULL COMMENT 'ID da sala que pertence ao usuário.',
  `id_usuario_sala_usuario` INT NOT NULL COMMENT 'ID do usuário que participa da sala.',
  `administrador_sala_usuario` TINYINT(1) NULL COMMENT 'Verifica se o usuário é administrador da sala.',
  `notificacoes_sala_usuario` TINYINT(1) NULL COMMENT 'Verifica se o usuário quer receber notificações daquela sala.',
  `criador_sala_usuario` TINYINT(1) NULL COMMENT 'Verifica se o usuário é o criador daquela sala.',
  PRIMARY KEY (`id_sala_sala_usuario`, `id_usuario_sala_usuario`),
  INDEX `idx_id_usuario_sala_usuario` (`id_usuario_sala_usuario` ASC),
  INDEX `idx_id_sala_sala_usuario` (`id_sala_sala_usuario` ASC),
  CONSTRAINT `fk_id_sala_sala_usuario`
    FOREIGN KEY (`id_sala_sala_usuario`)
    REFERENCES `chatters`.`sala` (`id_sala`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_sala_usuario`
    FOREIGN KEY (`id_usuario_sala_usuario`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`comentario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`comentario` ;

CREATE TABLE IF NOT EXISTS `chatters`.`comentario` (
  `id_comentario` INT NOT NULL AUTO_INCREMENT COMMENT 'ID do comentário.',
  `id_publicacao_comentario` INT NOT NULL COMMENT 'ID da publicação a qual o comentário pertence.',
  `mensagem_comentario` TEXT NULL COMMENT 'Mensagem ou texto do comentário.',
  `data_hora_criacao_comentario` DATETIME NULL COMMENT 'Data e hora da publicação do comentário.',
  `id_usuario_comentario` INT NULL COMMENT 'ID do usuário que postou o comentário.',
  PRIMARY KEY (`id_comentario`, `id_publicacao_comentario`),
  INDEX `idx_id_usuario_comentario` (`id_usuario_comentario` ASC),
  INDEX `idx_id_publicacao_comentario` (`id_publicacao_comentario` ASC),
  CONSTRAINT `fk_id_usuario_comentario`
    FOREIGN KEY (`id_usuario_comentario`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_publicacao_comentario`
    FOREIGN KEY (`id_publicacao_comentario`)
    REFERENCES `chatters`.`publicacao` (`id_publicacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`bloqueio_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`bloqueio_usuario` ;

CREATE TABLE IF NOT EXISTS `chatters`.`bloqueio_usuario` (
  `id_usuario_bloqueado_bloqueio_usuario` INT NOT NULL COMMENT 'ID do usuário que foi bloqueado.',
  `id_usuario_bloqueador_bloqueio_usuario` INT NOT NULL COMMENT 'ID do usuário que bloqueou.',
  `cod_status_bloqueio_usuario` CHAR(1) NULL COMMENT 'Código de status do bloqueio.',
  INDEX `idx_id_usuario_bloqueado_bloqueio_usuario` (`id_usuario_bloqueado_bloqueio_usuario` ASC),
  INDEX `idx_id_usuario_bloqueador_bloqueio_usuario` (`id_usuario_bloqueador_bloqueio_usuario` ASC),
  PRIMARY KEY (`id_usuario_bloqueado_bloqueio_usuario`, `id_usuario_bloqueador_bloqueio_usuario`),
  CONSTRAINT `fk_id_usuario_bloqueado_bloqueio_usuario`
    FOREIGN KEY (`id_usuario_bloqueado_bloqueio_usuario`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_bloqueador_bloqueio_usuario`
    FOREIGN KEY (`id_usuario_bloqueador_bloqueio_usuario`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`trabalho_perfil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`trabalho_perfil` ;

CREATE TABLE IF NOT EXISTS `chatters`.`trabalho_perfil` (
  `id_usuario_trabalho_perfil` INT NOT NULL COMMENT 'ID do usuário.',
  `ocupacao_trabalho_perfil` VARCHAR(45) NULL COMMENT 'Nome da ocupação que exerce/exerceu.',
  `empresa_trabalho_perfil` VARCHAR(45) NULL COMMENT 'Nome da empresa que trabalha/trabalhou.',
  `data_inicio_trabalho_perfil` DATE NULL COMMENT 'Data que iniciou os trabalhos.',
  `data_termino_trabalho_perfil` DATE NULL COMMENT 'Data que encerrou os trabalhos.',
  `descricao_trabalho_perfil` TEXT NULL COMMENT 'Descrição do que fazia.',
  INDEX `idx_id_usuario_trabalho_perfil` (`id_usuario_trabalho_perfil` ASC),
  PRIMARY KEY (`id_usuario_trabalho_perfil`),
  CONSTRAINT `fk_id_usuario_trabalho_perfil`
    FOREIGN KEY (`id_usuario_trabalho_perfil`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`idoma_perfil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`idoma_perfil` ;

CREATE TABLE IF NOT EXISTS `chatters`.`idoma_perfil` (
  `id_usuario_idoma_perfil` INT NOT NULL COMMENT 'ID do usuário.',
  `nome_idoma_perfil` VARCHAR(45) NULL COMMENT 'Nome do idioma.',
  `nivel_idioma_perfil` VARCHAR(45) NULL COMMENT 'Nível de conhecimento do idioma.',
  PRIMARY KEY (`id_usuario_idoma_perfil`),
  INDEX `idx_id_usuario_idoma_perfil` (`id_usuario_idoma_perfil` ASC),
  CONSTRAINT `fk_id_usuario_idoma_perfil`
    FOREIGN KEY (`id_usuario_idoma_perfil`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`educacao_perfil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`educacao_perfil` ;

CREATE TABLE IF NOT EXISTS `chatters`.`educacao_perfil` (
  `id_usuario_educacao_perfil` INT NOT NULL COMMENT 'ID do usuário.',
  `curso_educacao_perfil` VARCHAR(45) NULL COMMENT 'Nome do curso que faz/fez.',
  `escola_educacao_perfil` VARCHAR(45) NULL COMMENT 'Nome da escola que estuda/estudou.',
  `data_inicio_educacao_perfil` DATE NULL COMMENT 'Data que iniciou os estudos.',
  `data_termino_educacao_perfil` DATE NULL COMMENT 'Data que encerrou os estudos.',
  INDEX `idx_id_usuario_educacao_perfil` (`id_usuario_educacao_perfil` ASC),
  PRIMARY KEY (`id_usuario_educacao_perfil`),
  CONSTRAINT `fk_id_usuario_educacao_perfil`
    FOREIGN KEY (`id_usuario_educacao_perfil`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`notificacao_comentario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`notificacao_comentario` ;

CREATE TABLE IF NOT EXISTS `chatters`.`notificacao_comentario` (
  `id_notificacao_comentario` INT NOT NULL AUTO_INCREMENT COMMENT 'ID da notificação do comentário.',
  `id_comentario_notificacao_comentario` INT NOT NULL COMMENT 'ID do comentário a qual a notificação pertence.',
  `id_usuario_notificacao_comentario` INT NOT NULL COMMENT 'ID do usuário que recebeu a notificação do comentário.',
  `visualizada_notificacao_comentario` TINYINT(1) NULL COMMENT 'Verifica se a notificação foi visualizada.',
  `tipo_notificacao_comentario` CHAR(1) NULL COMMENT 'Tipo da notificação.',
  INDEX `idx_id_usuario_notificacao_comentario` (`id_usuario_notificacao_comentario` ASC),
  INDEX `idx_id_comentario_notificacao_comentario` (`id_comentario_notificacao_comentario` ASC),
  PRIMARY KEY (`id_notificacao_comentario`, `id_comentario_notificacao_comentario`, `id_usuario_notificacao_comentario`),
  CONSTRAINT `fk_id_usuario_notificacao_comentario`
    FOREIGN KEY (`id_usuario_notificacao_comentario`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_comentario_notificacao_comentario`
    FOREIGN KEY (`id_comentario_notificacao_comentario`)
    REFERENCES `chatters`.`comentario` (`id_comentario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`pais`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`pais` ;

CREATE TABLE IF NOT EXISTS `chatters`.`pais` (
  `id_pais` INT NOT NULL COMMENT 'ID do país.',
  `nome_pais` VARCHAR(125) NULL COMMENT 'Nome do país.',
  `sigla_pais` CHAR(3) NULL COMMENT 'Sigla do país.',
  PRIMARY KEY (`id_pais`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`estado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`estado` ;

CREATE TABLE IF NOT EXISTS `chatters`.`estado` (
  `id_estado` INT NOT NULL COMMENT 'ID do estado.',
  `nome_estado` VARCHAR(25) NULL COMMENT 'Nome do estado.',
  `sigla_estado` CHAR(2) NULL COMMENT 'Sigla do estado.',
  `id_pais_estado` INT NULL COMMENT 'ID do país a qual o estado pertence.',
  PRIMARY KEY (`id_estado`),
  INDEX `idx_id_pais_estado` (`id_pais_estado` ASC),
  CONSTRAINT `fk_id_pais_estado`
    FOREIGN KEY (`id_pais_estado`)
    REFERENCES `chatters`.`pais` (`id_pais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`cidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`cidade` ;

CREATE TABLE IF NOT EXISTS `chatters`.`cidade` (
  `id_cidade` INT NOT NULL COMMENT 'ID da cidade.',
  `nome_cidade` VARCHAR(255) NULL COMMENT 'Nome da cidade.',
  `id_estado_cidade` INT NULL COMMENT 'ID do estado a qual a cidade pertence.',
  PRIMARY KEY (`id_cidade`),
  INDEX `idx_id_estado_cidade` (`id_estado_cidade` ASC),
  CONSTRAINT `fk_id_estado_cidade`
    FOREIGN KEY (`id_estado_cidade`)
    REFERENCES `chatters`.`estado` (`id_estado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`perfil_usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`perfil_usuario` ;

CREATE TABLE IF NOT EXISTS `chatters`.`perfil_usuario` (
  `id_usuario_perfil_usuario` INT NOT NULL COMMENT 'ID do usuário ao qual o perfil pertence.',
  `nome_perfil_usuario` VARCHAR(45) NULL COMMENT 'Nome do usuário no perfil.',
  `sobrenome_perfil_usuario` VARCHAR(45) NULL COMMENT 'Sobrenome do usuário no perfil.',
  `url_imagem_perfil_usuario` VARCHAR(255) NULL COMMENT 'URL da imagem do perfil do usuário.',
  `sexo_perfil_usuario` CHAR(1) NULL COMMENT 'Sexo do usuário.',
  `sobre_perfil_usuario` TEXT NULL COMMENT 'Texto onde o usuário faz um resumo sobre ele.',
  `nascimento_perfil_usuario` DATE NULL COMMENT 'Data de nascimento do usuário.',
  `id_cidade_perfil_usuario` INT NULL COMMENT 'ID da cidade onde o usuário vive.',
  `id_estado_perfil_usuario` INT NULL COMMENT 'ID do estado onde o usuário vive.',
  `id_pais_perfil_usuario` INT NULL COMMENT 'ID do país onde o usuário vive.',
  `email_perfil_usuario` VARCHAR(45) NULL COMMENT 'E-mail do perfil do usuário.',
  `site_perfil_usuario` VARCHAR(45) NULL COMMENT 'Site/portfólio pessoal do perfil do usuário.',
  `apelido_perfil_usuario` VARCHAR(45) NULL COMMENT 'Apelido do perfil do usuário.',
  `ocupacao_perfil_usuario` VARCHAR(45) NULL COMMENT 'Ocupação do perfil do usuário.',
  INDEX `idx_id_cidade_perfil_usuario` (`id_cidade_perfil_usuario` ASC),
  PRIMARY KEY (`id_usuario_perfil_usuario`),
  INDEX `fk_perfil_usuario_estado1_idx` (`id_estado_perfil_usuario` ASC),
  INDEX `fk_perfil_usuario_pais1_idx` (`id_pais_perfil_usuario` ASC),
  CONSTRAINT `fk_id_cidade_perfil_usuario`
    FOREIGN KEY (`id_cidade_perfil_usuario`)
    REFERENCES `chatters`.`cidade` (`id_cidade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_perfil_usuario`
    FOREIGN KEY (`id_usuario_perfil_usuario`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_perfil_usuario_estado1`
    FOREIGN KEY (`id_estado_perfil_usuario`)
    REFERENCES `chatters`.`estado` (`id_estado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_perfil_usuario_pais1`
    FOREIGN KEY (`id_pais_perfil_usuario`)
    REFERENCES `chatters`.`pais` (`id_pais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`historico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`historico` ;

CREATE TABLE IF NOT EXISTS `chatters`.`historico` (
  `id_historico` INT NOT NULL AUTO_INCREMENT COMMENT 'ID do histórico.',
  `data_hora_historico` DATETIME NULL COMMENT 'Data e hora da tarefa.',
  `operacao_historico` VARCHAR(45) NULL COMMENT 'Operação que o usuário realizou.',
  `id_usuario_historico` INT NULL COMMENT 'ID do usuário que realizou a tarefa.',
  `ip_usuario_historico` VARCHAR(45) NULL COMMENT 'IP do usuário que realizou a tarefa.',
  PRIMARY KEY (`id_historico`),
  INDEX `idx_id_usuario_historico` (`id_usuario_historico` ASC),
  CONSTRAINT `fk_id_usuario_historico`
    FOREIGN KEY (`id_usuario_historico`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`notificacao_admin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`notificacao_admin` ;

CREATE TABLE IF NOT EXISTS `chatters`.`notificacao_admin` (
  `id_notificacao_admin` INT NOT NULL AUTO_INCREMENT COMMENT 'ID da notificação para administradores.',
  `id_usuario_notificacao_admin` INT NULL COMMENT 'ID do usuário que recebeu a notificação para administradores.',
  `data_hora_notificacao_admin` DATETIME NULL COMMENT 'Data e hora da notificação para administradores.',
  `mensagem_notificacao_admin` TEXT NULL COMMENT 'Mensagem ou texto da notificação para administradores.',
  `link_post_notificacao_admin` VARCHAR(255) NULL COMMENT 'Link da notificação para administradores, se a notificação estiver associada a algum post.',
  PRIMARY KEY (`id_notificacao_admin`),
  INDEX `idx_id_usuario_notificacao_admin` (`id_usuario_notificacao_admin` ASC),
  CONSTRAINT `fk_id_usuario_notificacao_admin`
    FOREIGN KEY (`id_usuario_notificacao_admin`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chatters`.`notificacao_sala`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chatters`.`notificacao_sala` ;

CREATE TABLE IF NOT EXISTS `chatters`.`notificacao_sala` (
  `id_notificacao_sala` INT NOT NULL AUTO_INCREMENT COMMENT 'ID da notificação.',
  `id_usuario_destinatario_notificacao_sala` INT NOT NULL COMMENT 'ID do usuario que recebeu da notificação.',
  `id_usuario_remetente_notificacao_sala` INT NOT NULL COMMENT 'ID do usuario que gerou a notificação.',
  `id_sala_notificacao_sala` INT NOT NULL COMMENT 'ID da sala da notificação.',
  `tipo_notificacao_sala` CHAR(1) NULL COMMENT 'Tipo da notificação.',
  `visualizada_notificacao_sala` TINYINT(1) NULL COMMENT 'Verifica se a notificação foi visualizada.',
  `data_hora_notificacao_sala` DATETIME NULL COMMENT 'Data e hora da notificação.',
  PRIMARY KEY (`id_notificacao_sala`, `id_usuario_destinatario_notificacao_sala`, `id_sala_notificacao_sala`, `id_usuario_remetente_notificacao_sala`),
  INDEX `fk_notificacao_sala_usuario1_idx` (`id_usuario_destinatario_notificacao_sala` ASC),
  INDEX `fk_notificacao_sala_sala1_idx` (`id_sala_notificacao_sala` ASC),
  INDEX `fk_notificacao_sala_usuario2_idx` (`id_usuario_remetente_notificacao_sala` ASC),
  CONSTRAINT `fk_notificacao_sala_usuario1`
    FOREIGN KEY (`id_usuario_destinatario_notificacao_sala`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_notificacao_sala_sala1`
    FOREIGN KEY (`id_sala_notificacao_sala`)
    REFERENCES `chatters`.`sala` (`id_sala`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_notificacao_sala_usuario2`
    FOREIGN KEY (`id_usuario_remetente_notificacao_sala`)
    REFERENCES `chatters`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

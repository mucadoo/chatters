FROM php:5.6-apache

# Install PDO MySQL extension
RUN docker-php-ext-install pdo_mysql

# Define the custom directory as an environment variable
ENV CUSTOM_DIR /var/www/html

# Change the document root to the custom directory
#RUN sed -i "s|/var/www/html|${CUSTOM_DIR}|g" /etc/apache2/sites-available/000-default.conf
#RUN sed -i "s|/var/www/html|${CUSTOM_DIR}|g" /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Copy your application files to the custom directory
COPY . ${CUSTOM_DIR}

# Ensure the custom directory has the correct permissions
#RUN chown -R www-data:www-data ${CUSTOM_DIR}

# Expose port 80
EXPOSE 80

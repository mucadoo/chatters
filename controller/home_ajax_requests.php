<?php

@header("Content-Type: text/html; charset=UTF-8");
@header("Cache-Control: no-cache, must-revalidate");
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

define("BASE_URL", "http://localhost");

if (isset($_COOKIE["lang"]) && file_exists("../config/lang/" . $_COOKIE['lang'] . ".php")) {
    require_once "../config/lang/" . $_COOKIE['lang'] . ".php";
} else {
    require_once '../config/lang/en.php';
}

require_once 'Bcrypt.class.php';
require_once 'funcoes.php';

require_once 'Conecta.class.php';
$conexao = Conecta::getConexao("../config/bd/geral.ini");

require_once '../model/EscolaDAO.class.php';
$objEscolaDAO = new EscolaDAO();

require_once '../model/UsuarioDAO.class.php';
$objUsuarioDAO = new UsuarioDAO();

if (isset($_POST['login_input'])) {
    $login_input = $_POST['login_input'];
    $senha_input = $_POST['senha_input'];
    $escolas_login = $_POST['escolas_login'];
    $lembrar_senha = isset($_POST['lembrar_senha']) ? 1 : 0;

    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, $escolas_login);

    echo $objUsuarioDAO->autenticarUsuario($conexao, $login_input, $senha_input, $lembrar_senha, $escolas_login);
} else if (isset($_POST['cpf_input'])) {
    $cpf_input = $_POST['cpf_input'];
    $num_reg_input = $_POST['num_reg_input'];
    $pre_senha_input = $_POST['pre_senha_input'];
    $escolas_cadastro = $_POST['escolas_cadastro'];

    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, $escolas_cadastro);

    echo $objUsuarioDAO->verificaCadastro($conexao, $cpf_input, $num_reg_input, $pre_senha_input, $escolas_cadastro);
} else if (isset($_POST['nome_usuario_input'])) {

    $nome_usuario_input = $_POST['nome_usuario_input'];
    $email_input = $_POST['email_input'];
    $escolas_cadastro = decriptNumberCookie($_POST['esc_cad_usuario']);

    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, $escolas_cadastro);

    if ($objUsuarioDAO->checkUsername($conexao, $nome_usuario_input)) {
        echo 'U';
    } else if ($objUsuarioDAO->checkEmail($conexao, $email_input)) {
        echo 'E';
    } else {
        $id_cad_usuario = decriptNumberCookie($_POST['id_cad_usuario']);
        $senha_input = $_POST['senha_input'];
        $nome_input = $_POST['nome_input'];
        $sobrenome_input = $_POST['sobrenome_input'];
        $data_nasc_completa = $_POST['data_nasc_completa'];
        $sexo_radio = $_POST['sexo_radio'];
        $objUsuarioDAO->concCadUser($conexao, $id_cad_usuario, $nome_usuario_input, $email_input, $senha_input, $nome_input, $sobrenome_input, $data_nasc_completa, $sexo_radio);
        echo $_POST['id_cad_usuario'] . ';' . $_POST['esc_cad_usuario'];
    }
} else if (isset($_POST['nome_adm_input'])) {

    $escolas_cadastro = decriptNumberCookie($_POST['esc_cad_usuario']);

    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, $escolas_cadastro);

    $id_cad_usuario = decriptNumberCookie($_POST['id_cad_usuario']);
    $nome_adm_input = $_POST['nome_adm_input'];
    $sobrenome_adm_input = $_POST['sobrenome_adm_input'];
    $sexo_radio_adm = $_POST['sexo_radio_adm'];
    $nova_senha_input = $_POST['nova_senha_input'];
    $objUsuarioDAO->concCadADM($conexao, $id_cad_usuario, $nome_adm_input, $sobrenome_adm_input, $sexo_radio_adm, $nova_senha_input);
    echo $_POST['id_cad_usuario'] . ';' . $_POST['esc_cad_usuario'];
} else if (isset($_POST['nome_contato'])) {

    $nome_contato = $_POST['nome_contato'];
    $email_contato = $_POST['email_contato'];
    $assunto_contato = $_POST['assunto_contato'];
    $msg_contato = $_POST['msg_contato'];
    $msg_contato = "$assunto_contato<br><br>$msg_contato<br><br>$nome_contato - $email_contato";
    echo enviaEmail($nome_contato, $email_contato, $assunto_contato, $msg_contato, 'agenciaadvanced@hotmail.com', 'Agência Advanced');
} else if (isset($_POST['forgot_input'])) {

    $nome_contato = $_POST['forgot_input'];
    $escolas_forgot = $_POST['escolas_forgot'];

    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, $escolas_forgot);

    $check_email = $objUsuarioDAO->checkEmail($conexao, $nome_contato);
    $check_username = $objUsuarioDAO->checkUsername($conexao, $nome_contato);

    if ($check_email) {
        $cod = geraRamdomString();
        $objUsuarioDAO->updForgotCod($conexao, $check_email, $cod);
        $link = BASE_URL . '/?use=' . criptNumberCookie($check_email) . '&esc=' . criptNumberCookie($escolas_forgot) . '&cod=' . $cod;
        $info_user = explode('///', $objUsuarioDAO->forgotInfo($conexao, $check_email));
        $msg_contato = "Olá $info_user[1].<br><br>Foi solicitada uma nova senha.<br>Se você não solicitou isto apenas ignore este email.<br>Se foi você entre no link <a href='$link'>$link</a> e recupere sua senha.<br>Se você recebeu mais de um e-mail deste tipo, considere apenas o último.<br><br>Atenciosamente, Chatters.";
        echo enviaEmail('Agência Advanced', 'agenciaadvanced@hotmail.com', 'Chatters - Recuperação de senha', $msg_contato, $info_user[0], $info_user[1]);
    } else if ($check_username) {
        $cod = geraRamdomString();
        $objUsuarioDAO->updForgotCod($conexao, $check_username, $cod);
        $link = BASE_URL . '/?use=' . criptNumberCookie($check_username) . '&esc=' . criptNumberCookie($escolas_forgot) . '&cod=' . $cod;
        $info_user = explode('///', $objUsuarioDAO->forgotInfo($conexao, $check_username));
        $msg_contato = "Olá $info_user[1].<br><br>Foi solicitada uma nova senha.<br>Se você não solicitou isto apenas ignore este email.<br>Se foi você entre no link <a href='$link'>$link</a> e recupere sua senha.<br>Se você recebeu mais de um e-mail deste tipo, considere apenas o último.<br><br>Atenciosamente, Chatters.";
        echo enviaEmail('Agência Advanced', 'agenciaadvanced@hotmail.com', 'Chatters - Recuperação de senha', $msg_contato, $info_user[0], $info_user[1]);
    } else {
        echo 0;
    }
} else if (isset($_POST['new_pass_input'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_cad_usuario']));

    $id_cad_usuario = decriptNumberCookie($_POST['id_cad_usuario']);
    $new_pass_input = $_POST['new_pass_input'];
    $objUsuarioDAO->resetPass($conexao, $id_cad_usuario, $new_pass_input);
}

?>

<?php

function maxChrString($string, $max) {
    if (strlen($string) > $max) {
        $string = substr($string, 0, $max - 3);
        $string .= '...';
    }
    return $string;
}

function pegaNome($filename) {
    $posicao_ponto = strrpos($filename, "/");
    $nome = substr($filename, ($posicao_ponto + 1));
    return $nome;
}

function pegaExtensao($filename) {
    $posicao_ponto = strrpos($filename, ".");
    $extensao = substr($filename, $posicao_ponto);
    return $extensao;
}

function geraRamdomString() {
    $random_string = "";
    for ($i = 0; $i < 15; $i++) {
        $random_string .= chr(rand(65, 90));
        $random_string .= chr(rand(97, 122));
    }
    return $random_string;
}

function enviaEmail($nome_remetente, $email_remetente, $assunto_contato, $msg_contato, $email_destino, $nome_destino) {
    require_once 'class.phpmailer.php';

    $mail = new PHPMailer();
    $mail->setLanguage(sigla, '../config/lang/');
    $mail->IsSMTP();
    $mail->CharSet = 'UTF-8';
    $mail->Host = "smtp.live.com";
    $mail->SMTPAuth = true;
    $mail->Port = 587;
    $mail->Username = 'agenciaadvanced@hotmail.com';
    $mail->Password = '';
    $mail->SMTPSecure = 'tls';
    $mail->From = $email_remetente;
    $mail->FromName = $nome_remetente;

    $mail->AddAddress($email_destino, $nome_destino);

    $mail->IsHTML(true);
    $mail->Subject = $assunto_contato;
    $mail->Body = $msg_contato;
    //$mail->AltBody = $msg_contato;
    $enviado = $mail->Send();

    $mail->ClearAllRecipients();
    $mail->ClearAttachments();

    if ($enviado) {
        return 'S';
    } else {
        return $mail->ErrorInfo;
    }
}

function criptWordCookie($cookie) {
    $palavra = str_split('?mqnwbevrctxyzuçilokpjahsgdf');
    $tamanho = strlen($cookie);
    $cript = $palavra[$tamanho] . $cookie . $palavra[$tamanho + 2] . $palavra[$tamanho - 1] . $palavra[$tamanho + 1];
    return base64_encode($cript);
}

function criptNumberCookie($cookie) {
    $palavra = str_split('?MQNWBEVRCTXYZUÇILOKPJAHSGDF');
    $tamanho = strlen($cookie);
    $cript = $palavra[$tamanho];
    if ($tamanho % 2 == 0) {
        $cript .= $tamanho + 2;
    } else {
        $cript .= $tamanho + 1;
    }
    $cript .= $cookie . $palavra[$tamanho - 1] . $palavra[$tamanho + 1];
    return base64_encode($cript);
}

function decriptWordCookie($cookie) {
    $decript = base64_decode($cookie);
    $decript_array = str_split($decript);
    $char1 = $decript_array[0];
    $palavra = str_split('?mqnwbevrctxyzuçilokpjahsgdf');
    for ($i = 1; $i < count($palavra); $i++) {
        if ($char1 == $palavra[$i]) {
            break;
        }
    }
    $cookie_final = substr($decript, 1, $i);
    return $cookie_final;
}

function decriptNumberCookie($cookie) {
    $decript = base64_decode($cookie);
    $decript_array = str_split($decript);
    $char1 = $decript_array[0];
    $palavra = str_split('?MQNWBEVRCTXYZUÇILOKPJAHSGDF');
    for ($i = 1; $i < count($palavra); $i++) {
        if ($char1 == $palavra[$i]) {
            break;
        }
    }
    if ($i % 2 == 0) {
        $tamanho = $i + 2;
    } else {
        $tamanho = $i + 1;
    }
    $comeco = strlen($tamanho) + 1;
    $cookie_final = substr($decript, $comeco, $i);
    return $cookie_final;
}

function escolherIconeStatus($letra_status) {
    switch ($letra_status) {
        case 'A':
            $status = "<p style='display:none'>Active</p><img style='cursor:pointer' title='Active' src='media/icones/green_circle.png'>";
            break;
        case 'C':
            $status = "<p style='display:none'>Inactive</p><img style='cursor:pointer' title='Registration Incomplete' src='media/icones/yellow_circle.png'>";
            break;
        case 'I':
            $status = "<p style='display:none'>Inactive</p><img style='cursor:pointer' title='Deactivated' src='media/icones/red_circle.png'>";
            break;
        case 'D':
            $status = "<p style='display:none'>Deactivated</p><img style='cursor:pointer' title='Deactivated' src='media/icones/red_circle.png'>";
            break;
    }
    return $status;
}

function escolherAcaoEditar($tipo, $status) {

    switch ($status) {
        case 'A':
            $status = "CC";
            break;
        case 'C':
            $status = "CI";
            break;
        case 'I':
            $status = "CC";
            break;
        case 'D':
            $status = "CC";
            break;
    }

    $acao = $tipo . $status;

    switch ($acao) {
        case "4CC":
            $cod_acao = $acao;
            break;
        case "4CI":
            $cod_acao = $acao;
            break;
        case "3CC":
            $cod_acao = $acao;
            break;
        case "3CI":
            $cod_acao = $acao;
            break;
        case "2CC":
            $cod_acao = $acao;
            break;
        case "2CI":
            $cod_acao = $acao;
            break;
    }
    return $cod_acao;
}

function locateIp() {

    $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);

    $sigla = substr($hostname, -3);

    if ($sigla == '.aw') {
        return 'en';
    } else if ($sigla == '.af') {
        return 'en';
    } else if ($sigla == '.ao') {
        return 'pt';
    } else if ($sigla == '.al') {
        return 'en';
    } else if ($sigla == '.ad') {
        return 'en';
    } else if ($sigla == '.an') {
        return 'en';
    } else if ($sigla == '.ae') {
        return 'en';
    } else if ($sigla == '.ar') {
        return 'en';
    } else if ($sigla == '.am') {
        return 'en';
    } else if ($sigla == '.as') {
        return 'en';
    } else if ($sigla == '.aq') {
        return 'en';
    } else if ($sigla == '.tf') {
        return 'en';
    } else if ($sigla == '.ag') {
        return 'en';
    } else if ($sigla == '.au') {
        return 'en';
    } else if ($sigla == '.at') {
        return 'en';
    } else if ($sigla == '.az') {
        return 'en';
    } else if ($sigla == '.bi') {
        return 'en';
    } else if ($sigla == '.be') {
        return 'en';
    } else if ($sigla == '.bj') {
        return 'en';
    } else if ($sigla == '.bd') {
        return 'en';
    } else if ($sigla == '.bg') {
        return 'en';
    } else if ($sigla == '.bh') {
        return 'en';
    } else if ($sigla == '.bs') {
        return 'en';
    } else if ($sigla == '.ba') {
        return 'en';
    } else if ($sigla == '.by') {
        return 'en';
    } else if ($sigla == '.bz') {
        return 'en';
    } else if ($sigla == '.bm') {
        return 'en';
    } else if ($sigla == '.bo') {
        return 'en';
    } else if ($sigla == '.br') {
        return 'pt';
    } else if ($sigla == '.bb') {
        return 'en';
    } else if ($sigla == '.bn') {
        return 'en';
    } else if ($sigla == '.bt') {
        return 'en';
    } else if ($sigla == '.bw') {
        return 'en';
    } else if ($sigla == '.cf') {
        return 'en';
    } else if ($sigla == '.ca') {
        return 'en';
    } else if ($sigla == '.ch') {
        return 'en';
    } else if ($sigla == '.cl') {
        return 'en';
    } else if ($sigla == '.cn') {
        return 'en';
    } else if ($sigla == '.ci') {
        return 'en';
    } else if ($sigla == '.cm') {
        return 'en';
    } else if ($sigla == '.cd') {
        return 'en';
    } else if ($sigla == '.cg') {
        return 'en';
    } else if ($sigla == '.ck') {
        return 'en';
    } else if ($sigla == '.co') {
        return 'en';
    } else if ($sigla == '.km') {
        return 'en';
    } else if ($sigla == '.cv') {
        return 'pt';
    } else if ($sigla == '.cr') {
        return 'en';
    } else if ($sigla == '.cu') {
        return 'en';
    } else if ($sigla == '.ky') {
        return 'en';
    } else if ($sigla == '.cy') {
        return 'en';
    } else if ($sigla == '.cz') {
        return 'en';
    } else if ($sigla == '.de') {
        return 'en';
    } else if ($sigla == '.dj') {
        return 'en';
    } else if ($sigla == '.dm') {
        return 'en';
    } else if ($sigla == '.dk') {
        return 'en';
    } else if ($sigla == '.do') {
        return 'en';
    } else if ($sigla == '.dz') {
        return 'en';
    } else if ($sigla == '.ec') {
        return 'en';
    } else if ($sigla == '.eg') {
        return 'en';
    } else if ($sigla == '.er') {
        return 'en';
    } else if ($sigla == '.es') {
        return 'en';
    } else if ($sigla == '.ee') {
        return 'en';
    } else if ($sigla == '.et') {
        return 'en';
    } else if ($sigla == '.fi') {
        return 'en';
    } else if ($sigla == '.fj') {
        return 'en';
    } else if ($sigla == '.fk') {
        return 'en';
    } else if ($sigla == '.fr') {
        return 'en';
    } else if ($sigla == '.fo') {
        return 'en';
    } else if ($sigla == '.fm') {
        return 'en';
    } else if ($sigla == '.ga') {
        return 'en';
    } else if ($sigla == '.gb') {
        return 'en';
    } else if ($sigla == '.ge') {
        return 'en';
    } else if ($sigla == '.gh') {
        return 'en';
    } else if ($sigla == '.gi') {
        return 'en';
    } else if ($sigla == '.gn') {
        return 'en';
    } else if ($sigla == '.gp') {
        return 'en';
    } else if ($sigla == '.gm') {
        return 'en';
    } else if ($sigla == '.gw') {
        return 'pt';
    } else if ($sigla == '.gq') {
        return 'pt';
    } else if ($sigla == '.gr') {
        return 'en';
    } else if ($sigla == '.gd') {
        return 'en';
    } else if ($sigla == '.gl') {
        return 'en';
    } else if ($sigla == '.gt') {
        return 'en';
    } else if ($sigla == '.gf') {
        return 'en';
    } else if ($sigla == '.gu') {
        return 'en';
    } else if ($sigla == '.gy') {
        return 'en';
    } else if ($sigla == '.hk') {
        return 'en';
    } else if ($sigla == '.hn') {
        return 'en';
    } else if ($sigla == '.hr') {
        return 'en';
    } else if ($sigla == '.ht') {
        return 'en';
    } else if ($sigla == '.hu') {
        return 'en';
    } else if ($sigla == '.id') {
        return 'en';
    } else if ($sigla == '.in') {
        return 'en';
    } else if ($sigla == '.io') {
        return 'en';
    } else if ($sigla == '.ie') {
        return 'en';
    } else if ($sigla == '.ir') {
        return 'en';
    } else if ($sigla == '.iq') {
        return 'en';
    } else if ($sigla == '.is') {
        return 'en';
    } else if ($sigla == '.il') {
        return 'en';
    } else if ($sigla == '.it') {
        return 'en';
    } else if ($sigla == '.jm') {
        return 'en';
    } else if ($sigla == '.jo') {
        return 'en';
    } else if ($sigla == '.jp') {
        return 'en';
    } else if ($sigla == '.kz') {
        return 'en';
    } else if ($sigla == '.ke') {
        return 'en';
    } else if ($sigla == '.kg') {
        return 'en';
    } else if ($sigla == '.kh') {
        return 'en';
    } else if ($sigla == '.ki') {
        return 'en';
    } else if ($sigla == '.kn') {
        return 'en';
    } else if ($sigla == '.kr') {
        return 'en';
    } else if ($sigla == '.kw') {
        return 'en';
    } else if ($sigla == '.la') {
        return 'en';
    } else if ($sigla == '.lb') {
        return 'en';
    } else if ($sigla == '.lr') {
        return 'en';
    } else if ($sigla == '.ly') {
        return 'en';
    } else if ($sigla == '.lc') {
        return 'en';
    } else if ($sigla == '.li') {
        return 'en';
    } else if ($sigla == '.lk') {
        return 'en';
    } else if ($sigla == '.ls') {
        return 'en';
    } else if ($sigla == '.lt') {
        return 'en';
    } else if ($sigla == '.lu') {
        return 'en';
    } else if ($sigla == '.lv') {
        return 'en';
    } else if ($sigla == '.mo') {
        return 'pt';
    } else if ($sigla == '.ma') {
        return 'en';
    } else if ($sigla == '.mc') {
        return 'en';
    } else if ($sigla == '.md') {
        return 'en';
    } else if ($sigla == '.mg') {
        return 'en';
    } else if ($sigla == '.mv') {
        return 'en';
    } else if ($sigla == '.mx') {
        return 'en';
    } else if ($sigla == '.mh') {
        return 'en';
    } else if ($sigla == '.mk') {
        return 'en';
    } else if ($sigla == '.ml') {
        return 'en';
    } else if ($sigla == '.mt') {
        return 'en';
    } else if ($sigla == '.mm') {
        return 'en';
    } else if ($sigla == '.mn') {
        return 'en';
    } else if ($sigla == '.mp') {
        return 'en';
    } else if ($sigla == '.mz') {
        return 'pt';
    } else if ($sigla == '.mr') {
        return 'en';
    } else if ($sigla == '.mq') {
        return 'en';
    } else if ($sigla == '.mu') {
        return 'en';
    } else if ($sigla == '.mw') {
        return 'en';
    } else if ($sigla == '.my') {
        return 'en';
    } else if ($sigla == '.yt') {
        return 'en';
    } else if ($sigla == '.na') {
        return 'en';
    } else if ($sigla == '.nc') {
        return 'en';
    } else if ($sigla == '.ne') {
        return 'en';
    } else if ($sigla == '.nf') {
        return 'en';
    } else if ($sigla == '.ng') {
        return 'en';
    } else if ($sigla == '.ni') {
        return 'en';
    } else if ($sigla == '.nl') {
        return 'en';
    } else if ($sigla == '.no') {
        return 'en';
    } else if ($sigla == '.np') {
        return 'en';
    } else if ($sigla == '.nr') {
        return 'en';
    } else if ($sigla == '.nz') {
        return 'en';
    } else if ($sigla == '.om') {
        return 'en';
    } else if ($sigla == '.pk') {
        return 'en';
    } else if ($sigla == '.pa') {
        return 'en';
    } else if ($sigla == '.pe') {
        return 'en';
    } else if ($sigla == '.ph') {
        return 'en';
    } else if ($sigla == '.pw') {
        return 'en';
    } else if ($sigla == '.pg') {
        return 'en';
    } else if ($sigla == '.pl') {
        return 'en';
    } else if ($sigla == '.pr') {
        return 'en';
    } else if ($sigla == '.pt') {
        return 'pt';
    } else if ($sigla == '.py') {
        return 'en';
    } else if ($sigla == '.ps') {
        return 'en';
    } else if ($sigla == '.pf') {
        return 'en';
    } else if ($sigla == '.qa') {
        return 'en';
    } else if ($sigla == '.re') {
        return 'en';
    } else if ($sigla == '.ro') {
        return 'en';
    } else if ($sigla == '.ru') {
        return 'en';
    } else if ($sigla == '.rw') {
        return 'en';
    } else if ($sigla == '.sa') {
        return 'en';
    } else if ($sigla == '.cs') {
        return 'en';
    } else if ($sigla == '.sd') {
        return 'en';
    } else if ($sigla == '.sn') {
        return 'en';
    } else if ($sigla == '.sg') {
        return 'en';
    } else if ($sigla == '.sb') {
        return 'en';
    } else if ($sigla == '.sl') {
        return 'en';
    } else if ($sigla == '.sv') {
        return 'en';
    } else if ($sigla == '.sm') {
        return 'en';
    } else if ($sigla == '.so') {
        return 'en';
    } else if ($sigla == '.st') {
        return 'pt';
    } else if ($sigla == '.sr') {
        return 'en';
    } else if ($sigla == '.sk') {
        return 'en';
    } else if ($sigla == '.si') {
        return 'en';
    } else if ($sigla == '.se') {
        return 'en';
    } else if ($sigla == '.sz') {
        return 'en';
    } else if ($sigla == '.sc') {
        return 'en';
    } else if ($sigla == '.sy') {
        return 'en';
    } else if ($sigla == '.td') {
        return 'en';
    } else if ($sigla == '.tg') {
        return 'en';
    } else if ($sigla == '.th') {
        return 'en';
    } else if ($sigla == '.tj') {
        return 'en';
    } else if ($sigla == '.tk') {
        return 'en';
    } else if ($sigla == '.tm') {
        return 'en';
    } else if ($sigla == '.tl') {
        return 'pt';
    } else if ($sigla == '.to') {
        return 'en';
    } else if ($sigla == '.tt') {
        return 'en';
    } else if ($sigla == '.tn') {
        return 'en';
    } else if ($sigla == '.tr') {
        return 'en';
    } else if ($sigla == '.tv') {
        return 'en';
    } else if ($sigla == '.tw') {
        return 'en';
    } else if ($sigla == '.tz') {
        return 'en';
    } else if ($sigla == '.ug') {
        return 'en';
    } else if ($sigla == '.ua') {
        return 'en';
    } else if ($sigla == '.uy') {
        return 'en';
    } else if ($sigla == '.us') {
        return 'en';
    } else if ($sigla == '.uz') {
        return 'en';
    } else if ($sigla == '.va') {
        return 'en';
    } else if ($sigla == '.vc') {
        return 'en';
    } else if ($sigla == '.ve') {
        return 'en';
    } else if ($sigla == '.vg') {
        return 'en';
    } else if ($sigla == '.vi') {
        return 'en';
    } else if ($sigla == '.vn') {
        return 'en';
    } else if ($sigla == '.vu') {
        return 'en';
    } else if ($sigla == '.ws') {
        return 'en';
    } else if ($sigla == '.ye') {
        return 'en';
    } else if ($sigla == '.za') {
        return 'en';
    } else if ($sigla == '.zm') {
        return 'en';
    } else if ($sigla == '.zw') {
        return 'en';
    } else {
        return FALSE;
    }
}

?>
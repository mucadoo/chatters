<?php

@header("Content-Type: text/html; charset=UTF-8");
@header("Cache-Control: no-cache, must-revalidate");
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

define("BASE_URL", "http://localhost");

if (isset($_COOKIE["lang"]) && file_exists("../config/lang/" . $_COOKIE['lang'] . ".php")) {
    require_once "../config/lang/" . $_COOKIE['lang'] . ".php";
} else {
    require_once '../config/lang/en.php';
}

require_once 'Bcrypt.class.php';
require_once 'funcoes.php';

require_once 'Conecta.class.php';
$conexao = Conecta::getConexao("../config/bd/geral.ini");

require_once '../model/EscolaDAO.class.php';
$objEscolaDAO = new EscolaDAO();

require_once '../model/UsuarioDAO.class.php';
$objUsuarioDAO = new UsuarioDAO();

require_once '../model/SalaDAO.class.php';
$objSalaDAO = new SalaDAO;

require_once '../model/ConversaDAO.class.php';
$objConversaDAO = new ConversaDAO();

require_once '../model/PublicacaoDAO.class.php';
$objPublicacaoDAO = new PublicacaoDAO();

require_once '../model/NotificacaoDAO.class.php';
$objNotificacaoDAO = new NotificacaoDAO();

require_once '../model/LocalDAO.class.php';
$objLocalDAO = new LocalDAO();

require_once '../model/CursoDAO.class.php';
$objCursoDAO = new CursoDAO();

if (isset($_POST['user_logado'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    if ($verifica_escola != FALSE) {
        $verifica_usuario = $objUsuarioDAO->verificaUser($conexao, decriptNumberCookie($_POST['user_logado']));
        if ($verifica_usuario != FALSE) {
            $objUsuarioDAO->updSttsOnline($conexao, decriptNumberCookie($_POST['user_logado']));
            echo $objNotificacaoDAO->selNotifytoHTML($conexao, decriptNumberCookie($_POST['user_logado']));
            echo '///';
            echo $objConversaDAO->selMsgHTML($conexao, decriptNumberCookie($_POST['user_logado']));
        } else {
            echo 0;
        }
    } else {
        echo 0;
    }
} else if (isset($_POST['id_sala'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_sala = ($_POST['id_sala'] - 7);
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    $tamanho = $_POST['tamanho'];
    if ($objSalaDAO->verPartSala($conexao, $id_usuario, $id_sala)) {
        echo $objSalaDAO->selSalaToBox($conexao, $id_sala, $id_usuario, $tamanho);
        echo '///';
        echo $objPublicacaoDAO->selPubs($conexao, $id_usuario, $id_sala);
    } else {
        echo 0;
    }
} else if (isset($_POST['id_user_chat'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    $id_usuario_to = ($_POST['id_user_chat'] - 7);
    echo $objUsuarioDAO->selUserToBox($conexao, $id_usuario_to);
    echo '///';
    echo $objConversaDAO->selConversas($conexao, $id_usuario, $id_usuario_to);
} else if (isset($_POST['mensagem'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $mensagem = utf8_decode($_POST['mensagem']);
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    $id_usuario_to = ($_POST['para_usuario'] - 7);
    $objConversaDAO->insConversa($conexao, $id_usuario, $id_usuario_to, $mensagem);
    echo $objConversaDAO->selConversas($conexao, $id_usuario, $id_usuario_to);
} else if (isset($_POST['usuario_dest'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    $id_usuario_dest = ($_POST['usuario_dest'] - 7);
    echo $objConversaDAO->selConversas($conexao, $id_usuario, $id_usuario_dest);
    echo '///';
    echo $objUsuarioDAO->selUserStatus($conexao, $id_usuario_dest);
} else if (isset($_POST['check_users_online'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_usuario_dest = ($_POST['check_users_online'] - 7);
    echo $objUsuarioDAO->selUserStatus($conexao, $id_usuario_dest);
} else if (isset($_POST['sala_post'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    $id_sala_dest = ($_POST['sala_post'] - 7);
    if ($_POST['post'] != '' && isset($_POST['post'])) {
        $post = utf8_decode($_POST['post']);
    } else {
        $post = NULL;
    }
    if (isset($_POST['extensao'])) {
        $arq[0] = "../upload/$verifica_escola/temp/" . $id_usuario;
        $arq[1] = "../upload/$verifica_escola/publicacao/";
        $arq[2] = utf8_decode($_POST['extensao']);
    } else {
        $arq = NULL;
    }
    $objPublicacaoDAO->insPostSala($conexao, $id_usuario, $id_sala_dest, $post, $arq);
    echo $objPublicacaoDAO->selPubs($conexao, $id_usuario, $id_sala_dest);
} else if (isset($_POST['sala_dest'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    $id_sala_dest = ($_POST['sala_dest'] - 7);
    echo $objPublicacaoDAO->selPubs($conexao, $id_usuario, $id_sala_dest);
} else if (isset($_POST['id_remetente'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    $id_remetente = ($_POST['id_remetente'] - 7);
    $objConversaDAO->updVisConversas($conexao, $id_usuario, $id_remetente);
    echo $objConversaDAO->selMsgHTML($conexao, $id_usuario);
} else if (isset($_POST['id_not'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_not = ($_POST['id_not'] - 7);
    $tipo_not = $_POST['tipo_not'];
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    $objNotificacaoDAO->updStatusNot($conexao, $id_not, $tipo_not);
    echo $objNotificacaoDAO->selNotifytoHTML($conexao, $id_usuario);
} else if (isset($_POST['cod_upd_nots'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_usuario = decriptNumberCookie($_POST['cod_upd_nots']);
    $objNotificacaoDAO->updStatusNots($conexao, $id_usuario);
    echo $objNotificacaoDAO->selNotifytoHTML($conexao, $id_usuario);
} else if (isset($_POST['id_curso_sala'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_curso = $_POST['id_curso_sala'];
    $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    echo $objUsuarioDAO->selUsersToCheck($conexao, decriptNumberCookie($_POST['cod_logado']), $id_curso);
} else if (isset($_POST['nome_sala'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $id_usuario = decriptNumberCookie($_COOKIE['cod_logado']);
    $nome_sala = utf8_decode($_POST['nome_sala']);
    $curso_sala = $_POST['hidden_salas'];
    $tipo_sala = $_POST['tipo_sala_radio'];
    if (isset($_POST['users_add'])) {
        $users_sala = explode('/', $_POST['users_add']);
    } else {
        $users_sala = FALSE;
    }
    $array_icone[0] = "../upload/$verifica_escola/temp/" . $id_usuario;
    $array_icone[1] = "../upload/$verifica_escola/sala/";
    $array_icone[2] = $_POST['extensao'];
    $objSalaDAO->insNewSala($conexao, $id_usuario, $nome_sala, $curso_sala, $tipo_sala, $array_icone, $users_sala);
} else if (isset($_POST['actions_perfil'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    if ($_POST['actions_perfil'] == 1) {
        $id_usuario = decriptNumberCookie($_POST['cod_logado']);
    } else {
        $id_usuario = ($_POST['cod_user_perfil'] - 7);
    }
    echo $objUsuarioDAO->selPerfilUSer($conexao, $id_usuario);
} else if (isset($_POST['cod_sel_perfil'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_usuario = decriptNumberCookie($_POST['cod_sel_perfil']);
    echo $objUsuarioDAO->selPerfilToJson($conexao, $id_usuario);
} else if (isset($_POST['id_pais'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_pais = $_POST['id_pais'];
    echo $objLocalDAO->comboBoxEstados($conexao, $id_pais);
} else if (isset($_POST['id_estado'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $id_estado = $_POST['id_estado'];
    echo $objLocalDAO->comboBoxCidades($conexao, $id_estado);
} else if (isset($_POST['nome_perfil'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $id_usuario = decriptNumberCookie($_COOKIE['cod_logado']);
    $dados['nome'] = utf8_decode($_POST['nome_perfil']);
    $dados['sobrenome'] = utf8_decode($_POST['sobrenome_perfil']);
    $dados['apelido'] = utf8_decode($_POST['apelido_perfil']);
    $dados['sobre'] = utf8_decode($_POST['sobre_perfil']);
    $dados['nascimento'] = $_POST['ano_sel'] . '-' . $_POST['mes_sel'] . '-' . $_POST['dia_sel'];
    $dados['sexo'] = $_POST['sexo_sel'];
    $dados['ocupacao'] = $_POST['ocupacao_perfil'];
    $dados['cidade'] = $_POST['city_sel'] == 0 ? NULL : $_POST['city_sel'];
    $dados['estado'] = $_POST['state_sel'] == 0 ? NULL : $_POST['state_sel'];
    $dados['pais'] = $_POST['pais_sel'] == 0 ? NULL : $_POST['pais_sel'];
    $dados['email'] = $_POST['email_perfil'];
    $dados['site'] = $_POST['site_perfil'];
    if (isset($_POST['extensao'])) {
        $dados['img'][0] = "../upload/$verifica_escola/temp/" . $id_usuario;
        $dados['img'][1] = "../upload/$verifica_escola/usuario/";
        $dados['img'][2] = $_POST['extensao'];
    } else {
        $dados['img'] = FALSE;
    }
    $objUsuarioDAO->updPerfil($conexao, $id_usuario, $dados);
} else if (isset($_POST['foto_nome_user'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    echo $objUsuarioDAO->selNomeFoto($conexao, decriptNumberCookie($_POST['cod_logado']));
} else if (isset($_POST['pesquisa'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    echo $objCursoDAO->pesqUserSala($conexao, decriptNumberCookie($_POST['cod_logado']), utf8_decode($_POST['pesquisa']));
} else if (isset($_POST['upd_salas'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    echo $objSalaDAO->selSalasToList($conexao, decriptNumberCookie($_POST['upd_salas']));
} else if (isset($_POST['quero_sala'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $objSalaDAO->pedidoSala($conexao, decriptNumberCookie($_POST['cod_logado']), ($_POST['quero_sala'] - 7));
} else if (isset($_POST['id_not_pedido'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    $objSalaDAO->addUserSala($conexao, ($_POST['id_not_pedido'] - 7), decriptNumberCookie($_POST['cod_logado']));
} else if (isset($_POST['upd_users'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    echo $objUsuarioDAO->selUsersToList($conexao, decriptNumberCookie($_POST['upd_users']));
} else if (isset($_POST['salas_default'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_POST['esc_logado']));
    echo $objSalaDAO->selSalasDefault($conexao, decriptNumberCookie($_POST['salas_default']));
} else if (isset($_FILES)) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $id_usuario = decriptNumberCookie($_COOKIE['cod_logado']);
    $uploaddir = "../upload/$verifica_escola/temp/";
    if (isset($_GET['sala'])) {
        foreach ($_FILES as $file) {
            $tipo = $file['type'];
            $extensao = utf8_decode(basename($file['name']));
            if ($tipo == 'image/jpeg' || $tipo == 'image/pjpeg' || $tipo == 'image/gif' || $tipo == 'image/png' || $tipo == 'image/x-png' || $tipo == 'application/pdf' || $tipo == 'text/plain' || $tipo == 'text/rtf' || $tipo == 'application/msword' || $tipo == "application/vnd.ms-excel" || $tipo == "application/vnd.ms-powerpoint" || pegaExtensao(basename($file['name'])) == '.docx' || pegaExtensao(basename($file['name'])) == '.pptx' || pegaExtensao(basename($file['name'])) == '.xlsx') {
                if ($file['size'] <= 10000000) {
                    move_uploaded_file($file['tmp_name'], $uploaddir . $id_usuario);
                    $data = $extensao;
                } else {
                    $data = 'S';
                }
            } else {
                $data = 'T';
            }
        }
    } else {
        foreach ($_FILES as $file) {
            $tipo = $file['type'];
            $extensao = pegaExtensao(basename($file['name']));
            if ($tipo == 'image/jpeg' || $tipo == 'image/pjpeg' || $tipo == 'image/gif' || $tipo == 'image/png' || $tipo == 'image/x-png') {
                if ($file['size'] <= 1000000) {
                    move_uploaded_file($file['tmp_name'], $uploaddir . $id_usuario);
                    $data = $extensao;
                } else {
                    $data = 'S';
                }
            } else {
                $data = 'T';
            }
        }
    }
    echo $data;
}
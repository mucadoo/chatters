<?php

@header("Content-Type: text/html; charset=UTF-8");
@header("Cache-Control: no-cache, must-revalidate");
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

define("BASE_URL", "http://localhost");

if (isset($_COOKIE["lang"]) && file_exists("../config/lang/" . $_COOKIE['lang'] . ".php")) {
    require_once "../config/lang/" . $_COOKIE['lang'] . ".php";
} else {
    require_once '../config/lang/en.php';
}

require_once 'Bcrypt.class.php';
require_once 'funcoes.php';

require_once 'Conecta.class.php';
$conexao = Conecta::getConexao("../config/bd/geral.ini");

require_once '../model/EscolaDAO.class.php';
$objEscolaDAO = new EscolaDAO();

require_once '../model/UsuarioDAO.class.php';
$objUsuarioDAO = new UsuarioDAO();

require_once '../model/SalaDAO.class.php';
$objSalaDAO = new SalaDAO();

require_once '../model/CursoDAO.class.php';
$objCursoDAO = new CursoDAO();

if (isset($_POST['del_id'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['del_id'];
    $objUsuarioDAO->delUsuario($conexao, $del_id);
} else if (isset($_POST['atv_id'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['atv_id'];
    $objUsuarioDAO->ativaUsuario($conexao, $del_id);
} else if (isset($_POST['des_id'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['des_id'];
    $objUsuarioDAO->desativaUsuario($conexao, $del_id);
} else if (isset($_POST['del_curso'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['del_curso'];
    $objCursoDAO->delCurso($conexao, $del_id);
} else if (isset($_POST['des_curso'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['des_curso'];
    $objCursoDAO->desCurso($conexao, $del_id);
} else if (isset($_POST['atv_curso'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['atv_curso'];
    $objCursoDAO->atvCurso($conexao, $del_id);
} else if (isset($_POST['del_turma'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['del_turma'];
    $objSalaDAO->delTurma($conexao, $del_id);
} else if (isset($_POST['des_turma'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['des_turma'];
    $objSalaDAO->desTurma($conexao, $del_id);
} else if (isset($_POST['atv_turma'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['atv_turma'];
    $objSalaDAO->atvTurma($conexao, $del_id);
} else if (isset($_POST['des_sala'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['des_sala'];
    $objSalaDAO->desSala($conexao, $del_id);
} else if (isset($_POST['atv_sala'])) {
    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));
    $del_id = $_POST['atv_sala'];
    $objSalaDAO->atvSala($conexao, $del_id);
}
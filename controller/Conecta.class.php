<?php

final class Conecta {

    public static function getConexao($arq, $nome_bd = -1) {

        if (file_exists($arq)) {
            $db = parse_ini_file($arq);
        } else {
            throw new Exception("File not found: $arq");
        }

        $sgbd = isset($db['sgbd']) ? $db['sgbd'] : NULL;
        $host = isset($db['host']) ? $db['host'] : NULL;
        $porta = isset($db['porta']) ? $db['porta'] : NULL;
        $nome_bd = $nome_bd == -1 ? $db['nome_bd'] : $nome_bd;
        $usuario = isset($db['usuario']) ? $db['usuario'] : NULL;
        $senha = isset($db['senha']) ? $db['senha'] : NULL;

        try {
            switch ($sgbd) {
                case 'mysql':
                    $porta = $porta ? $porta : '3306';
                    $conexao = new PDO("mysql:host=$host;port=$porta;dbname=$nome_bd", $usuario, $senha);
                    break;

                case 'mssql':
                    $porta = $porta ? $porta : '1433';
                    $conexao = new PDO("sqlsrv:server=$host:$porta;database=$nome_bd", $usuario, $senha);
                    break;
            }
        } catch (PDOException $e) {
            print_r($e);
        }

        $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $conexao;
    }

}

?>
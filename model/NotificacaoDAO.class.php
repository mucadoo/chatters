<?php

class NotificacaoDAO {

    private static $SEL_NOTIFYS = "SELECT
                                    p.id_sala_publicacao,
                                    p.id_publicacao,
                                    timediff(now(), p.data_hora_criacao_publicacao) as data_hora,
                                    np.visualizada_notificacao_publicacao,
                                    np.tipo_notificacao_publicacao,
                                    pu.nome_perfil_usuario,
                                    pu.sobrenome_perfil_usuario,
                                    pu.url_imagem_perfil_usuario,
                                    s.nome_sala,
                                    np.id_notificacao_publicacao
                                    FROM
                                    publicacao p
                                    JOIN
                                    perfil_usuario pu
                                    JOIN
                                    notificacao_publicacao np
                                    JOIN
                                    sala s
                                    WHERE
                                    s.id_sala = p.id_sala_publicacao
                                    AND
                                    p.id_publicacao = np.id_publicacao_notificacao_publicacao
                                    AND
                                    pu.id_usuario_perfil_usuario = p.id_usuario_publicacao
                                    AND
                                    np.id_usuario_notificacao_publicacao = :id_usuario
                                    UNION ALL
                                    SELECT
                                    p.id_sala_publicacao,
                                    c.id_publicacao_comentario,
                                    timediff(now(), c.data_hora_criacao_comentario) as data_hora,
                                    nc.visualizada_notificacao_comentario,
                                    p.id_usuario_publicacao,
                                    pu.nome_perfil_usuario,
                                    pu.sobrenome_perfil_usuario,
                                    pu.url_imagem_perfil_usuario,
                                    s.nome_sala,
                                    id_notificacao_comentario
                                    FROM
                                    publicacao p
                                    JOIN
                                    comentario c
                                    JOIN
                                    perfil_usuario pu
                                    JOIN
                                    notificacao_comentario nc
                                    JOIN
                                    sala s
                                    WHERE
                                    s.id_sala = p.id_sala_publicacao
                                    AND
                                    c.id_comentario = nc.id_comentario_notificacao_comentario
                                    AND
                                    pu.id_usuario_perfil_usuario = c.id_usuario_comentario
                                    AND
                                    nc.id_usuario_notificacao_comentario = :id_usuario
                                    AND
                                    c.id_publicacao_comentario = p.id_publicacao
                                    UNION ALL
                                    SELECT
                                    s.id_sala,
                                    ns.tipo_notificacao_sala,
                                    timediff(now(), ns.data_hora_notificacao_sala) as data_hora,
                                    ns.visualizada_notificacao_sala,
                                    ns.tipo_notificacao_sala,
                                    pu.nome_perfil_usuario,
                                    pu.sobrenome_perfil_usuario,
                                    pu.url_imagem_perfil_usuario,
                                    s.nome_sala,
                                    ns.id_notificacao_sala
                                    FROM
                                    notificacao_sala ns
                                    JOIN
                                    sala s
                                    JOIN
                                    perfil_usuario pu
                                    WHERE
                                    ns.id_usuario_remetente_notificacao_sala = pu.id_usuario_perfil_usuario
                                    AND
                                    ns.id_usuario_destinatario_notificacao_sala = :id_usuario
                                    AND
                                    ns.id_sala_notificacao_sala = s.id_sala
                                    ORDER BY data_hora";
    private static $UPD_NOT_COM = "UPDATE notificacao_comentario
                                    SET
                                    visualizada_notificacao_comentario = TRUE
                                    WHERE id_comentario_notificacao_comentario = :id_not";
    private static $UPD_NOT_PUB = "UPDATE notificacao_publicacao
                                    SET
                                    visualizada_notificacao_publicacao = TRUE
                                    WHERE id_publicacao_notificacao_publicacao = :id_not";
    private static $UPD_NOT_SALA = "UPDATE notificacao_sala ns
                                    SET
                                    visualizada_notificacao_sala = TRUE
                                    WHERE ns.id_notificacao_sala = :id_not";
    private static $UPD_NOTS = "UPDATE notificacao_publicacao
                                SET
                                visualizada_notificacao_publicacao = TRUE
                                WHERE id_usuario_notificacao_publicacao = :id_usuario;
                                UPDATE notificacao_comentario
                                SET
                                visualizada_notificacao_comentario = TRUE
                                WHERE id_usuario_notificacao_comentario = :id_usuario;
                                UPDATE notificacao_sala ns
                                SET
                                visualizada_notificacao_sala = TRUE
                                WHERE ns.id_usuario_destinatario_notificacao_sala = :id_usuario";

    public function selNotifytoHTML(PDO $conexao, $id_usuario) {
        try {
            $stmtSelNotify = $conexao->prepare(NotificacaoDAO::$SEL_NOTIFYS);
            $stmtSelNotify->execute(array(
                ':id_usuario' => $id_usuario
            ));
            $linhas = $stmtSelNotify->fetchAll();
            $list = "";
            if (empty($linhas)) {
                return "<a class='media list-group-item no-notify'>
                            <span class='media-body block m-b-none'>You don't have any notifications yet!</span>
                        </a>";
            } else {
                foreach ($linhas as $colunas) {
                    $classe3 = "";
                    if ($colunas[4] == 'P') {
                        $tipo_not = $colunas[4];
                        $frase = "posted in the room";
                    } else if ($colunas[4] == 'A') {
                        $tipo_not = $colunas[4];
                        $frase = "added you to the room";
                    } else if ($colunas[4] == 'M') {
                        $tipo_not = $colunas[4];
                        $frase = "asked to join the room";
                        $classe3 = " not-request";
                    } else {
                        $tipo_not = 'C';
                        if ($colunas[4] == $id_usuario) {
                            $frase = "commented on your post in the room";
                        } else {
                            $frase = "also commented on a post in the room";
                        }
                    }
                    if ($colunas[3] == FALSE) {
                        $classe = " not-no-vis";
                    } else {
                        $classe = "";
                    }
                    if ($colunas[2] <= '00:00:05') {
                        $classe2 = " not-alert";
                    } else {
                        $classe2 = "";
                    }
                    $list .= "<a href='" . ($colunas[0] + 7) . "/" . ($colunas[1] + 7) . "/" . ($colunas[9] + 7) . "/$tipo_not' class='media list-group-item$classe$classe2$classe3'>
                            <span class='pull-left thumb-small'>
                                <img src='$colunas[7]' alt='John said' class='img-circle'>
                            </span>
                            <span class='media-body block m-b-none'>
                                $colunas[5] $colunas[6] $frase $colunas[8]
                                <br>
                                <small class='text-muted'>$colunas[2]</small>
                            </span>
                        </a>";
                }
                return $list;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function updStatusNot(PDO $conexao, $id_not, $tipo_not) {
        try {
            if ($tipo_not == 'P') {
                $stmt = NotificacaoDAO::$UPD_NOT_PUB;
            } else if ($tipo_not == 'A') {
                $stmt = NotificacaoDAO::$UPD_NOT_SALA;
            } else {
                $stmt = NotificacaoDAO::$UPD_NOT_COM;
            }
            $stmtUpdNot = $conexao->prepare($stmt);
            $stmtUpdNot->execute(array(
                ':id_not' => $id_not
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function updStatusNots(PDO $conexao, $id_usuario) {
        try {
            $stmtUpdNot = $conexao->prepare(NotificacaoDAO::$UPD_NOTS);
            $stmtUpdNot->execute(array(
                ':id_usuario' => $id_usuario
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

}

?>
<?php

class ComentarioDAO {

    private static $INS_COMMENT = "INSERT INTO comentario
                                    (id_comentario,
                                    id_publicacao_comentario,
                                    mensagem_comentario,
                                    data_hora_criacao_comentario,
                                    id_usuario_comentario)
                                    VALUES
                                    (NULL,
                                    :id_publicacao,
                                    :mensagem_comentario,
                                    now(),
                                    :id_usuario)";
    private static $SEL_USERS_NOTIFY = "SELECT
                                        p.id_usuario_publicacao as id_user
                                        FROM
                                        publicacao p
                                        WHERE
                                        p.id_publicacao = :id_publicacao
                                        AND
                                        p.id_usuario_publicacao NOT IN(:id_usuario)
                                        UNION ALL
                                        SELECT
                                        c.id_usuario_comentario as id_user
                                        FROM
                                        publicacao p,
                                        comentario c
                                        WHERE
                                        p.id_publicacao = c.id_publicacao_comentario
                                        AND
                                        p.id_publicacao = :id_publicacao
                                        AND
                                        c.id_usuario_comentario NOT IN(:id_usuario)";
    private static $SEL_PK_COMMENT = "SELECT
                                    last_insert_id() as ultimaPK
                                    FROM
                                    comentario
                                    limit 0,1";
    private static $INS_NOTIFY = "INSERT INTO
                                        notificacao_comentario
                                        VALUES
                                        (NULL,
                                        :id_comentario,
                                        :id_usuario,
                                        0,
                                        'C')";

    public function insComment(PDO $conexao, $id_usuario, $id_pub, $comentario) {
        try {
            $stmtInsPub = $conexao->prepare(ComentarioDAO::$INS_COMMENT);
            $stmtInsPub->execute(array(
                ':id_publicacao' => $id_pub,
                ':mensagem_comentario' => $comentario,
                ':id_usuario' => $id_usuario
            ));

            $stmtSelUsers = $conexao->prepare(ComentarioDAO::$SEL_USERS_NOTIFY);
            $stmtSelUsers->execute(array(
                ':id_publicacao' => $id_pub,
                ':id_usuario' => $id_usuario
            ));
            $usuarios = $stmtSelUsers->fetchAll();

            $i = 0;
            foreach ($usuarios as $usuario) {
                $usuarios_to_notify[$i] = $usuario[0];
                $i++;
            }
            $usuarios_to_notify = array_unique($usuarios_to_notify);

            $stmtSelPk = $conexao->query(ComentarioDAO::$SEL_PK_COMMENT);
            $resultSelPK = $stmtSelPk->fetch(PDO::FETCH_OBJ);
            $id_comentario = $resultSelPK->ultimaPK;

            foreach ($usuarios_to_notify as $usuario) {
                $stmtInsNot = $conexao->prepare(ComentarioDAO::$INS_NOTIFY);
                $stmtInsNot->execute(array(
                    ':id_comentario' => $id_comentario,
                    ':id_usuario' => $usuario
                ));
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

}

?>
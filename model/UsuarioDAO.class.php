<?php

class UsuarioDAO {

    private static $UPD_IMG = "UPDATE
                                perfil_usuario
                                SET
                                url_imagem_perfil_usuario = :img
                                WHERE id_usuario_perfil_usuario = :id_usuario";
    private static $UPD_PERFIL = "UPDATE perfil_usuario
                                    SET
                                    nome_perfil_usuario = :nome,
                                    sobrenome_perfil_usuario = :sobrenome,
                                    sexo_perfil_usuario = :sexo,
                                    sobre_perfil_usuario = :sobre,
                                    nascimento_perfil_usuario = :nascimento,
                                    id_cidade_perfil_usuario = :cidade,
                                    id_estado_perfil_usuario = :estado,
                                    id_pais_perfil_usuario = :pais,
                                    email_perfil_usuario = :email,
                                    site_perfil_usuario = :site,
                                    apelido_perfil_usuario = :apelido,
                                    ocupacao_perfil_usuario = :ocupacao
                                    WHERE id_usuario_perfil_usuario = :id_usuario";
    private static $SEL_COUNTRY_PERFIL = "SELECT
                                            p.nome_pais
                                            FROM
                                            pais p
                                            WHERE
                                            p.id_pais = :id_pais";
    private static $SEL_STATE_PERFIL = "SELECT
                                        e.sigla_estado
                                        FROM
                                        estado e
                                        WHERE
                                        e.id_estado = :id_estado";
    private static $SEL_CITY_PERFIL = "SELECT
                                        c.nome_cidade
                                        FROM
                                        cidade c
                                        WHERE
                                        c.id_cidade = :id_cidade";
    private static $SEL_PERFIL_USER = "SELECT
                                        pu.apelido_perfil_usuario,
                                        pu.email_perfil_usuario,
                                        pu.id_cidade_perfil_usuario,
                                        pu.id_estado_perfil_usuario,
                                        pu.id_pais_perfil_usuario,
                                        pu.nascimento_perfil_usuario,
                                        pu.nome_perfil_usuario,
                                        pu.sexo_perfil_usuario,
                                        pu.site_perfil_usuario,
                                        pu.sobre_perfil_usuario,
                                        pu.sobrenome_perfil_usuario,
                                        pu.url_imagem_perfil_usuario,
                                        pu.ocupacao_perfil_usuario
                                        FROM
                                        perfil_usuario pu
                                        WHERE
                                        pu.id_usuario_perfil_usuario = :id_usuario";
    private static $SEL_USERS_CURSO = "SELECT
                                        pu.id_usuario_perfil_usuario,
                                        pu.nome_perfil_usuario,
                                        pu.sobrenome_perfil_usuario,
                                        pu.url_imagem_perfil_usuario
                                        FROM
                                        perfil_usuario pu
                                        JOIN
                                        curso_usuario cu
                                        JOIN
                                        usuario u
                                        WHERE
                                        pu.id_usuario_perfil_usuario = u.id_usuario
                                        AND
                                        pu.id_usuario_perfil_usuario = cu.id_usuario_curso_usuario
                                        AND
                                        pu.id_usuario_perfil_usuario NOT IN(:id_usuario)
                                        AND
                                        cu.id_curso_curso_usuario = :id_curso
                                        AND
                                        u.cod_status_usuario NOT IN('C')";
    private static $SEL_USER_ONLINE = "SELECT
                                        timediff(now(), u.data_hora_online) as diferenca
                                        FROM
                                        usuario u
                                        WHERE
                                        id_usuario = :id_usuario";
    private static $SEL_CHAT_BOX = "SELECT
                                    pu.nome_perfil_usuario,
                                    pu.sobrenome_perfil_usuario,
                                    pu.url_imagem_perfil_usuario,
                                    timediff(now(), u.data_hora_online) as diferenca
                                    FROM
                                    perfil_usuario pu
                                    JOIN
                                    usuario u
                                    WHERE
                                    pu.id_usuario_perfil_usuario = u.id_usuario
                                    AND
                                    pu.id_usuario_perfil_usuario = :id_usuario";
    private static $SEL_AMIGOS_LIST = "SELECT
                                        u.id_usuario,
                                        u.id_grupo_usuario_usuario,
                                        pu.nome_perfil_usuario,
                                        pu.sobrenome_perfil_usuario,
                                        pu.url_imagem_perfil_usuario,
                                        timediff(now(), u.data_hora_online) as diferenca
                                        FROM
                                        usuario u
                                        JOIN
                                        perfil_usuario pu
                                        WHERE
                                        u.id_usuario = pu.id_usuario_perfil_usuario
                                        AND
                                        u.id_usuario = :id_usuario";
    private static $SEL_CURSOS_USER = "SELECT
                                        cu.id_curso_curso_usuario
                                        FROM
                                        curso_usuario cu
                                        WHERE
                                        cu.id_usuario_curso_usuario = :id_usuario";
    private static $SEL_AMIGOS = "SELECT
                                    cu.id_usuario_curso_usuario
                                    FROM
                                    curso_usuario cu
                                    JOIN
                                    usuario u
                                    WHERE
                                    u.id_usuario = cu.id_usuario_curso_usuario
                                    AND
                                    u.id_usuario NOT IN (:id_usuario)
                                    AND
                                    cu.id_curso_curso_usuario = :id_curso
                                    AND
                                    u.cod_status_usuario NOT IN('C')";
    private static $SEL_NOME_FOTO = "SELECT
                                    pu.nome_perfil_usuario,
                                    pu.sobrenome_perfil_usuario,
                                    pu.url_imagem_perfil_usuario
                                    FROM
                                    perfil_usuario pu
                                    WHERE
                                    pu.id_usuario_perfil_usuario = :id_usuario";
    private static $UPD_USER_ONLINE = "UPDATE usuario u
                                        SET
                                        u.data_hora_online = now()
                                        WHERE u.id_usuario = :id_usuario";
    private static $UPD_RESET_PASS = "UPDATE
                                    usuario u
                                    SET
                                    u.cod_recupera_senha = '',
                                    u.senha_usuario = :senha_usuario
                                    WHERE
                                    u.id_usuario = :id_usuario";
    private static $UPD_FOG_COD = "UPDATE
                                    usuario
                                    SET
                                    cod_recupera_senha = :cod_recupera_senha
                                    WHERE id_usuario = :id_usuario";
    private static $SEL_FORG_COD = "SELECT
                                    u.cod_recupera_senha,
                                    count(u.id_usuario) as total
                                    FROM
                                    usuario u
                                    WHERE
                                    u.id_usuario = :id_usuario";
    private static $SEL_FORG_INFO = "SELECT
                                    email_usuario,
                                    nome_perfil_usuario
                                    FROM usuario
                                    JOIN perfil_usuario
                                    ON (id_usuario = :id_usuario
                                    AND
                                    id_usuario_perfil_usuario = :id_usuario_perfil_usuario)";
    private static $INS_UPD_CAD_ADM = "UPDATE usuario
                                        SET
                                        senha_usuario = :senha_usuario,
                                        cod_status_usuario = 'A'
                                        WHERE id_usuario = :id_usuario;
                                        UPDATE perfil_usuario
                                        SET
                                        id_usuario_perfil_usuario = :id_usuario_perfil_usuario,
                                        nome_perfil_usuario = :nome_perfil_usuario,
                                        sobrenome_perfil_usuario = :sobrenome_perfil_usuario,
                                        sexo_perfil_usuario = :sexo_perfil_usuario,
                                        url_imagem_perfil_usuario = 'media/avatar.png'
                                        WHERE id_usuario_perfil_usuario = :id_usuario_perfil_usuario";
    private static $INS_UPD_CAD_USER = "UPDATE usuario
                                        SET
                                        senha_usuario = :senha_usuario,
                                        cod_status_usuario = 'A',
                                        nome_usuario = :nome_usuario,
                                        email_usuario = :email_usuario
                                        WHERE id_usuario = :id_usuario;
                                        UPDATE perfil_usuario
                                        SET
                                        id_usuario_perfil_usuario = :id_usuario_perfil_usuario,
                                        nome_perfil_usuario = :nome_perfil_usuario,
                                        sobrenome_perfil_usuario = :sobrenome_perfil_usuario,
                                        sexo_perfil_usuario = :sexo_perfil_usuario,
                                        nascimento_perfil_usuario = :nascimento_perfil_usuario,
                                        url_imagem_perfil_usuario = 'media/avatar.png'
                                        WHERE id_usuario_perfil_usuario = :id_usuario_perfil_usuario";
    private static $SEL_CHECK_EMAIL = "SELECT
                                        id_usuario,
                                        count(u.id_usuario) as total
                                        FROM
                                        usuario u
                                        WHERE
                                        email_usuario = :email_usuario";
    private static $SEL_CHECK_USER = "SELECT
                                    id_usuario,
                                    count(u.id_usuario) as total
                                    FROM
                                    usuario u
                                    WHERE
                                    nome_usuario = :nome_usuario";
    private static $SEL_CADASTRO = "SELECT
                                    u.id_usuario,
                                    count(u.id_usuario) as total
                                    FROM
                                    usuario u
                                    WHERE
                                    cpf_usuario = :cpf_usuario
                                    AND
                                    num_matricula_usuario = :num_matricula_usuario
                                    AND
                                    senha_usuario = :pre_senha_usuario
                                    AND
                                    cod_status_usuario = 'C'";
    private static $UPD_REATIVA = "UPDATE usuario u
                                    SET
                                    u.cod_status_usuario = 'A'
                                    WHERE u.id_usuario = :id_usuario";
    private static $COOKIE_ALT = "select
                                    u.id_grupo_usuario_usuario,
                                    u.cod_status_usuario,
                                    count(u.id_usuario) as total
                                from
                                   usuario u
                                where
                                    u.id_usuario = :id_usuario";
    private static $AUT_USUARIO = "select
                                    u.id_usuario,
                                    u.id_grupo_usuario_usuario,
                                    u.cod_status_usuario,
                                    u.data_reativacao_usuario,
                                    u.senha_usuario,
                                    count(u.id_usuario) as total
                                from
                                   usuario u
                                where
                                    u.nome_usuario = :nome_usuario
                                    OR
                                    u.email_usuario = :email_usuario";
    private static $SEL_USUARIO = "select
                                        id_usuario,
                                        nome_usuario,
                                        num_matricula_usuario,
                                        email_usuario,
                                        cod_status_usuario
                                    from
                                        usuario u,
                                        grupo_usuario g
                                    where
                                        u.id_grupo_usuario_usuario = g.id_grupo_usuario
                                        AND
                                        g.id_grupo_usuario = :id_grupo_usuario";
    private static $SEL_CURSOS_USUARIO = "select
                                            nome_curso
                                        from
                                            curso c,
                                            curso_usuario cu
                                        where
                                            c.id_curso = cu.id_curso_curso_usuario
                                            AND
                                            cu.id_usuario_curso_usuario = :id_usuario";
    private static $PRE_CAD_USUARIO = "INSERT INTO usuario
                                                (id_usuario,
                                                id_grupo_usuario_usuario,
                                                cpf_usuario,
                                                num_matricula_usuario,
                                                senha_usuario,
                                                data_hora_criacao_usuario,
                                                cod_status_usuario,
                                                id_escola)
                                                VALUES
                                                (NULL,
                                                :tipo_usuario,
                                                :cpf_usuario,
                                                :num_matricula_usuario,
                                                :senha_usuario,
                                                now(),
                                                'C',
                                                :id_escola)";
    private static $PRE_CAD_ADM_USUARIO = "INSERT INTO usuario
                                            (id_usuario,
                                            id_grupo_usuario_usuario,
                                            senha_usuario,
                                            data_hora_criacao_usuario,
                                            cod_status_usuario,
                                            nome_usuario,
                                            email_usuario,
                                            id_escola)
                                            VALUES
                                            (NULL,
                                            '2',
                                            :senha_usuario,
                                            now(),
                                            'C',
                                            :nome_usuario,
                                            :email_usuario,
                                            :id_escola)";
    private static $PRE_CAD_PERFIL = "INSERT INTO perfil_usuario
                                            (`id_usuario_perfil_usuario`)
                                            VALUES
                                            (:id_usuario_perfil_usuario);";
    private static $SEL_SALAS_ALUNO = "SELECT
                                        s.id_sala
                                        FROM
                                        sala s
                                        WHERE
                                        s.cod_tipo_sala IN('S', 'B', 'G')
                                        AND
                                        s.id_curso_sala = :id_curso";
    private static $SEL_SALAS_PROF = "SELECT
                                        s.id_sala
                                        FROM
                                        sala s
                                        WHERE
                                        s.id_curso_sala = :id_curso
                                        AND
                                        s.cod_tipo_sala IN('S', 'B', 'G', 'P')";
    private static $PRE_CAD_ALUNO_SALA = "INSERT INTO sala_usuario
                                            (id_sala_sala_usuario,
                                            id_usuario_sala_usuario,
                                            administrador_sala_usuario,
                                            notificacoes_sala_usuario,
                                            criador_sala_usuario)
                                            VALUES
                                            (:id_sala_sala_usuario,
                                            :id_usuario_sala_usuario,
                                            FALSE,
                                            TRUE,
                                            FALSE)";
    private static $PRE_CAD_PROF_SALA = "INSERT INTO sala_usuario
                                            (id_sala_sala_usuario,
                                            id_usuario_sala_usuario,
                                            administrador_sala_usuario,
                                            notificacoes_sala_usuario,
                                            criador_sala_usuario)
                                            VALUES
                                            (:id_sala_sala_usuario,
                                            :id_usuario_sala_usuario,
                                            TRUE,
                                            TRUE,
                                            FALSE)";
    private static $PRE_CAD_CURSO = "INSERT INTO curso_usuario
                                            (`id_curso_curso_usuario`,
                                            `id_usuario_curso_usuario`)
                                            VALUES
                                            (:id_curso_curso_usuario,
                                            :id_usuario_curso_usuario);";
    private static $SEL_PK_USUARIO = "select last_insert_id() as ultimaPK from usuario limit 0,1";
    private static $DEL_USUARIO = "DELETE FROM curso_usuario
                                    WHERE id_usuario_curso_usuario = :id_usuario;
                                    DELETE FROM sala_usuario
                                    WHERE id_usuario_sala_usuario = :id_usuario;
                                    DELETE FROM perfil_usuario
                                    WHERE id_usuario_perfil_usuario = :id_usuario;
                                    DELETE FROM usuario
                                    WHERE id_usuario = :id_usuario";
    private static $DESATIVA_USUARIO = "UPDATE usuario
                                    SET
                                    cod_status_usuario = 'D'
                                    WHERE id_usuario = :id_usuario";
    private static $ATIVA_USUARIO = "UPDATE usuario
                                    SET
                                    cod_status_usuario = 'A'
                                    WHERE id_usuario = :id_usuario";

    public function selectUsuarioToHTML(PDO $conexao, $tipo_usuario) {
        try {

            switch ($tipo_usuario) {
                case 4:
                    $stmtSel = $conexao->prepare(UsuarioDAO::$SEL_USUARIO);
                    $stmtSel->execute(array(':id_grupo_usuario' => $tipo_usuario));
                    $numLinhas = $stmtSel->rowCount();
                    if ($numLinhas == 0) {
                        return "<thead style='display: none'>
                                    <tr>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th class='tabela_aluno_vazia'>No Registered Student Exists!</th>
                                    </tr>
                                </tbody>";
                    } else {
                        $tabela = "<thead>
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>Registration Number</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Course</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>";
                        $linha = $stmtSel->fetchAll();
                        foreach ($linha as $coluna) {
                            $stmtSelCursos = $conexao->prepare(UsuarioDAO::$SEL_CURSOS_USUARIO);
                            $stmtSelCursos->execute(array(':id_usuario' => $coluna[0]));
                            $linha1 = $stmtSelCursos->fetchAll();
                            $status = escolherIconeStatus($coluna[4]);
                            $cod_acao = escolherAcaoEditar($tipo_usuario, $coluna[4]);
                            if ($coluna[4] == 'C') {
                                $class = "delete_user";
                                $img = "<img title='Delete User' src='media/icones/delete.png'>";
                            } else if ($coluna[4] == 'A') {
                                $class = "deactivate_user";
                                $img = "<img title='Deactivate User' src='media/icones/red_stop.png'>";
                            } else {
                                $class = "activate_user";
                                $img = "<img title='Activate User' src='media/icones/green_check.png'>";
                            }
                            $tabela .= " <tr>
                            <td>$coluna[0]</td>
                            <td>$coluna[1]</td>
                            <td>$coluna[2]</td>
                            <td>$coluna[3]</td>
                            <td style='text-align:center'>$status</td>
                            <td>" . $linha1[0][0] . "</td>
                            <td style='text-align:center'>
                                <!--<a href='index.php?page=usuarios&acao=$cod_acao&id=$coluna[0]'><img title='Edit User' src='media/icones/rewrite.png'></a>-->
                                <a style='margin-left:7px' class='$class' id='$coluna[0]'>$img</a>
                            </td>
                        </tr>";
                        }
                        $tabela .= "</tbody>";
                        return $tabela;
                    }
                    break;

                case 3:
                    $stmtSel = $conexao->prepare(UsuarioDAO::$SEL_USUARIO);
                    $stmtSel->execute(array(':id_grupo_usuario' => $tipo_usuario));
                    $numLinhas = $stmtSel->rowCount();
                    if ($numLinhas == 0) {
                        return "<thead style='display: none'>
                                    <tr>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th class='tabela_prof_vazia'>No Registered Teacher Exists!</th>
                                    </tr>
                                </tbody>";
                    } else {
                        $tabela = "<thead>
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>Registration Number</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Courses</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>";
                        $linha = $stmtSel->fetchAll();
                        foreach ($linha as $coluna) {
                            $stmtSelCursos = $conexao->prepare(UsuarioDAO::$SEL_CURSOS_USUARIO);
                            $stmtSelCursos->execute(
                                    array(
                                        ':id_usuario' => $coluna[0]
                                    )
                            );
                            $nroLinhas = $stmtSelCursos->rowCount();
                            if ($nroLinhas == 0) {
                                $cursos = "";
                            } else {
                                $linha1 = $stmtSelCursos->fetchAll();
                                if (!isset($linha1[1])) {
                                    $cursos = $linha1[0][0];
                                } else if (isset($linha1[2])) {
                                    $cursos = $linha1[0][0] . ', ' . $linha1[1][0] . ' and ' . $linha1[2][0];
                                } else {
                                    $cursos = $linha1[0][0] . ' and ' . $linha1[1][0];
                                }
                            }
                            $status = escolherIconeStatus($coluna[4]);
                            $cod_acao = escolherAcaoEditar($tipo_usuario, $coluna[4]);
                            if ($coluna[4] == 'C') {
                                $class = "delete_user";
                                $img = "<img title='Delete User' src='media/icones/delete.png'>";
                            } else if ($coluna[4] == 'A') {
                                $class = "deactivate_user";
                                $img = "<img title='Deactivate User' src='media/icones/red_stop.png'>";
                            } else {
                                $class = "activate_user";
                                $img = "<img title='Activate User' src='media/icones/green_check.png'>";
                            }
                            $tabela .= " <tr>
                            <td>$coluna[0]</td>
                            <td>$coluna[1]</td>
                            <td>$coluna[2]</td>
                            <td>$coluna[3]</td>
                            <td style='text-align:center'>$status</td>
                            <td>$cursos</td>
                            <td style='text-align:center'>
                                <!--<a href='index.php?page=usuarios&acao=$cod_acao&id=$coluna[0]'><img title='Edit User' src='media/icones/rewrite.png'></a>-->
                                <a style='margin-left:7px' class='$class' id='$coluna[0]'>$img</a>
                            </td>
                        </tr>";
                        }
                        $tabela .= "</tbody>";
                        return $tabela;
                    }
                    break;

                case 2:
                    $stmtSel = $conexao->prepare(UsuarioDAO::$SEL_USUARIO);
                    $stmtSel->execute(array(':id_grupo_usuario' => $tipo_usuario));
                    $numLinhas = $stmtSel->rowCount();
                    if ($numLinhas == 0) {
                        return "<thead style='display: none'>
                                    <tr>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th class='tabela_adm_vazia'>No Registered Administrator Exists!</th>
                                    </tr>
                                </tbody>";
                    } else {
                        $tabela = "<thead>
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>";
                        $linha = $stmtSel->fetchAll();
                        foreach ($linha as $coluna) {
                            $status = escolherIconeStatus($coluna[4]);
                            $cod_acao = escolherAcaoEditar($tipo_usuario, $coluna[4]);
                            if ($coluna[4] == 'C') {
                                $class = "excluir_usuario";
                                $img = "<img title='Delete User' src='media/icones/delete.png'>";
                            } else if ($coluna[4] == 'A') {
                                $class = "desativar_usuario";
                                $img = "<img title='Deactivate User' src='media/icones/red_stop.png'>";
                            } else {
                                $class = "ativar_usuario";
                                $img = "<img title='Activate User' src='media/icones/green_check.png'>";
                            }
                            $tabela .= " <tr>
                            <td>$coluna[0]</td>
                            <td>$coluna[1]</td>
                            <td>$coluna[3]</td>
                            <td style='text-align:center'>$status</td>
                            <td style='text-align:center'>
                                <!--<a href='index.php?page=usuarios&acao=$cod_acao&id=$coluna[0]'><img title='Edit User' src='media/icones/rewrite.png'></a>-->
                                <a style='margin-left:7px' class='$class' id='$coluna[0]'>$img</a>
                            </td>
                        </tr>";
                        }
                        $tabela .= "</tbody>";
                        return $tabela;
                    }
                    break;
            }
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function addUsuario(PDO $conexao, $cpf, $num_matricula, $pre_senha, $turma, $curso, $tipo_usuario, $id_escola) {
        try {
            switch ($tipo_usuario) {
                case 4:
                    $stmtInsUsuario = $conexao->prepare(UsuarioDAO::$PRE_CAD_USUARIO);
                    $stmtInsUsuario->execute(
                            array(
                                ':cpf_usuario' => $cpf,
                                ':num_matricula_usuario' => $num_matricula,
                                ':senha_usuario' => criptWordCookie($pre_senha),
                                ':tipo_usuario' => $tipo_usuario,
                                ':id_escola' => $id_escola
                            )
                    );

                    $stmtSelPkFunc = $conexao->query(UsuarioDAO::$SEL_PK_USUARIO);
                    $resultSelPK = $stmtSelPkFunc->fetch(PDO::FETCH_OBJ);
                    $id_usuario_corrente = $resultSelPK->ultimaPK;

                    $stmtInsPerfil = $conexao->prepare(UsuarioDAO::$PRE_CAD_PERFIL);
                    $stmtInsPerfil->execute(
                            array(
                                ':id_usuario_perfil_usuario' => $id_usuario_corrente
                            )
                    );

                    $stmtSelSalas = $conexao->prepare(UsuarioDAO::$SEL_SALAS_ALUNO);
                    $stmtSelSalas->execute(
                            array(
                                ':id_curso' => $curso
                            )
                    );
                    $salas = $stmtSelSalas->fetchAll();

                    $i = 0;
                    foreach ($salas as $sala) {
                        $id_salas[$i] = $sala[0];
                        $i++;
                    }

                    $id_salas[] = $turma;

                    foreach ($id_salas as $colunas) {
                        $stmtInsSala = $conexao->prepare(UsuarioDAO::$PRE_CAD_ALUNO_SALA);
                        $stmtInsSala->execute(
                                array(
                                    ':id_sala_sala_usuario' => $colunas,
                                    ':id_usuario_sala_usuario' => $id_usuario_corrente
                                )
                        );
                    }

                    $stmtInsCurso = $conexao->prepare(UsuarioDAO::$PRE_CAD_CURSO);
                    $stmtInsCurso->execute(
                            array(
                                ':id_curso_curso_usuario' => $curso,
                                ':id_usuario_curso_usuario' => $id_usuario_corrente
                            )
                    );
                    echo "<script class='sucesso_aluno'>
                        </script>";
                    break;

                case 3:
                    $stmtInsUsuario = $conexao->prepare(UsuarioDAO::$PRE_CAD_USUARIO);
                    $stmtInsUsuario->execute(
                            array(
                                ':cpf_usuario' => $cpf,
                                ':num_matricula_usuario' => $num_matricula,
                                ':senha_usuario' => criptWordCookie($pre_senha),
                                ':tipo_usuario' => $tipo_usuario,
                                ':id_escola' => $id_escola
                            )
                    );

                    $stmtSelPkFunc = $conexao->query(UsuarioDAO::$SEL_PK_USUARIO);
                    $resultSelPK = $stmtSelPkFunc->fetch(PDO::FETCH_OBJ);
                    $id_usuario_corrente = $resultSelPK->ultimaPK;

                    $stmtInsPerfil = $conexao->prepare(UsuarioDAO::$PRE_CAD_PERFIL);
                    $stmtInsPerfil->execute(
                            array(
                                ':id_usuario_perfil_usuario' => $id_usuario_corrente
                            )
                    );

                    $i = 0;
                    foreach ($curso as $id_sala_curso) {
                        $posicao_traco = strripos($id_sala_curso, "-");
                        $id_cursos[$i] = substr($id_sala_curso, 0, $posicao_traco);
                        $i++;
                    }
                    $id_cursos = array_unique($id_cursos);

                    $i = 0;
                    foreach ($turma as $id_sala_curso) {
                        $posicao_traco = strripos($id_sala_curso, "-");
                        $id_salas[$i] = substr($id_sala_curso, $posicao_traco + 1);
                        $i++;
                    }

                    foreach ($id_cursos as $id_curso) {
                        $stmtInsCurso = $conexao->prepare(UsuarioDAO::$PRE_CAD_CURSO);
                        $stmtInsCurso->execute(array(
                            ':id_curso_curso_usuario' => $id_curso,
                            ':id_usuario_curso_usuario' => $id_usuario_corrente
                        ));
                        $stmtSelSalas = $conexao->prepare(UsuarioDAO::$SEL_SALAS_PROF);
                        $stmtSelSalas->execute(array(
                            ':id_curso' => $id_curso
                        ));
                        $id_salas_default = $stmtSelSalas->fetchAll();
                        foreach ($id_salas_default as $id_sala_default) {
                            $id_salas[] = $id_sala_default[0];
                        }
                    }

                    foreach ($id_salas as $id_sala) {
                        $stmtInsSala = $conexao->prepare(UsuarioDAO::$PRE_CAD_PROF_SALA);
                        $stmtInsSala->execute(array(
                            ':id_sala_sala_usuario' => $id_sala,
                            ':id_usuario_sala_usuario' => $id_usuario_corrente
                        ));
                    }
                    echo "<script class='sucesso_prof'>
                        </script>";
                    break;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function addAdminnistrador(PDO $conexao, $pre_senha_adm, $nome_usuario_adm, $email_adm, $id_escola) {
        try {
            $stmtInsUsuario = $conexao->prepare(UsuarioDAO::$PRE_CAD_ADM_USUARIO);
            $stmtInsUsuario->execute(
                    array(
                        ':nome_usuario' => $nome_usuario_adm,
                        ':email_usuario' => $email_adm,
                        ':senha_usuario' => criptWordCookie($pre_senha_adm),
                        ':id_escola' => $id_escola
                    )
            );

            $stmtSelPkFunc = $conexao->query(UsuarioDAO::$SEL_PK_USUARIO);
            $resultSelPK = $stmtSelPkFunc->fetch(PDO::FETCH_OBJ);
            $id_usuario_corrente = $resultSelPK->ultimaPK;

            $stmtInsPerfil = $conexao->prepare(UsuarioDAO::$PRE_CAD_PERFIL);
            $stmtInsPerfil->execute(
                    array(
                        ':id_usuario_perfil_usuario' => $id_usuario_corrente
                    )
            );
            echo "<script class='sucesso_adm'>
                  </script>";
        } catch (PDOException $ex) {
            echo($ex);
        }
    }

    public function desativaUsuario(PDO $conexao, $id_usuario) {
        try {
            $stmtDesUsuario = $conexao->prepare(UsuarioDAO::$DESATIVA_USUARIO);
            $stmtDesUsuario->execute(array(':id_usuario' => $id_usuario));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function ativaUsuario(PDO $conexao, $id_usuario) {
        try {
            $stmtAtvUsuario = $conexao->prepare(UsuarioDAO::$ATIVA_USUARIO);
            $stmtAtvUsuario->execute(array(':id_usuario' => $id_usuario));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function delUsuario(PDO $conexao, $id_usuario) {
        try {
            $stmtDelUsuario = $conexao->prepare(UsuarioDAO::$DEL_USUARIO);
            $stmtDelUsuario->execute(array(':id_usuario' => $id_usuario));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function autenticarUsuario(PDO $conexao, $login_usuario, $senha_usuario, $lembrar_senha, $escolas_login) {
        try {
            $stmtAut = $conexao->prepare(UsuarioDAO::$AUT_USUARIO);
            $stmtAut->execute(array(
                ':nome_usuario' => $login_usuario,
                ':email_usuario' => $login_usuario
            ));
            $result = $stmtAut->fetch(PDO::FETCH_OBJ);
            if ($result->cod_status_usuario == 'C' && $result->total == 1 && $result->senha_usuario == criptWordCookie($senha_usuario)) {
                return criptNumberCookie($result->id_usuario) . '///' . criptNumberCookie($escolas_login) . '///A';
            } else if ($result->total == 1 && Bcrypt::check($senha_usuario, $result->senha_usuario)) {
                if ($result->cod_status_usuario == 'A') {
                    $this->updSttsOnline($conexao, $result->id_usuario);
                    return criptNumberCookie($result->id_usuario) . ';' . criptNumberCookie($escolas_login) . ';' . $lembrar_senha;
                } else if ($result->cod_status_usuario == 'D') {
                    return 'D';
                } else {
                    if ($result->data_reativacao_usuario <= date("Y-m-d")) {
                        $stmtUpdReativa = $conexao->prepare(UsuarioDAO::$UPD_REATIVA);
                        $stmtUpdReativa->execute(array(
                            ':id_usuario' => $result->id_usuario
                        ));
                        $this->updSttsOnline($conexao, $result->id_usuario);
                        return criptNumberCookie($result->id_usuario) . ';' . criptNumberCookie($escolas_login) . ';' . $lembrar_senha;
                    } else {
                        return 'I;' . $result->data_reativacao_usuario;
                    }
                }
            } else {
                return 0;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function verificaCadastro(PDO $conexao, $cpf_usuario, $num_reg_usuario, $pre_senha_usuario, $escolas_cadastro) {
        try {
            $stmtCad = $conexao->prepare(UsuarioDAO::$SEL_CADASTRO);
            $stmtCad->execute(array(
                ':cpf_usuario' => $cpf_usuario,
                ':num_matricula_usuario' => $num_reg_usuario,
                ':pre_senha_usuario' => criptWordCookie($pre_senha_usuario)
            ));
            $result = $stmtCad->fetch(PDO::FETCH_OBJ);
            if ($result->total == 1) {
                return criptNumberCookie($result->id_usuario) . '///' . criptNumberCookie($escolas_cadastro) . '///U';
            } else {
                return 0;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function verificaCookieAut(PDO $conexao, $cookie, $cad = FALSE) {
        try {
            $stmtAut = $conexao->prepare(UsuarioDAO::$COOKIE_ALT);
            $stmtAut->execute(array(
                ':id_usuario' => decriptNumberCookie($cookie)
            ));
            $result = $stmtAut->fetch(PDO::FETCH_OBJ);
            if ($result->total == 1) {
                if ($cad) {
                    if ($result->cod_status_usuario == 'C') {
                        return $result->id_grupo_usuario_usuario;
                    } else {
                        return FALSE;
                    }
                } else {
                    if ($result->cod_status_usuario == 'A') {
                        return $result->id_grupo_usuario_usuario;
                    } else {
                        return FALSE;
                    }
                }
            } else {
                return FALSE;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function verificaUser(PDO $conexao, $id) {
        try {
            $stmtAut = $conexao->prepare(UsuarioDAO::$COOKIE_ALT);
            $stmtAut->execute(array(
                ':id_usuario' => $id
            ));
            $result = $stmtAut->fetch(PDO::FETCH_OBJ);
            if ($result->total == 1) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function checkUsername(PDO $conexao, $nome_usuario) {
        try {
            $stmtCheck = $conexao->prepare(UsuarioDAO::$SEL_CHECK_USER);
            $stmtCheck->execute(array(
                ':nome_usuario' => $nome_usuario
            ));
            $result = $stmtCheck->fetch(PDO::FETCH_OBJ);
            if ($result->total == 1) {
                return $result->id_usuario;
            } else {
                return FALSE;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function checkEmail(PDO $conexao, $email_usuario) {
        try {
            $stmtCheck = $conexao->prepare(UsuarioDAO::$SEL_CHECK_EMAIL);
            $stmtCheck->execute(array(
                ':email_usuario' => $email_usuario
            ));
            $result = $stmtCheck->fetch(PDO::FETCH_OBJ);
            if ($result->total == 1) {
                return $result->id_usuario;
            } else {
                return FALSE;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function concCadUser(PDO $conexao, $id_cad_usuario, $nome_usuario_input, $email_input, $senha_input, $nome_input, $sobrenome_input, $data_nasc_completa, $sexo_radio) {
        try {
            $stmtInsUsuario = $conexao->prepare(UsuarioDAO::$INS_UPD_CAD_USER);
            $stmtInsUsuario->execute(
                    array(
                        ':senha_usuario' => Bcrypt::hash($senha_input),
                        ':nome_usuario' => $nome_usuario_input,
                        ':email_usuario' => $email_input,
                        ':id_usuario' => $id_cad_usuario,
                        ':id_usuario_perfil_usuario' => $id_cad_usuario,
                        ':nome_perfil_usuario' => $nome_input,
                        ':sobrenome_perfil_usuario' => $sobrenome_input,
                        ':sexo_perfil_usuario' => $sexo_radio,
                        ':nascimento_perfil_usuario' => $data_nasc_completa
                    )
            );
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function concCadADM(PDO $conexao, $id_cad_usuario, $nome_input, $sobrenome_input, $sexo_radio, $senha_input) {
        try {
            $stmtInsUsuario = $conexao->prepare(UsuarioDAO::$INS_UPD_CAD_ADM);
            $stmtInsUsuario->execute(
                    array(
                        ':senha_usuario' => Bcrypt::hash($senha_input),
                        ':id_usuario' => $id_cad_usuario,
                        ':id_usuario_perfil_usuario' => $id_cad_usuario,
                        ':nome_perfil_usuario' => $nome_input,
                        ':sobrenome_perfil_usuario' => $sobrenome_input,
                        ':sexo_perfil_usuario' => $sexo_radio
                    )
            );
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function forgotInfo(PDO $conexao, $id_usuario) {
        try {
            $stmtCheck = $conexao->prepare(UsuarioDAO::$SEL_FORG_INFO);
            $stmtCheck->execute(array(
                ':id_usuario' => $id_usuario,
                ':id_usuario_perfil_usuario' => $id_usuario
            ));
            $result = $stmtCheck->fetch(PDO::FETCH_OBJ);
            return $result->email_usuario . '///' . $result->nome_perfil_usuario;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function checkForgotCod(PDO $conexao, $id_usuario, $cod_recuperacao) {
        try {
            $stmtCheck = $conexao->prepare(UsuarioDAO::$SEL_FORG_COD);
            $stmtCheck->execute(array(
                ':id_usuario' => $id_usuario
            ));
            $result = $stmtCheck->fetch(PDO::FETCH_OBJ);
            if ($result->total == 1 && Bcrypt::check($cod_recuperacao, $result->cod_recupera_senha)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function updForgotCod(PDO $conexao, $id_usuario, $cod) {
        try {
            $stmtCheck = $conexao->prepare(UsuarioDAO::$UPD_FOG_COD);
            $stmtCheck->execute(array(
                ':cod_recupera_senha' => Bcrypt::hash($cod),
                ':id_usuario' => $id_usuario
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function resetPass(PDO $conexao, $id_usuario, $senha) {
        try {
            $stmtCheck = $conexao->prepare(UsuarioDAO::$UPD_RESET_PASS);
            $stmtCheck->execute(array(
                ':senha_usuario' => Bcrypt::hash($senha),
                ':id_usuario' => $id_usuario
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function updSttsOnline(PDO $conexao, $id_usuario) {
        try {
            $stmtUpd = $conexao->prepare(UsuarioDAO::$UPD_USER_ONLINE);
            $stmtUpd->execute(array(
                ':id_usuario' => $id_usuario
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selNomeFoto(PDO $conexao, $id_usuario) {
        try {
            $stmtSel = $conexao->prepare(UsuarioDAO::$SEL_NOME_FOTO);
            $stmtSel->execute(array(
                ':id_usuario' => $id_usuario
            ));
            $result = $stmtSel->fetch(PDO::FETCH_OBJ);
            return "<span class='thumb-small avatar inline'>
                        <img src='$result->url_imagem_perfil_usuario' alt='Your photo' class='img-circle'>
                    </span><span class='hidden-sm-only'>$result->nome_perfil_usuario $result->sobrenome_perfil_usuario</span>
                    <b class='caret hidden-sm-only'></b>";
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selUsersToList(PDO $conexao, $id_usuario) {
        try {
            $stmtSel = $conexao->prepare(UsuarioDAO::$SEL_CURSOS_USER);
            $stmtSel->execute(array(":id_usuario" => $id_usuario));
            $linha = $stmtSel->fetchAll();
            $i = 0;
            foreach ($linha as $coluna) {
                $stmtSelAmigos = $conexao->prepare(UsuarioDAO::$SEL_AMIGOS);
                $stmtSelAmigos->execute(array(
                    ":id_curso" => $coluna[0],
                    ":id_usuario" => $id_usuario
                ));
                $linha1 = $stmtSelAmigos->fetchAll();
                foreach ($linha1 as $coluna1) {
                    $id_usuarios[$i] = $coluna1[0];
                    $i++;
                }
            }
            if (empty($id_usuarios)) {
                return "<a class='media list-group-item no-users'>
                            <span class='media-body block m-b-none'>No other users registered in your course yet!</span>
                        </a>";
            } else {
                $id_usuarios = array_unique($id_usuarios);
                $list = "";
                foreach ($id_usuarios as $id_user) {
                    $stmtSelAmigos = $conexao->prepare(UsuarioDAO::$SEL_AMIGOS_LIST);
                    $stmtSelAmigos->execute(array(
                        ":id_usuario" => $id_user
                    ));
                    $result = $stmtSelAmigos->fetch(PDO::FETCH_OBJ);
                    if ($result->diferenca != NULL && $result->diferenca <= '00:00:05') {
                        $img_online = 'media/icones/green_circle.png';
                    } else {
                        $img_online = 'media/icones/white_circle.png';
                    }
                    if ($result->id_grupo_usuario_usuario == 3) {
                        $img_tipo = 'media/icones/teacher.png';
                    } else {
                        $img_tipo = 'media/icones/student.png';
                    }
                    $list .= "<a href='" . ($result->id_usuario + 7) . "' class='media list-group-item'>
                                <span class='pull-left thumb-small'>
                                    <img src='$result->url_imagem_perfil_usuario' alt='$result->nome_perfil_usuario $result->sobrenome_perfil_usuario' class='img-circle'>
                                </span>
                                <span class='media-body block m-b-none'>$result->nome_perfil_usuario $result->sobrenome_perfil_usuario<br>
                                    <img class='img_online_list' src='$img_online' alt='' width='16' height='16'>
                                    <img class='img_tipo_user_list' src='$img_tipo' alt='' width='16' height='16'>
                                </span>
                            </a>";
                }
                return $list;
            }
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function selUserToBox(PDO $conexao, $id_usuario) {
        try {
            $stmtSelChat = $conexao->prepare(UsuarioDAO::$SEL_CHAT_BOX);
            $stmtSelChat->execute(array(":id_usuario" => $id_usuario));
            $result = $stmtSelChat->fetch(PDO::FETCH_OBJ);
            if ($result->diferenca != NULL && $result->diferenca <= '00:00:05') {
                $img_online = 'media/icones/green_circle.png';
            } else {
                $img_online = 'media/icones/white_circle.png';
            }
            return "<div class='box_container'>
                        <div id='chat" . ($id_usuario + 7) . "' class='box size1'>
                            <div class='header_box'>
                                <img src='$result->url_imagem_perfil_usuario' alt='' width='16' height='16'>
                                <h3>$result->nome_perfil_usuario $result->sobrenome_perfil_usuario</h3>
                                <img class='img_online_box' src='$img_online' alt='' width='16' height='16'>
                                <span class='close_span'></span>
                                <span class='minimize_span'></span>
                            </div>
                            <div class='content_box with-actions'></div>
                            <div class='actions'>
                                <div style='width: 100%' class='chatters-form'>
                                    <label class='lbl-ui append-icon'>
                                        <input style='height: 30px' type='text' class='input chat_input' placeholder='Write your message'>
                                        <span style='margin: -2px 0 0 -2px' ><i class='fa fa-comment'></i></span>
                                    </label>
                                </div>
                            </div>
                            <div class='clear'></div>
                        </div>
                    </div>";
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function selUserStatus(PDO $conexao, $id_usuario) {
        try {
            $stmtSelChat = $conexao->prepare(UsuarioDAO::$SEL_USER_ONLINE);
            $stmtSelChat->execute(array(":id_usuario" => $id_usuario));
            $result = $stmtSelChat->fetch(PDO::FETCH_OBJ);
            if ($result->diferenca != NULL && $result->diferenca <= '00:00:05') {
                return 'media/icones/green_circle.png';
            } else {
                return 'media/icones/white_circle.png';
            }
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function selUsersToCheck(PDO $conexao, $id_usuario, $curso) {
        try {
            $stmtSelAmigos = $conexao->prepare(UsuarioDAO::$SEL_USERS_CURSO);
            $stmtSelAmigos->execute(array(
                ":id_curso" => $curso,
                ":id_usuario" => $id_usuario
            ));
            if ($stmtSelAmigos->rowCount() == 0) {
                return "<label>There are no other users registered in your course yet!</label>";
            } else {
                $linhas = $stmtSelAmigos->fetchAll();
                $list = "";
                foreach ($linhas as $result) {
                    $list .= "<span class='goption'>
                                <label class='options'>
                                    <input type='checkbox' id='user_sala" . ($result[0] + 7) . "' name='users_sala' value='" . ($result[0] + 7) . "'>
                                    <span class='checkbox'></span>
                                </label>
                                <img src='$result[3]' alt='Student One' class='img-circle' width='16px' heigth: '16px'>
                                <label for='user_sala" . ($result[0] + 7) . "'>$result[1] $result[2]</label>
                            </span>";
                }
                return $list;
            }
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function selPerfilUSer(PDO $conexao, $id_usuario) {
        try {
            $stmtSelPerfil = $conexao->prepare(UsuarioDAO::$SEL_PERFIL_USER);
            $stmtSelPerfil->execute(array(":id_usuario" => $id_usuario));
            $result = $stmtSelPerfil->fetch(PDO::FETCH_OBJ);
            $ocupacao = $result->ocupacao_perfil_usuario == NULL ? NULL : "<h3>" . strtoupper($result->ocupacao_perfil_usuario) . "</h3>";
            $sobre = $result->sobre_perfil_usuario == NULL ? NULL : "<p>$result->sobre_perfil_usuario<p>";
            $sexo = $result->sexo_perfil_usuario == 'M' ? 'Male' : 'Female';
            $apelido = $result->apelido_perfil_usuario == NULL ? NULL : "<li><label>Nickname</label><span>$result->apelido_perfil_usuario</span></li>";
            $email = $result->email_perfil_usuario == NULL ? NULL : "<li><label>Email</label><span>$result->email_perfil_usuario</span></li>";
            $site = $result->site_perfil_usuario == NULL ? NULL : "<li><label>Website</label><span><a href='#'>$result->site_perfil_usuario</span></a></li>";
            if ($result->id_pais_perfil_usuario != NULL) {
                $stmtSelPais = $conexao->prepare(UsuarioDAO::$SEL_COUNTRY_PERFIL);
                $stmtSelPais->execute(array(":id_pais" => $result->id_pais_perfil_usuario));
                $pais = $stmtSelPais->fetchAll();
                $local = "<li><label>Location</label><span>{$pais[0][0]}</span></li>";
                if ($result->id_estado_perfil_usuario != NULL) {
                    $stmtSelEstado = $conexao->prepare(UsuarioDAO::$SEL_STATE_PERFIL);
                    $stmtSelEstado->execute(array(":id_estado" => $result->id_estado_perfil_usuario));
                    $estado = $stmtSelEstado->fetchAll();
                    $local = "<li><label>Location</label><span>{$estado[0][0]}, {$pais[0][0]}</span></li>";
                    if ($result->id_cidade_perfil_usuario != NULL) {
                        $stmtSelCidade = $conexao->prepare(UsuarioDAO::$SEL_CITY_PERFIL);
                        $stmtSelCidade->execute(array(":id_cidade" => $result->id_cidade_perfil_usuario));
                        $cidade = $stmtSelCidade->fetchAll();
                        $local = "<li><label>Location</label><span>{$cidade[0][0]}, {$estado[0][0]}, {$pais[0][0]}</span></li>";
                    }
                }
            } else {
                $local = NULL;
            }
            $perfil = "<div class='about'>
                        <div class='photo-inner'>
                            <img src='$result->url_imagem_perfil_usuario' height='186' width='153'>
                        </div>
                        <h1>" . strtoupper($result->nome_perfil_usuario . " " . $result->sobrenome_perfil_usuario) . "</h1>
                        $ocupacao
                        $sobre
                        </div>
                        <ul class='personal-info'>
                            <li><label>Name</label><span>$result->nome_perfil_usuario $result->sobrenome_perfil_usuario</span></li>
                            $apelido
                            <li><label>Birthdate</label><span>$result->nascimento_perfil_usuario</span></li>
                            <li><label>Gender</label><span>$sexo</span></li>
                            $local
                            $email
                            $site
                        </ul>";
            return $perfil;
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function selPerfilToJson(PDO $conexao, $id_usuario) {
        try {
            $stmtSelPerfil = $conexao->prepare(UsuarioDAO::$SEL_PERFIL_USER);
            $stmtSelPerfil->execute(array(":id_usuario" => $id_usuario));
            $result = $stmtSelPerfil->fetch(PDO::FETCH_OBJ);

            $perfil = $result->nome_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->sobrenome_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->apelido_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->url_imagem_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->sobre_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->nascimento_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->ocupacao_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->sexo_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->id_pais_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->id_estado_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->id_cidade_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->email_perfil_usuario;
            $perfil .= '///';
            $perfil .= $result->site_perfil_usuario;
            return $perfil;
        } catch (PDOException $e) {
            print_r($e);
        }
    }

    public function updPerfil(PDO $conexao, $id_usuario, $dados) {
        try {
            $stmtUpdPerfil = $conexao->prepare(UsuarioDAO::$UPD_PERFIL);
            $stmtUpdPerfil->execute(array(
                ':id_usuario' => $id_usuario,
                ':nome' => $dados['nome'],
                ':sobrenome' => $dados['sobrenome'],
                ':sexo' => $dados['sexo'],
                ':sobre' => $dados['sobre'],
                ':nascimento' => $dados['nascimento'],
                ':cidade' => $dados['cidade'],
                ':estado' => $dados['estado'],
                ':pais' => $dados['pais'],
                ':email' => $dados['email'],
                ':site' => $dados['site'],
                ':apelido' => $dados['apelido'],
                ':ocupacao' => $dados['ocupacao']
            ));
            if ($dados['img'] != FALSE) {
                $novo_arq = $dados['img'][1] . $id_usuario . $dados['img'][2];
                copy($dados['img'][0], $novo_arq);
                unlink($dados['img'][0]);
                $stmtUpdImg = $conexao->prepare(UsuarioDAO::$UPD_IMG);
                $stmtUpdImg->execute(array(
                    ':id_usuario' => $id_usuario,
                    ':img' => str_replace('../', '', $novo_arq)
                ));
            }
        } catch (PDOException $e) {
            return $e;
        }
    }

}

?>
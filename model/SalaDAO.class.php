<?php

class SalaDAO {

    private static $UPD_ICONE_SALA = "UPDATE
                                        sala
                                        SET
                                        url_imagem_sala = :url_imagem
                                        WHERE
                                        id_sala = :id_sala";
    private static $INS_NOT_SALA = "INSERT INTO
                                    notificacao_sala
                                    VALUES
                                    (NULL,
                                    :id_dest,
                                    :id_reme,
                                    :id_sala,
                                    'A',
                                    FALSE,
                                    now())";
    private static $SEL_PK_SALA = "select
                                    last_insert_id() as ultimaPK
                                    from
                                    sala
                                    limit 0,1";
    private static $INS_ADM_SALA = "INSERT INTO
                                        sala_usuario
                                        VALUES
                                        (:id_sala,
                                        :id_usuario,
                                        TRUE,
                                        TRUE,
                                        TRUE)";
    private static $INS_USERS_SALA = "INSERT INTO
                                        sala_usuario
                                        VALUES
                                        (:id_sala,
                                        :id_usuario,
                                        FALSE,
                                        TRUE,
                                        FALSE)";
    private static $INS_SALA = "INSERT INTO
                                    sala
                                    VALUES
                                    (NULL,
                                    :id_curso,
                                    :nome_sala,
                                    now(),
                                    NULL,
                                    'A',
                                    :tipo_sala)";
    private static $SEL_BOX_SALA = "SELECT
                                    s.nome_sala,
                                    c.nome_curso,
                                    s.url_imagem_sala,
                                    s.cod_tipo_sala
                                    FROM
                                    sala s
                                    JOIN
                                    curso c
                                    WHERE
                                    s.id_curso_sala = c.id_curso
                                    AND
                                    s.id_sala = :id_sala";
    private static $SEL_PK_CURSO = "select
                                    last_insert_id() as ultimaPK
                                    from
                                    curso
                                    limit 0,1";
    private static $INS_SALAS_CURSO = "INSERT INTO sala
                                        VALUES
                                        (NULL,
                                        :id_curso_sala1,
                                        'Secretaria',
                                        now(),
                                        'media/icones/school.png',
                                        'A',
                                        'S');
                                        INSERT INTO sala
                                        VALUES
                                        (NULL,
                                        :id_curso_sala2,
                                        'Biblioteca',
                                        now(),
                                        'media/icones/books.png',
                                        'A',
                                        'B');
                                        INSERT INTO sala
                                        VALUES
                                        (NULL,
                                        :id_curso_sala3,
                                        'Sala dos Professores',
                                        now(),
                                        'media/icones/teacher.png',
                                        'A',
                                        'P');
                                        INSERT INTO sala
                                        VALUES
                                        (NULL,
                                        :id_curso_sala4,
                                        'Pátio',
                                        now(),
                                        'media/icones/globe.png',
                                        'A',
                                        'G');";
    private static $INS_TURMA = "INSERT INTO sala
                                        VALUES
                                        (NULL,
                                        :id_curso,
                                        :nome_turma,
                                        now(),
                                        'media/icones/class.png',
                                        'A',
                                        'T');";
    private static $SEL_SALAS = "SELECT
                                    s.id_sala,
                                    s.nome_sala,
                                    s.data_hora_criacao_sala,
                                    s.cod_status_sala,
                                    c.nome_curso,
                                    s.cod_tipo_sala
                                    FROM
                                    sala s
                                    JOIN 
                                    curso c
                                    WHERE
                                    s.id_curso_sala = c.id_curso
                                    AND
                                    s.cod_tipo_sala IN('T', 'V', 'I')";
    private static $SEL_TURMAS = "SELECT
                                    s.id_sala,
                                    s.nome_sala,
                                    c.id_curso
                                    FROM
                                    sala s
                                    join
                                    curso c
                                    WHERE
                                    s.cod_status_sala = 'A'
                                    AND
                                    s.cod_tipo_sala = 'T'
                                    AND
                                    s.id_curso_sala = c.id_curso";
    private static $SEL_TURMAS_PROFESSOR = "SELECT
                                            id_sala,
                                            nome_sala
                                            FROM
                                            sala
                                            WHERE
                                            cod_status_sala = 'A'
                                            AND
                                            cod_tipo_sala = 'T'
                                            AND
                                            id_curso_sala = :id_curso";
    private static $SEL_CURSOS = "SELECT
                                    id_curso,
                                    nome_curso
                                    FROM
                                    curso";
    private static $SEL_CURSOS_USER = "SELECT
                                        cu.id_curso_curso_usuario,
                                        c.nome_curso
                                        FROM
                                        curso_usuario cu
                                        JOIN
                                        curso c
                                        WHERE
                                        cu.id_usuario_curso_usuario = :id_usuario
                                        AND
                                        cu.id_curso_curso_usuario = c.id_curso";
    private static $SEL_OTHER_SALAS = "SELECT
                                        s.id_sala,
                                        s.nome_sala,
                                        s.url_imagem_sala
                                        FROM
                                        sala s
                                        JOIN
                                        sala_usuario su
                                        WHERE
                                        s.id_curso_sala = :id_curso
                                        AND
                                        s.id_sala = su.id_sala_sala_usuario
                                        AND
                                        s.cod_status_sala = 'A'
                                        AND
                                        su.id_usuario_sala_usuario = :id_usuario";
    private static $SEL_GRUPO_USUARIO = "SELECT
                                        id_grupo_usuario_usuario
                                        FROM
                                        usuario
                                        WHERE
                                        id_usuario = :id_usuario";
    private static $SEL_COUNT_TURMA = "SELECT
                                        count(id_sala_sala_usuario) as count
                                        FROM
                                        sala_usuario
                                        WHERE
                                        id_sala_sala_usuario = :id_sala";
    private static $DEL_TURMA = "DELETE FROM sala
                                    WHERE id_sala = :id_sala";
    private static $DES_TURMA = "UPDATE usuario u, sala_usuario su
                                SET
                                cod_status_usuario = 'D'
                                WHERE
                                u.id_usuario = su.id_usuario_sala_usuario
                                AND
                                id_sala_sala_usuario = :id_sala;
                                UPDATE sala
                                SET
                                cod_status_sala = 'D'
                                WHERE
                                id_sala = :id_sala";
    private static $ATV_TURMA = "UPDATE usuario u, sala_usuario su
                                    SET
                                    cod_status_usuario = 'A'
                                    WHERE
                                    u.id_usuario = su.id_usuario_sala_usuario
                                    AND
                                    id_sala_sala_usuario = :id_sala;
                                    UPDATE sala
                                    SET
                                    cod_status_sala = 'A'
                                    WHERE
                                    id_sala = :id_sala";
    private static $DES_SALA = "UPDATE sala
                                SET
                                cod_status_sala = 'D'
                                WHERE
                                id_sala = :id_sala";
    private static $ATV_SALA = "UPDATE sala
                                SET
                                cod_status_sala = 'A'
                                WHERE
                                id_sala = :id_sala";
    private static $VER_PART_SALA = "SELECT
                                        count(id_usuario_sala_usuario) as count
                                        FROM
                                        sala_usuario
                                        WHERE
                                        id_usuario_sala_usuario = :id_usuario
                                        AND
                                        id_sala_sala_usuario = :id_sala";
    private static $SEL_ADMS_SALA = "SELECT
                                        su.id_usuario_sala_usuario
                                        FROM
                                        sala_usuario su
                                        WHERE
                                        su.id_sala_sala_usuario = :id_sala
                                        AND
                                        su.administrador_sala_usuario = TRUE";
    private static $INS_NOT_PED_SALA = "INSERT INTO
                                        notificacao_sala
                                        VALUES
                                        (NULL,
                                        :id_dest,
                                        :id_reme,
                                        :id_sala,
                                        'M',
                                        FALSE,
                                        now())";
    private static $SEL_NOT_PED = "SELECT
                                    ns.id_sala_notificacao_sala,
                                    ns.id_usuario_remetente_notificacao_sala
                                    FROM
                                    notificacao_sala ns
                                    WHERE
                                    ns.id_notificacao_sala = :id_not";
    private static $SEL_FIRST_CURSO = "SELECT
                                        id_curso_curso_usuario as curso
                                        FROM
                                        curso_usuario
                                        WHERE
                                        id_usuario_curso_usuario = :id_usuario
                                        LIMIT 0, 1";
    private static $SEL_FIRST_TURMA = "SELECT
                                        s.id_sala as turma
                                        FROM
                                        sala s
                                        JOIN
                                        sala_usuario su
                                        WHERE
                                        su.id_sala_sala_usuario = s.id_sala
                                        AND
                                        su.id_usuario_sala_usuario = :id_usuario
                                        AND
                                        s.cod_tipo_sala = 'T'
                                        LIMIT 0,1";
    private static $SEL_TWO_SALA = "SELECT
                                    s.id_sala
                                    FROM
                                    sala s
                                    WHERE
                                    s.cod_tipo_sala IN('B', 'G')
                                    AND
                                    s.id_curso_sala = :id_curso";

    public function insSalasDefault(PDO $conexao) {
        try {
            $stmtPK = $conexao->query(SalaDAO::$SEL_PK_CURSO);
            $resultSelPK = $stmtPK->fetch(PDO::FETCH_OBJ);
            $id_curso_corrente = $resultSelPK->ultimaPK;

            $stmtInsSala = $conexao->prepare(SalaDAO::$INS_SALAS_CURSO);
            $stmtInsSala->execute(array(
                ':id_curso_sala1' => $id_curso_corrente,
                ':id_curso_sala2' => $id_curso_corrente,
                ':id_curso_sala3' => $id_curso_corrente,
                ':id_curso_sala4' => $id_curso_corrente
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function insTurma(PDO $conexao, $id_curso, $nome_turma) {
        try {
            $stmtInsSala = $conexao->prepare(SalaDAO::$INS_TURMA);
            $stmtInsSala->execute(array(
                ':id_curso' => $id_curso,
                ':nome_turma' => $nome_turma
            ));
            echo "<script class='sucesso_turma'>
                        </script>";
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selSalasToForm(PDO $conexao) {
        try {
            $stmtSel = $conexao->query(SalaDAO::$SEL_SALAS);
            $numLinhas = $stmtSel->rowCount();
            if ($numLinhas == 0) {
                return "<thead style='display: none'>
                                    <tr>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>No Registered Room Exists!</th>
                                    </tr>
                                </tbody>";
            } else {
                $tabela = "<thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Creation</th>
                                <th>Status</th>
                                <th>Course</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>";
                $linha = $stmtSel->fetchAll();
                foreach ($linha as $coluna) {
                    if ($coluna[5] == 'T') {
                        $stmtSelCount = $conexao->prepare(SalaDAO::$SEL_COUNT_TURMA);
                        $stmtSelCount->execute(array(
                            ':id_sala' => $coluna[0]
                        ));
                        $resultCount = $stmtSelCount->fetch(PDO::FETCH_OBJ);
                        if ($resultCount->count > 0) {
                            if ($coluna[3] == 'A') {
                                $link = "<a style='margin-left:7px' class='desativar_turma' id='$coluna[0]'><img title='Deactivate Room' src='media/icones/red_stop.png'></a>";
                            } else {
                                $link = "<a style='margin-left:7px' class='ativar_turma' id='$coluna[0]'><img title='Activate Room' src='media/icones/green_check.png'></a>";
                            }
                        } else {
                            $link = "<a style='margin-left:7px' class='excluir_turma' id='$coluna[0]'><img title='Delete Room' src='media/icones/delete.png'></a>";
                        }
                    } else if ($coluna[5] == 'V' || $coluna[5] == 'I') {
                        if ($coluna[3] == 'A') {
                            $link = "<a style='margin-left:7px' class='desativar_sala' id='$coluna[0]'><img title='Deactivate Room' src='media/icones/red_stop.png'></a>";
                        } else {
                            $link = "<a style='margin-left:7px' class='ativar_sala' id='$coluna[0]'><img title='Activate Room' src='media/icones/green_check.png'></a>";
                        }
                    }
                    $status = escolherIconeStatus($coluna[3]);
                    $tabela .= " <tr>
                            <td>$coluna[0]</td>
                            <td>$coluna[1]</td>
                            <td>$coluna[2]</td>
                            <td style='text-align:center'>$status</td>
                            <td>$coluna[4]</td>
                            <td style='text-align:center'>
                                $link
                            </td>
                        </tr>";
                }
                $tabela .= "</tbody>";
                return $tabela;
            }
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function comboBoxTurmas(PDO $conexao) {
        try {
            $stmtSelGrAcesso = $conexao->query(SalaDAO::$SEL_TURMAS);
            $linhas = $stmtSelGrAcesso->fetchAll();

            $comboBox = "";

            foreach ($linhas as $coluna) {
                $comboBox.= "<option class='sel_turma id_curso_$coluna[2]' value=$coluna[0]>$coluna[1]</option>";
            }

            return $comboBox;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selectTurmasProfessor(PDO $conexao) {
        try {
            $stmtSelCursos = $conexao->query(SalaDAO::$SEL_CURSOS);
            $linhas = $stmtSelCursos->fetchAll();

            $resultado = "<ul style=\"float: left; margin-left: 20px\">";
            $resultado_turmas = "";

            foreach ($linhas as $coluna) {
                $resultado.= "<li><a href=\"#tab_curso_$coluna[0]\">$coluna[1]</a></li>";
                $resultado_turmas.="<div id=\"tab_curso_$coluna[0]\" class=\"tab-content\">";
                $stmtSelTurmas = $conexao->prepare(SalaDAO::$SEL_TURMAS_PROFESSOR);
                $stmtSelTurmas->execute(array(":id_curso" => $coluna[0]));
                $linhas2 = $stmtSelTurmas->fetchAll();
                foreach ($linhas2 as $coluna2) {
                    $resultado_turmas.= "<p style=\"float: left; margin-left: 30px\">
                                            <label for=\"checkbox_turma_$coluna2[0]\">
                                                <input id=\"checkbox_turma_$coluna2[0]\" value=\"$coluna[0]-$coluna2[0]\" name=\"cursos_turmas_professor[]\" type=\"checkbox\"/>$coluna2[1]</label>
                                        </p>";
                }
                $resultado_turmas.="</div>";
            }
            $resultado.="</ul>
                    </div>
                    <div style=\"clear: both; border: none; margin-left: 10px\">
                        $resultado_turmas";

            return $resultado;
        } catch (PDOException $ex) {
            echo($ex);
        }
    }

    public function selSalasToList(PDO $conexao, $id_usuario) {
        try {
            $stmtSel = $conexao->prepare(SalaDAO::$SEL_CURSOS_USER);
            $stmtSel->execute(array(":id_usuario" => $id_usuario));
            $linha = $stmtSel->fetchAll();
            $list = "";
            foreach ($linha as $coluna) {
                $list .= "<div style='margin: -5px 0 0 0; border-bottom: none' class='panel-heading bg-white'>
                                    <span class='h5'>
                                        <strong>$coluna[1]</strong>
                                    </span>
                                </div>";
                $stmtSelTurmas = $conexao->prepare(SalaDAO::$SEL_OTHER_SALAS);
                $stmtSelTurmas->execute(array(
                    ":id_curso" => $coluna[0],
                    ":id_usuario" => $id_usuario
                ));
                $linha2 = $stmtSelTurmas->fetchAll();
                foreach ($linha2 as $coluna2) {
                    $list .= "<a href='" . ($coluna2[0] + 7) . "' class='media list-group-item'>
                                    <span class='pull-left thumb-small'>
                                        <img src='$coluna2[2]' style='margin-top: -5px' alt='John said' class='img' width='24' height='24'>
                                    </span>
                                    <span class='media-body block m-b-none'>$coluna2[1]</span>
                                </a>";
                }
            }
            return $list;
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function selSalaToBox(PDO $conexao, $id_sala, $id_usuario, $tamanho) {
        try {
            $stmtSelGrupo = $conexao->prepare(SalaDAO::$SEL_GRUPO_USUARIO);
            $stmtSelGrupo->execute(array(":id_usuario" => $id_usuario));
            $result0 = $stmtSelGrupo->fetch(PDO::FETCH_OBJ);
            $grupo_usuario = $result0->id_grupo_usuario_usuario;
            $stmtSelSala = $conexao->prepare(SalaDAO::$SEL_BOX_SALA);
            $stmtSelSala->execute(array(":id_sala" => $id_sala));
            $result = $stmtSelSala->fetch(PDO::FETCH_OBJ);
            if ($tamanho == 1) {
                $max = 30;
                $tamanho = 2;
                $width = 89;
            } else {
                $max = 50;
                $tamanho = 3;
                $width = 93;
            }
            $actions = "<div class='actions'>
                                <div style='width: 100%' class='chatters-form'>
                                    <label style='width: $width%; float: left' class='lbl-ui append-icon'>
                                        <input style='height: 30px' type='text' class='input sala_input' placeholder='Escreva sua mensagem'>
                                        <span style='margin: -2px 0 0 -2px' ><i class='fa fa-share-square-o'></i></span>
                                    </label>
                                    <label for='file_sala' class='lbl-ui file-input box-upload'>
                                        <span class='button' style='height: auto; border: none'>
                                            <input type='file' id='file_sala' style='width: 0px' name='file_sala'>
                                        </span>
                                        <input style='width: 32px; height: 30px; z-index: -1'  type='text' id='input_file_sala' name='input_file_sala' class='input' placeholder='' readonly=''>
                                        <b style='display: none' class='tooltip right'></b>
                                    </label>
                                </div>
                            </div>";
            $class = "with-actions";
            if ($result->cod_tipo_sala == 'B' && $grupo_usuario == 3) {
                
            } else if ($result->cod_tipo_sala == 'P' && $grupo_usuario == 3) {
                
            } else if ($result->cod_tipo_sala == 'G' || $result->cod_tipo_sala == 'T' || $result->cod_tipo_sala == 'V' || $result->cod_tipo_sala == 'I') {
                
            } else {
                $actions = "";
                $class = "";
            }
            return "<div class='box_container'>
                        <div id='sala" . ($id_sala + 7) . "' class='box size$tamanho'>
                            <div class='header_box'>
                                <img src='$result->url_imagem_sala' alt='' width='16' height='16'>
                                <h3>" . maxChrString($result->nome_sala . " - " . $result->nome_curso, $max) . "</h3>
                                <span class='close_span'></span>
                                <span class='minimize_span'></span>
                            </div>
                            <div class='content_box $class'></div>
                            $actions
                            <div class='clear'></div>
                        </div>
                    </div>";
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function insNewSala(PDO $conexao, $id_usuario, $nome_sala, $curso_sala, $tipo_sala, $array_icone, $users_sala) {
        try {
            $stmtInsSala = $conexao->prepare(SalaDAO::$INS_SALA);
            $stmtInsSala->execute(array(
                ':id_curso' => $curso_sala,
                ':nome_sala' => $nome_sala,
                ':tipo_sala' => $tipo_sala
            ));

            $stmtPK = $conexao->query(SalaDAO::$SEL_PK_SALA);
            $resultSelPK = $stmtPK->fetch(PDO::FETCH_OBJ);
            $id_sala_corrente = $resultSelPK->ultimaPK;

            copy($array_icone[0], $array_icone[1] . $id_sala_corrente . $array_icone[2]);
            unlink($array_icone[0]);

            $stmtUpdIco = $conexao->prepare(SalaDAO::$UPD_ICONE_SALA);
            $stmtUpdIco->execute(array(
                ':url_imagem' => str_replace('../', '', $array_icone[1] . $id_sala_corrente . $array_icone[2]),
                ':id_sala' => $id_sala_corrente
            ));

            $stmtInsCriador = $conexao->prepare(SalaDAO::$INS_ADM_SALA);
            $stmtInsCriador->execute(array(
                ':id_sala' => $id_sala_corrente,
                ':id_usuario' => $id_usuario
            ));
            if ($users_sala != FALSE) {
                foreach ($users_sala as $usuario) {
                    $stmtInsUserSala = $conexao->prepare(SalaDAO::$INS_USERS_SALA);
                    $stmtInsUserSala->execute(array(
                        ':id_sala' => $id_sala_corrente,
                        ':id_usuario' => ($usuario - 7)
                    ));

                    $stmtInsNot = $conexao->prepare(SalaDAO::$INS_NOT_SALA);
                    $stmtInsNot->execute(array(
                        ':id_reme' => $id_usuario,
                        ':id_dest' => ($usuario - 7),
                        ':id_sala' => $id_sala_corrente
                    ));
                }
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function delTurma(PDO $conexao, $id_sala) {
        try {
            $stmtCurso = $conexao->prepare(SalaDAO::$DEL_TURMA);
            $stmtCurso->execute(array(
                ':id_sala' => $id_sala
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function desTurma(PDO $conexao, $id_sala) {
        try {
            $stmtCurso = $conexao->prepare(SalaDAO::$DES_TURMA);
            $stmtCurso->execute(array(
                ':id_sala' => $id_sala
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function atvTurma(PDO $conexao, $id_sala) {
        try {
            $stmtCurso = $conexao->prepare(SalaDAO::$ATV_TURMA);
            $stmtCurso->execute(array(
                ':id_sala' => $id_sala
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function desSala(PDO $conexao, $id_sala) {
        try {
            $stmtCurso = $conexao->prepare(SalaDAO::$DES_SALA);
            $stmtCurso->execute(array(
                ':id_sala' => $id_sala
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function atvSala(PDO $conexao, $id_sala) {
        try {
            $stmtCurso = $conexao->prepare(SalaDAO::$ATV_SALA);
            $stmtCurso->execute(array(
                ':id_sala' => $id_sala
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function verPartSala(PDO $conexao, $id_usuario, $id_sala) {
        try {
            $stmtVer = $conexao->prepare(SalaDAO::$VER_PART_SALA);
            $stmtVer->execute(array(
                ':id_usuario' => $id_usuario,
                ':id_sala' => $id_sala
            ));
            $resultSel = $stmtVer->fetch(PDO::FETCH_OBJ);
            if ($resultSel->count > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function pedidoSala(PDO $conexao, $id_usuario, $id_sala) {
        try {
            $stmtSelAdms = $conexao->prepare(SalaDAO::$SEL_ADMS_SALA);
            $stmtSelAdms->execute(array(
                ':id_sala' => $id_sala
            ));
            $result = $stmtSelAdms->fetchAll();
            foreach ($result as $coluna) {
                $stmtInsNot = $conexao->prepare(SalaDAO::$INS_NOT_PED_SALA);
                $stmtInsNot->execute(array(
                    ':id_reme' => $id_usuario,
                    ':id_dest' => $coluna[0],
                    ':id_sala' => $id_sala
                ));
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function addUserSala(PDO $conexao, $id_not, $id_usuario) {
        try {
            $stmtSelNot = $conexao->prepare(SalaDAO::$SEL_NOT_PED);
            $stmtSelNot->execute(array(
                ':id_not' => $id_not
            ));
            $result = $stmtSelNot->fetch(PDO::FETCH_OBJ);
            $stmtInsUseSala = $conexao->prepare(SalaDAO::$INS_USERS_SALA);
            $stmtInsUseSala->execute(array(
                ':id_usuario' => $result->id_usuario_remetente_notificacao_sala,
                ':id_sala' => $result->id_sala_notificacao_sala,
            ));
            $stmtInsNot = $conexao->prepare(SalaDAO::$INS_NOT_SALA);
            $stmtInsNot->execute(array(
                ':id_reme' => $id_usuario,
                ':id_dest' => $result->id_usuario_remetente_notificacao_sala,
                ':id_sala' => $result->id_sala_notificacao_sala
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selSalasDefault(PDO $conexao, $id_usuario) {
        try {
            $stmtSelCurso = $conexao->prepare(SalaDAO::$SEL_FIRST_CURSO);
            $stmtSelCurso->execute(array(
                ':id_usuario' => $id_usuario
            ));
            $result1 = $stmtSelCurso->fetch(PDO::FETCH_OBJ);
            
            $stmtSelTurma = $conexao->prepare(SalaDAO::$SEL_FIRST_TURMA);
            $stmtSelTurma->execute(array(
                ':id_usuario' => $id_usuario
            ));
            $result2 = $stmtSelTurma->fetch(PDO::FETCH_OBJ);
            
            $stmtSelOther = $conexao->prepare(SalaDAO::$SEL_TWO_SALA);
            $stmtSelOther->execute(array(
                ':id_curso' => $result1->curso
            ));
            $result3 = $stmtSelOther->fetchAll();
            
            return ($result2->turma + 7) . "///" . ($result3[0][0] + 7) . "///" . ($result3[1][0] + 7);
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

}

?>
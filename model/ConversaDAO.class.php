<?php

class ConversaDAO {

    private static $SEL_CONVERSAS = "SELECT
                                        c.mensagem_conversa,
                                        c.id_remetente_conversa
                                        FROM
                                        conversa c
                                        WHERE
                                        c.id_remetente_conversa = :id_usuario1
                                        AND
                                        c.id_destinatario_conversa = :id_usuario_dest1
                                        OR
                                        c.id_remetente_conversa = :id_usuario_dest2
                                        AND
                                        c.id_destinatario_conversa = :id_usuario2
                                        ORDER BY c.data_hora_conversa";
    private static $INS_CHAT = "INSERT INTO conversa
                                (id_conversa,
                                data_hora_conversa,
                                id_remetente_conversa,
                                id_destinatario_conversa,
                                mensagem_conversa,
                                data_hora_visualizacao_conversa)
                                VALUES
                                (NULL,
                                now(),
                                :id_usuario,
                                :id_usuario_to,
                                :mensagem,
                                date_sub(now(), INTERVAL 10 YEAR))";
    private static $SEL_MENSAGENS = "SELECT
                                        pu.id_usuario_perfil_usuario,
                                        pu.nome_perfil_usuario,
                                        pu.sobrenome_perfil_usuario,
                                        pu.url_imagem_perfil_usuario,
                                        MIN(c.data_hora_visualizacao_conversa),
                                        MIN(timediff(now(), c.data_hora_conversa)) as diferenca
                                        FROM
                                        conversa c,
                                        perfil_usuario pu
                                        WHERE
                                        pu.id_usuario_perfil_usuario = c.id_remetente_conversa
                                        AND
                                        c.id_destinatario_conversa = :id_usuario
                                        GROUP BY
                                        pu.id_usuario_perfil_usuario,
                                        pu.nome_perfil_usuario,
                                        pu.sobrenome_perfil_usuario,
                                        pu.url_imagem_perfil_usuario
                                        ORDER BY diferenca";
    private static $UPD_VIS_CONVERSAS = "UPDATE
                                            conversa
                                            SET
                                            data_hora_visualizacao_conversa = now()
                                            WHERE
                                            id_remetente_conversa = :id_remetente
                                            AND
                                            id_destinatario_conversa = :id_usuario";

    public function insConversa(PDO $conexao, $id_usuario, $id_usuario_to, $mensagem) {
        try {
            $stmtInsConversa = $conexao->prepare(ConversaDAO::$INS_CHAT);
            $stmtInsConversa->execute(
                    array(
                        ':id_usuario' => $id_usuario,
                        ':id_usuario_to' => $id_usuario_to,
                        ':mensagem' => $mensagem
                    )
            );
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selConversas(PDO $conexao, $id_usuario, $id_usuario_dest) {
        try {
            $stmtSelConversa = $conexao->prepare(ConversaDAO::$SEL_CONVERSAS);
            $stmtSelConversa->execute(
                    array(
                        ':id_usuario1' => $id_usuario,
                        ':id_usuario2' => $id_usuario,
                        ':id_usuario_dest1' => $id_usuario_dest,
                        ':id_usuario_dest2' => $id_usuario_dest
                    )
            );
            $linhas = $stmtSelConversa->fetchAll();
            $conteudo = "";
            foreach ($linhas as $colunas) {
                if ($colunas[1] == $id_usuario) {
                    $float = "left";
                } else {
                    $float = "right";
                }
                $conteudo .= "<div class='talk-bubble talk_border talk_round' style='float: $float; max-width= 75%; margin: 10px 10px 0px 10px'>
                                    <div class='talktext' style='padding: 1px 5px 1px 5px'>
                                      <p>$colunas[0]</p>
                                    </div>
                                  </div>
                                  <div class='clear'></div>";
            }
            return $conteudo;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selMsgHTML(PDO $conexao, $id_usuario) {
        try {
            $stmtSelMsg = $conexao->prepare(ConversaDAO::$SEL_MENSAGENS);
            $stmtSelMsg->execute(array(
                ':id_usuario' => $id_usuario
            ));
            $linhas = $stmtSelMsg->fetchAll();
            $list = "";
            if (empty($linhas)) {
                return "<a class='media list-group-item no-msgs'>
                            <span class='media-body block m-b-none'>You have no messages yet!</span>
                        </a>";
            } else {
                foreach ($linhas as $colunas) {
                    if ($colunas[4] <= '2010-01-01 00:00:00') {
                        $classe = " msg-no-vis";
                    } else {
                        $classe = "";
                    }
                    if ($colunas[5] <= '00:00:05') {
                        $classe2 = " msg-alert";
                    } else {
                        $classe2 = "";
                    }
                    $list .= "<a href='" . ($colunas[0] + 7) . "' class='media list-group-item$classe$classe2'>
                            <span class='pull-left thumb-small'>
                                <img src='$colunas[3]' alt='John said' class='img-circle'>
                            </span>
                            <span class='media-body block m-b-none'>
                                $colunas[1] $colunas[2] talked to you.
                                <br>
                                <small class='text-muted'>$colunas[5]</small>
                            </span>
                        </a>";
                }
                return $list;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function updVisConversas(PDO $conexao, $id_usuario, $id_remetente) {
        try {
            $stmtUpdVis = $conexao->prepare(ConversaDAO::$UPD_VIS_CONVERSAS);
            $stmtUpdVis->execute(array(
                ':id_remetente' => $id_remetente,
                ':id_usuario' => $id_usuario
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

}

?>
<?php

class EscolaDAO {

    private static $SEL_ESCOLAS = "SELECT
                                    e.id_escola,
                                    e.nome
                                    FROM
                                    escola e";
    private static $SEL_ESCOLA = "SELECT
                                    e.sigla
                                    FROM
                                    escola e
                                    WHERE
                                    e.id_escola = :id_escola";

    public function comboBoxEscolas(PDO $conexao) {
        try {
            $stmtSelEscolas = $conexao->query(EscolaDAO::$SEL_ESCOLAS);
            $linhas = $stmtSelEscolas->fetchAll();
            $comboBox = "";
            foreach ($linhas as $colunas) {
                $comboBox.= "<option value='$colunas[0]'>$colunas[1]</option>";
            }
            return $comboBox;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function verificaEscola(PDO $conexao, $id_escola) {
        try {
            $stmtAut = $conexao->prepare(EscolaDAO::$SEL_ESCOLA);
            $stmtAut->execute(array(
                ':id_escola' => $id_escola
            ));
            $nroLinhas = $stmtAut->rowCount();
            if ($nroLinhas == 0) {
                return FALSE;
            } else {
                $result = $stmtAut->fetch(PDO::FETCH_OBJ);
                return $result->sigla;
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

}

?>
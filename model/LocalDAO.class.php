<?php

class LocalDAO {

    private static $SEL_PAISES = "SELECT
                                    p.id_pais,
                                    p.nome_pais
                                    FROM
                                    pais p";
    private static $SEL_ESTADOS = "SELECT
                                    e.id_estado,
                                    e.nome_estado
                                    FROM
                                    estado e
                                    JOIN
                                    pais p
                                    WHERE
                                    e.id_pais_estado = :id_pais";
    private static $SEL_CIDADES = "SELECT
                                    c.id_cidade,
                                    c.nome_cidade
                                    FROM
                                    cidade c
                                    JOIN
                                    estado e
                                    WHERE
                                    e.id_estado = c.id_estado_cidade
                                    AND
                                    e.id_estado = :id_estado";

    public function comboBoxPaises(PDO $conexao) {
        try {
            $stmtSelPaises = $conexao->query(LocalDAO::$SEL_PAISES);
            $linhas = $stmtSelPaises->fetchAll();
            $comboBox = "";
            foreach ($linhas as $colunas) {
                $comboBox.= "<option value='$colunas[0]'>$colunas[1]</option>";
            }
            return $comboBox;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }
    
    public function comboBoxEstados(PDO $conexao, $id_pais) {
        try {
            $stmtSelEstados = $conexao->prepare(LocalDAO::$SEL_ESTADOS);
            $stmtSelEstados->execute(array(
                ':id_pais' => $id_pais
            ));
            $linhas = $stmtSelEstados->fetchAll();
            $comboBox = "<option></option><option value='0'>Do not inform</option>";
            foreach ($linhas as $colunas) {
                $comboBox.= "<option value='$colunas[0]'>$colunas[1]</option>";
            }
            return $comboBox;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }
    
    public function comboBoxCidades(PDO $conexao, $id_estado) {
        try {
            $stmtSelCidades = $conexao->prepare(LocalDAO::$SEL_CIDADES);
            $stmtSelCidades->execute(array(
                ':id_estado' => $id_estado
            ));
            $linhas = $stmtSelCidades->fetchAll();
            $comboBox = "<option></option><option value='0'>Do not inform</option>";
            foreach ($linhas as $colunas) {
                $comboBox.= "<option value='$colunas[0]'>$colunas[1]</option>";
            }
            return $comboBox;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

}

?>

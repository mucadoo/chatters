<?php

class CursoDAO {

    private static $INS_CURSO = "INSERT INTO curso
                                    VALUES
                                    (NULL,
                                    :nome_curso,
                                    'A',
                                    now(), :id_escola)";
    private static $SEL_CMB_CURSOS = "select
                                        id_curso,
                                        nome_curso
                                    from
                                        curso
                                    where
                                        cod_status_curso = 'A'";
    private static $SEL_CURSOS = "select
                                    *
                                    from
                                    curso";
    private static $SEL_CURSOS_USER = "SELECT
                                        c.id_curso,
                                        c.nome_curso
                                        FROM
                                        curso c
                                        JOIN
                                        curso_usuario cu
                                        WHERE
                                        c.id_curso = cu.id_curso_curso_usuario
                                        AND
                                        cu.id_usuario_curso_usuario = :id_usuario";
    private static $SEL_PESQUISA = "SELECT
                                    DISTINCT
                                    u.id_usuario,
                                    CONCAT(pu.nome_perfil_usuario, ' ' , pu.sobrenome_perfil_usuario) as nome,
                                    pu.url_imagem_perfil_usuario,
                                    u.id_grupo_usuario_usuario
                                    FROM
                                    curso_usuario cu
                                    JOIN
                                    usuario u
                                    JOIN
                                    perfil_usuario pu
                                    WHERE
                                    u.id_usuario = pu.id_usuario_perfil_usuario
                                    AND
                                    u.id_usuario = cu.id_usuario_curso_usuario
                                    AND
                                    u.id_usuario NOT IN (:id_usuario)
                                    AND
                                    cu.id_curso_curso_usuario IN (:id_cursos)
                                    AND
                                    u.cod_status_usuario NOT IN('C')
                                    AND
                                    pu.nome_perfil_usuario LIKE :pesquisa
                                    OR
                                    u.id_usuario = pu.id_usuario_perfil_usuario
                                    AND
                                    u.id_usuario = cu.id_usuario_curso_usuario
                                    AND
                                    u.id_usuario NOT IN (:id_usuario)
                                    AND
                                    cu.id_curso_curso_usuario IN (:id_cursos)
                                    AND
                                    u.cod_status_usuario NOT IN('C')
                                    AND
                                    pu.sobrenome_perfil_usuario LIKE :pesquisa
                                    UNION ALL
                                    SELECT
                                    DISTINCT
                                    s.id_sala,
                                    s.nome_sala as nome,
                                    s.url_imagem_sala,
                                    c.nome_curso
                                    FROM
                                    sala s
                                    JOIN
                                    sala_usuario su
                                    JOIN
                                    curso c
                                    WHERE
                                    s.id_curso_sala IN (:id_cursos)
                                    AND
                                    s.id_sala = su.id_sala_sala_usuario
                                    AND
                                    s.cod_status_sala = 'A'
                                    AND
                                    su.id_usuario_sala_usuario = :id_usuario
                                    AND
                                    c.id_curso = s.id_curso_sala
                                    AND
                                    s.nome_sala LIKE :pesquisa
                                    OR
                                    s.id_curso_sala IN (:id_cursos)
                                    AND
                                    s.id_sala = su.id_sala_sala_usuario
                                    AND
                                    s.cod_status_sala = 'A'
                                    AND
                                    su.id_usuario_sala_usuario = :id_usuario
                                    AND
                                    c.id_curso = s.id_curso_sala
                                    AND
                                    c.nome_curso LIKE :pesquisa
                                    OR
                                    s.id_curso_sala IN (:id_cursos)
                                    AND
                                    s.id_sala = su.id_sala_sala_usuario
                                    AND
                                    s.cod_status_sala = 'A'
                                    AND
                                    c.id_curso = s.id_curso_sala
                                    AND
                                    s.cod_tipo_sala = 'V'
                                    AND
                                    s.nome_sala LIKE :pesquisa
                                    OR
                                    s.id_curso_sala IN (:id_cursos)
                                    AND
                                    s.id_sala = su.id_sala_sala_usuario
                                    AND
                                    s.cod_status_sala = 'A'
                                    AND
                                    c.id_curso = s.id_curso_sala
                                    AND
                                    s.cod_tipo_sala = 'V'
                                    AND
                                    c.nome_curso LIKE :pesquisa
                                    ORDER BY nome";
    private static $SEL_COUNT_CURSO = "SELECT
                                        count(id_curso_curso_usuario) as count
                                        FROM
                                        curso_usuario
                                        WHERE
                                        id_curso_curso_usuario = :id_curso";
    private static $DEL_CURSO = "DELETE FROM sala
                                    WHERE id_curso_sala = :id_curso;
                                    DELETE FROM curso
                                    WHERE id_curso = :id_curso;";
    private static $DES_CURSO = "UPDATE usuario u, curso_usuario cu
                                    SET
                                    cod_status_usuario = 'D'
                                    WHERE
                                    u.id_usuario = id_usuario_curso_usuario
                                    AND
                                    id_curso_curso_usuario = :id_curso;
                                    UPDATE curso
                                    SET
                                    cod_status_curso = 'D'
                                    WHERE
                                    id_curso = :id_curso";
    private static $ATV_CURSO = "UPDATE usuario u, curso_usuario cu
                                    SET
                                    cod_status_usuario = 'A'
                                    WHERE
                                    u.id_usuario = id_usuario_curso_usuario
                                    AND
                                    id_curso_curso_usuario = :id_curso;
                                    UPDATE curso
                                    SET
                                    cod_status_curso = 'A'
                                    WHERE
                                    id_curso = :id_curso";

    public function insCurso(PDO $conexao, $nome_curso, $id_escola) {
        try {
            $stmtInsCurso = $conexao->prepare(CursoDAO::$INS_CURSO);
            $stmtInsCurso->execute(array(
                ':nome_curso' => $nome_curso,
                ':id_escola' => $id_escola
            ));
            echo "<script class='sucesso_curso'>
                        </script>";
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function comboBoxCursos(PDO $conexao) {
        try {
            $stmtSelGrAcesso = $conexao->query(CursoDAO::$SEL_CMB_CURSOS);
            $linhas = $stmtSelGrAcesso->fetchAll();

            $comboBox = "";

            foreach ($linhas as $coluna) {
                $comboBox.= "<option value=$coluna[0]>$coluna[1]</option>";
            }
            return $comboBox;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selCursosToForm(PDO $conexao) {
        try {
            $stmtSel = $conexao->query(CursoDAO::$SEL_CURSOS);
            $numLinhas = $stmtSel->rowCount();
            if ($numLinhas == 0) {
                return "<thead style='display: none'>
                                    <tr>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>No Registered Course Exists!</th>
                                    </tr>
                                </tbody>";
            } else {
                $tabela = "<thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Creation</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>";
                $linha = $stmtSel->fetchAll();
                foreach ($linha as $coluna) {
                    $stmtSelCount = $conexao->prepare(CursoDAO::$SEL_COUNT_CURSO);
                    $stmtSelCount->execute(array(
                        ':id_curso' => $coluna[0]
                    ));
                    $resultCount = $stmtSelCount->fetch(PDO::FETCH_OBJ);
                    if ($resultCount->count > 0) {
                        if ($coluna[2] == 'A') {
                            $class = "desativar_curso";
                            $img = "<img title='Deactivate Course' src='media/icones/red_stop.png'>";
                        } else {
                            $class = "ativar_curso";
                            $img = "<img title='Activate Course' src='media/icones/green_check.png'>";
                        }
                    } else {
                        $class = "excluir_curso";
                        $img = "<img title='Delete Course' src='media/icones/delete.png'>";
                    }
                    $status = escolherIconeStatus($coluna[2]);
                    $tabela .= " <tr>
                            <td>$coluna[0]</td>
                            <td>$coluna[1]</td>
                            <td>$coluna[3]</td>
                            <td style='text-align:center'>$status</td>
                            <td style='text-align:center'>
                                <!--<a href='index.php?page=usuarios&acao=2&id=$coluna[0]'><img title='Edit User' src='media/icones/rewrite.png'></a>-->
                                <a style='margin-left:7px' class='$class' id='$coluna[0]'>$img</a>
                            </td>
                        </tr>";
                }
                $tabela .= "</tbody>";
                return $tabela;
            }
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function cmbCursosUser(PDO $conexao, $id_usuario) {
        try {
            $stmtSelCursos = $conexao->prepare(CursoDAO::$SEL_CURSOS_USER);
            $stmtSelCursos->execute(array(
                ':id_usuario' => $id_usuario
            ));
            $linhas = $stmtSelCursos->fetchAll();
            $comboBox = "";
            foreach ($linhas as $coluna) {
                $comboBox.= "<option value=$coluna[0]>$coluna[1]</option>";
            }
            return $comboBox;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function pesqUserSala(PDO $conexao, $id_usuario, $pesquisa) {
        try {
            $stmtSel = $conexao->prepare(CursoDAO::$SEL_CURSOS_USER);
            $stmtSel->execute(array(":id_usuario" => $id_usuario));
            $linha = $stmtSel->fetchAll();
            $numLinhas = $stmtSel->rowCount();
            $i = 0;
            $id_cursos = '';
            foreach ($linha as $coluna) {
                if ($i == (count($linha) - 1)) {
                    $id_cursos .= $coluna[0];
                } else {
                    $id_cursos .= $coluna[0] . ', ';
                }
                $i++;
            }
            $stmtPesq = str_replace(':id_cursos', $id_cursos, CursoDAO::$SEL_PESQUISA);
            $stmtSelPesquisa = $conexao->prepare($stmtPesq);
            $stmtSelPesquisa->execute(array(
                ":id_usuario" => $id_usuario,
                ":id_cursos" => $id_cursos,
                ":pesquisa" => $pesquisa . '%'
            ));
            $numLinhas = $stmtSelPesquisa->rowCount();
            if ($numLinhas == 0) {
                $list = "<header id='1' class='panel-heading bg-white'>
                            <span class='h5' style='float: left'>
                                <strong>No results for '$pesquisa'</strong>
                            </span>
                        </header>";
            } else {
                $list = "<header id='1' class='panel-heading bg-white'>
                            <span class='h5' style='float: left'>
                                <strong>Showing results for '$pesquisa'</strong>
                            </span>
                        </header>
                        <div class='scroll-list list-group list-group-flush m-t-n'>";
                $result = $stmtSelPesquisa->fetchAll();
                foreach ($result as $coluna) {
                    if (is_numeric($coluna[3])) {
                        $tamanho = "class='img-circle'";
                        $nome = "";
                        $class = "pesq_user";
                        if ($coluna[3] == 4) {
                            $img_tipo = "<br><img class='img_tipo_user_list' src='media/icones/student.png' alt='' width='16' height='16'>";
                        } else {
                            $img_tipo = "<br><img class='img_tipo_user_list' src='media/icones/teacher.png' alt='' width='16' height='16'>";
                        }
                    } else {
                        $class = "pesq_sala";
                        $nome = " - $coluna[3]";
                        $tamanho = "style='margin-top: -5px'  class='img' width='24' height='24'";
                        $img_tipo = '';
                    }
                    $list .= "<a href='" . ($coluna[0] + 7) . "' class='media list-group-item $class'>
                                <span class='pull-left thumb-small'>
                                    <img src='$coluna[2]' alt='$coluna[1]$nome' $tamanho>
                                </span>
                                <span class='media-body block m-b-none'>$coluna[1]$nome
                                    $img_tipo
                                </span>
                            </a>";
                }
                $list .= "</div>";
            }
            return $list;
        } catch (PDOException $e) {
            return $e;
        }
    }
    
    public function delCurso(PDO $conexao, $id_curso) {
        try {
            $stmtCurso = $conexao->prepare(CursoDAO::$DEL_CURSO);
            $stmtCurso->execute(array(
                ':id_curso' => $id_curso
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }
    
    public function desCurso(PDO $conexao, $id_curso) {
        try {
            $stmtCurso = $conexao->prepare(CursoDAO::$DES_CURSO);
            $stmtCurso->execute(array(
                ':id_curso' => $id_curso
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }
    
    public function atvCurso(PDO $conexao, $id_curso) {
        try {
            $stmtCurso = $conexao->prepare(CursoDAO::$ATV_CURSO);
            $stmtCurso->execute(array(
                ':id_curso' => $id_curso
            ));
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

}

?>
<?php

class PublicacaoDAO {

    private static $UPD_ARQ_PUB = "UPDATE publicacao
                                    SET
                                    url_arquivo_publicacao = :arq
                                    WHERE id_publicacao = :pub";
    private static $INS_PUB = "INSERT INTO publicacao
                                (id_publicacao,
                                id_sala_publicacao,
                                mensagem_publicacao,
                                data_hora_criacao_publicacao,
                                url_arquivo_publicacao,
                                id_usuario_publicacao)
                                VALUES
                                (NULL,
                                :id_sala,
                                :post,
                                now(),
                                NULL,
                                :id_usuario)";
    private static $SEL_USERS_NOTIFY = "SELECT
                                        su.id_usuario_sala_usuario,
                                        su.notificacoes_sala_usuario
                                        FROM
                                        sala_usuario su
                                        JOIN
                                        usuario u
                                        WHERE
                                        su.id_usuario_sala_usuario = u.id_usuario
                                        AND
                                        su.id_sala_sala_usuario = :id_sala
                                        AND
                                        su.id_usuario_sala_usuario NOT IN(:id_usuario)
                                        AND
                                        u.cod_status_usuario NOT IN('C')";
    private static $INS_NOTIFY = "INSERT INTO
                                        notificacao_publicacao
                                        VALUES
                                        (NULL,
                                        :id_publicacao,
                                        :id_usuario,
                                        0,
                                        'P')";
    private static $SEL_PK_PUB = "SELECT
                                    last_insert_id() as ultimaPK
                                    FROM
                                    publicacao
                                    limit 0,1";
    private static $SEL_PUBS = "SELECT
                                pu.nome_perfil_usuario,
                                pu.sobrenome_perfil_usuario,
                                pu.url_imagem_perfil_usuario,
                                p.id_publicacao,
                                p.mensagem_publicacao,
                                p.url_arquivo_publicacao,
                                p.id_usuario_publicacao,
                                timediff(now(), p.data_hora_criacao_publicacao) as data_hora,
                                u.id_grupo_usuario_usuario
                                FROM
                                publicacao p
                                JOIN
                                perfil_usuario pu
                                JOIN
                                usuario u
                                WHERE
                                u.id_usuario = pu.id_usuario_perfil_usuario
                                AND
                                p.id_usuario_publicacao = pu.id_usuario_perfil_usuario
                                AND
                                p.id_sala_publicacao = :id_sala
                                ORDER BY p.data_hora_criacao_publicacao DESC";

    public function insPostSala(PDO $conexao, $id_usuario, $id_sala_dest, $post, $arq) {
        try {
            $stmtInsPub = $conexao->prepare(PublicacaoDAO::$INS_PUB);
            $stmtInsPub->execute(array(
                ':id_sala' => $id_sala_dest,
                ':post' => $post,
                ':id_usuario' => $id_usuario
            ));

            $stmtSelPk = $conexao->query(PublicacaoDAO::$SEL_PK_PUB);
            $resultSelPK = $stmtSelPk->fetch(PDO::FETCH_OBJ);
            $id_publicacao = $resultSelPK->ultimaPK;

            if ($arq != NULL) {
                mkdir($arq[1] . $id_publicacao);
                $novo_arq = $arq[1] . $id_publicacao . '/' . $arq[2];
                copy($arq[0], $novo_arq);
                unlink($arq[0]);
                $stmtInsNot = $conexao->prepare(PublicacaoDAO::$UPD_ARQ_PUB);
                $stmtInsNot->execute(array(
                    ':arq' => str_replace('../', '', $novo_arq),
                    ':pub' => $id_publicacao
                ));
            }

            $stmtSelUsers = $conexao->prepare(PublicacaoDAO::$SEL_USERS_NOTIFY);
            $stmtSelUsers->execute(array(
                ':id_sala' => $id_sala_dest,
                ':id_usuario' => $id_usuario
            ));
            $linhas = $stmtSelUsers->fetchAll();

            foreach ($linhas as $colunas) {
                if ($colunas[1] == TRUE) {
                    $stmtInsNot = $conexao->prepare(PublicacaoDAO::$INS_NOTIFY);
                    $stmtInsNot->execute(array(
                        ':id_publicacao' => $id_publicacao,
                        ':id_usuario' => $colunas[0]
                    ));
                }
            }
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

    public function selPubs(PDO $conexao, $id_usuario, $id_sala) {
        try {
            $stmtSelPubs = $conexao->prepare(PublicacaoDAO::$SEL_PUBS);
            $stmtSelPubs->execute(array(
                ':id_sala' => $id_sala
            ));
            $linhas = $stmtSelPubs->fetchAll();
            $conteudo = "";
            foreach ($linhas as $colunas) {
                $pub = '';
                if ($colunas[6] == $id_usuario) {
                    $float = "left";
                    $excluir = "<span></span>";
                } else {
                    $float = "right";
                }
                if ($colunas [8] == 4) {
                    $img = "media/icones/student.png";
                } else {
                    $img = "media/icones/teacher.png";
                }
                if ($colunas[4] != NULL) {
                    $pub .= "<p>$colunas[4]</p>";
                }
                if ($colunas[5] != NULL) {
                    $arq_name = pegaNome($colunas[5]);
                    $pub .= "<p><a href='?download=$colunas[5]' target='_blank' class='down_arq'>$arq_name</a></p>";
                }
                $conteudo .= "<div id='$colunas[3]' class='talk-bubble talk_border talk_round' style='float: $float; max-width: 80%'>
                                    <div style='padding: 10px 10px 0 10px' class='dropdown-toggle'>
                                    <span class='pull-left thumb-small'>
                                        <img src='$colunas[2]' alt='$colunas[0] $colunas[1]' class='img-circle'>
                                    </span>
                                    <span style='padding-left: 5px' class='media-body block m-b-none'>$colunas[0] $colunas[1]<br>
                                        <img class='img_tipo_user_list' src='$img' alt='' width='12' height='12'>
                                        <small class='text-muted'>$colunas[7]</small>
                                        <a class='coments' title='Comments'><img class='img_tipo_user_list' src='media/icones/balloon.png' alt='' width='12' height='12'></a>
                                    </span>
                                    </div>
                                    <div class='talktext'>
                                      $pub
                                    </div>
                                  </div>
                                  <div class='clear'></div>";
            }
            return $conteudo;
        } catch (PDOException $ex) {
            print_r($ex);
        }
    }

}

?>
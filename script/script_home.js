var BASE_URL = "http://localhost";

$(function(e) {
    var x = document.documentElement.clientHeight;
    var y = e(".header").outerHeight();
    e("#parallax_wrapper").css("height", x - y + "px");
    e("#parallax_wrapper").css("left", 50 + "%");
    e(".scene_1").plaxify({"xRange": 0, "yRange": 0, "invert": true}),
    e(".scene_2").plaxify({"xRange": 70, "yRange": 20, "invert": true}),
    e(".scene_3").plaxify({"xRange": 0, "yRange": 40, "invert": true}),
    e.plax.enable();
});

function resizePanel() {
    width = $(window).width();
    height = $(window).height();
    mask_width = width * $('.item').length;
    $('#wrapper2, .item').css({width: width, height: height});
    $('#mask').css({width: mask_width, height: height});
    $('#wrapper2').scrollTo($('a.menu_selected').attr('href'), 0);
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}

function criaValidacao(nome, mensagem) {
    $.validator.addMethod(
            nome,
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }, mensagem);
}

$(document).ready(function() {

    $('.main-menu').css('padding-left', PADDING_MENU);

    $('.scroll-pane').jScrollPane();

    resizePanel();
    $(window).resize(function() {
        resizePanel();
    });

    if ($('#new_pass_form')[0]) {
        $('a.a_scroll').removeClass('menu_selected');
        $('.main-menu').find('a[href=#new_pass_page]').addClass('menu_selected');
        $('#wrapper2').scrollTo('#new_pass_page', 1200);
        setTimeout(function() {
            $('#new_pass_input').focus();
        }, 1200);
    } else if ($('#container_cad_form1').hasClass('cadastro_andando_u')) {
        $('#container_cad_form3').css("display", "none");
        $('a.a_scroll').removeClass('menu_selected');
        $('.main-menu').find('a[href=#sign_up_page]').addClass('menu_selected');
        $('#wrapper2').scrollTo('#sign_up_page', 1200);
        setTimeout(function() {
            $('#nome_usuario_input').focus();
        }, 1200);
    } else if ($('#container_cad_form1').hasClass('cadastro_andando_a')) {
        $('#container_cad_form2').css("display", "none");
        $('a.a_scroll').removeClass('menu_selected');
        $('.main-menu').find('a[href=#sign_up_page]').addClass('menu_selected');
        $('#wrapper2').scrollTo('#sign_up_page', 1200);
        setTimeout(function() {
            $('#nome_adm_input').focus();
        }, 1200);
    }

    $('a.a_scroll').click(function() {
        if ($(this).attr('href') == '#login_page' && !$(this).hasClass("menu_selected")) {
            setTimeout(function() {
                $('#login_input').focus();
            }, 1200);
        } else if ($(this).attr('href') == '#sign_up_page' && !$(this).hasClass("menu_selected")) {
            if ($('.cadastro_andando_u')[0]) {
                setTimeout(function() {
                    $('#nome_usuario_input').focus();
                }, 1200);
            } else if ($('.cadastro_andando_a')[0]) {
                setTimeout(function() {
                    $('#nome_adm_input').focus();
                }, 1200);
            } else {
                setTimeout(function() {
                    $('#cpf_input').focus();
                }, 1200);
            }
        } else if ($(this).attr('href') == '#contact_page' && !$(this).hasClass("menu_selected")) {
            setTimeout(function() {
                $('#nome_contato').focus();
            }, 1200);
        } else if ($(this).attr('href') == '#forgot_pass_page' && !$(this).hasClass("menu_selected")) {
            setTimeout(function() {
                $('#forgot_input').focus();
            }, 1200);
        }
        if ($(this).parent().hasClass("main-menu")) {
            $('a.a_scroll').removeClass('menu_selected');
            $(this).addClass('menu_selected');
        }
        else {
            $('a.a_scroll').removeClass('menu_selected');
            var link = $(this).attr('href');
            $('.main-menu').find('a[href=' + link + ']').addClass('menu_selected');
        }
        $('#wrapper2').scrollTo($(this).attr('href'), 1200);
        return false;
    });

    $('select').chosen();
    $('.chzn-drop').css('width', '100%');
    $('.chzn-drop').find('input').css('width', '100%');

    $('.mini select, #assunto_contato').chosen().change(function() {
        var sel_atual = $(this);
        sel_atual.siblings('input').val(sel_atual.val());
    });

    $('#dia_sel').chosen().change(function() {
        $(this).parent().parent().parent().siblings('#hidden_dia').val($(this).val());
    });

    $('#mes_sel').chosen().change(function() {
        $(this).parent().parent().parent().siblings('#hidden_mes').val($(this).val());
    });

    $('#ano_sel').chosen().change(function() {
        $(this).parent().parent().parent().siblings('#hidden_ano').val($(this).val());
    });

    $('input:radio[name="sexo_radio"]').change(function() {
        $(this).parent().parent().parent().siblings('#hidden_cad2').val($(this).val());
    });

    $('input:radio[name="sexo_radio_adm"]').change(function() {
        $(this).parent().parent().parent().siblings('#hidden_cad4').val($(this).val());
    });

    $('#agree_terms').change(function() {
        if ($('#agree_terms').is(':checked')) {
            $(this).siblings('#hidden_cad3').val($(this).val());
        } else {
            $(this).siblings('#hidden_cad3').val('');
        }
    });

    criaValidacao('inicia_letras', MSG_INICIA_LETRAS);

    criaValidacao('usuario_email', MSG_USUARIO_EMAIL);

    criaValidacao('nome_usuario', MSG_NOME_USUARIO);

    criaValidacao('cpf', MSG_CPF);

    criaValidacao('nome', MSG_NOME);

    jQuery.extend(jQuery.validator.messages, {
        required: MSG_REQUIRED,
        email: MSG_EMAIL,
        number: MSG_NUMEROS
    });

    $('#login_form').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.siblings('b').append(error);
            element.siblings('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {

            var dados = $('#login_form').serialize();

            $.post("controller/home_ajax_requests.php",
                    dados,
                    function(data) {
                        var split_barra = data.split('///');
                        if (split_barra[2] == 'A') {

                            $('#container_cad_form1').css("display", "none");
                            $('#container_cad_form2').css("display", "none");
                            $('#container_cad_form3').css("display", "block");

                            document.cookie = "cod_cadastro = " + data + ";";

                            $('#conc_sign_up_form2').prepend('<input type="hidden" name="id_cad_usuario" id="id_cad_usuario" value="' + split_barra[0] + '">');
                            $('#conc_sign_up_form2').prepend('<input type="hidden" name="esc_cad_usuario" id="esc_cad_usuario" value="' + split_barra[1] + '">');

                            $('a.a_scroll').removeClass('menu_selected');
                            $('.main-menu').find('a[href=#sign_up_page]').addClass('menu_selected');
                            $('#wrapper2').scrollTo('#sign_up_page', 1200);
                            setTimeout(function() {
                                $('#nome_adm_input').focus();
                            }, 1200);
                            $('#container_cad_form1').addClass('cadastro_andando_a');

                        } else if (data == 'D') {
                            $.fallr({
                                content: MSG_CONTA_BANIDA,
                                icon: 'error',
                                zIndex: 1000
                            });
                        } else if (data == 0) {
                            $.fallr({
                                content: MSG_LOGIN_ERRADO,
                                icon: 'error',
                                zIndex: 1000
                            });
                        } else {
                            var split = data.split(';');
                            if (split[0] == 'I') {
                                var us_data = split[1].split('-');
                                var data_final = us_data[2] + '/' + us_data[1] + '/' + us_data[0];
                                $.fallr({
                                    content: MSG_CONTA_DESATIVADA_1 + data_final + MSG_CONTA_DESATIVADA_2,
                                    icon: 'error',
                                    zIndex: 1000
                                });
                            } else {
                                if (split[2] == 1) {
                                    var d = new Date();
                                    d.setTime(d.getTime() + (1825 * 24 * 60 * 60 * 1000));
                                    var expires = "expires=" + d.toGMTString();
                                    document.cookie = "cod_logado = " + split[0] + ";" + expires;
                                    document.cookie = "esc_logado = " + split[1] + ";" + expires;
                                } else {
                                    document.cookie = "cod_logado = " + split[0] + ";";
                                    document.cookie = "esc_logado = " + split[1] + ";";
                                }
                                location.reload(true);
                            }
                        }
                    }
            );
        },
        rules: {
            login_input: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                usuario_email: "^[0-9a-zA-Z@.]+$"
            },
            senha_input: {
                required: true
            },
            hidden_login: {
                required: true
            }
        },
        messages: {
            hidden_login: {
                required: MSG_ESCOLA
            }
        }
    });

    $('#sign_up_form').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.siblings('b').append(error);
            element.siblings('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {

            var dados = $('#sign_up_form').serialize();

            $.post("controller/home_ajax_requests.php",
                    dados,
                    function(data) {
                        if (data != 0) {

                            $('#hidden_cad').css("display", "none");

                            document.cookie = "cod_cadastro = " + data + ";";

                            $('#container_cad_form1').fadeOut("normal", function() {
                                $('#container_cad_form1').addClass('cadastro_andando_u');
                                var split = data.split('///');
                                $('#conc_sign_up_form').prepend('<input type="hidden" name="id_cad_usuario" id="id_cad_usuario" value="' + split[0] + '">');
                                $('#conc_sign_up_form').prepend('<input type="hidden" name="esc_cad_usuario" id="esc_cad_usuario" value="' + split[1] + '">');
                                $('#container_cad_form1').css("display", "none");
                                $('#container_cad_form2').fadeIn("normal", function() {
                                    $('#nome_usuario_input').focus();
                                });
                            });

                        } else {

                            $.fallr({
                                content: MSG_CAD_ERRADO,
                                icon: 'error',
                                zIndex: 1000
                            });
                        }
                    }
            );
        },
        rules: {
            cpf_input: {
                required: true,
                cpf: "^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})|([0-9]{9}-[0-9]{2})|([0-9]{11}))$"
            },
            num_reg_input: {
                required: true,
                number: true
            },
            pre_senha_input: {
                required: true
            },
            hidden_cad: {
                required: true
            }
        },
        messages: {
            hidden_cad: {
                required: MSG_ESCOLA
            }
        }

    });

    $('#conc_sign_up_form').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.next('b').append(error);
            element.next('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {

            $('#data_nasc_completa').val($('#ano_sel').val() + '-' + $('#mes_sel').val() + '-' + $('#dia_sel').val());

            var dados = $('#conc_sign_up_form').serialize();

            $.post("controller/home_ajax_requests.php",
                    dados,
                    function(data) {
                        if (data == 'U') {
                            $('#nome_usuario_input').next('b').append('<em>' + MSG_USER_EXIST + '</em>');
                            $('#nome_usuario_input').next('b').css('display', 'block');
                            $('#nome_usuario_input').focus();
                            $('#nome_usuario_input').keydown(function() {
                                $('#nome_usuario_input').next('b').css('display', 'none');
                                $('#nome_usuario_input').next('b').find('em').remove();
                            });
                        } else if (data == 'E') {
                            $('#email_input').next('b').append('<em>' + MSG_EMAIL_EXIST + '</em>');
                            $('#email_input').next('b').css('display', 'block');
                            $('#email_input').focus();
                            $('#email_input').keydown(function() {
                                $('#email_input').next('b').css('display', 'none');
                                $('#email_input').next('b').find('em').remove();
                            });
                        } else {
                            var split = data.split(';');
                            document.cookie = "cod_logado = " + split[0] + ";";
                            document.cookie = "esc_logado = " + split[1] + ";";
                            $.fallr({
                                content: MSG_CONC_CAD,
                                zIndex: 1000
                            });
                            $('#fallr-button-button1').click(function() {
                                setTimeout(function() {
                                    location.reload(true);
                                }, 300);
                            });
                        }
                    }
            );
        },
        rules: {
            nome_usuario_input: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                nome_usuario: "^[0-9a-zA-Z]+$"
            },
            email_input: {
                required: true,
                email: true
            },
            senha_input: {
                required: true,
                minlength: 6
            },
            repeat_senha_input: {
                required: true,
                equalTo: "#conc_sign_up_form #senha_input"
            },
            nome_input: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                nome: "^[a-zA-Z������������������������ ]+$"
            },
            sobrenome_input: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                nome: "^[a-zA-Z������������������������ ]+$"
            },
            hidden_dia: {
                required: true
            },
            hidden_mes: {
                required: true
            },
            hidden_ano: {
                required: true
            },
            hidden_cad2: {
                required: true
            },
            hidden_cad3: {
                required: true
            }
        },
        messages: {
            senha_input: {
                minlength: MSG_MIN_6
            },
            repeat_senha_input: {
                equalTo: MSG_SAME_PASS
            },
            hidden_dia: {
                required: MSG_DATA_NASC
            },
            hidden_mes: {
                required: MSG_DATA_NASC
            },
            hidden_ano: {
                required: MSG_DATA_NASC
            },
            hidden_cad2: {
                required: MSG_SEXO
            },
            hidden_cad3: {
                required: MSG_TERMOS
            }
        }

    });

    $('#conc_sign_up_form2').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.next('b').append(error);
            element.next('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {

            var dados = $('#conc_sign_up_form2').serialize();

            $.post("controller/home_ajax_requests.php",
                    dados,
                    function(data) {
                        var split = data.split(';');
                        document.cookie = "cod_logado = " + split[0] + ";";
                        document.cookie = "esc_logado = " + split[1] + ";";
                        $.fallr({
                            content: MSG_CONC_CAD,
                            zIndex: 1000
                        });
                        $('#fallr-button-button1').click(function() {
                            setTimeout(function() {
                                location.reload(true);
                            }, 300);
                        });
                    }
            );
        },
        rules: {
            nome_adm_input: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                nome: "^[a-zA-Z������������������������ ]+$"
            },
            sobrenome_adm_input: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                nome: "^[a-zA-Z������������������������ ]+$"
            },
            nova_senha_input: {
                required: true,
                minlength: 6
            },
            repita_senha_input: {
                required: true,
                equalTo: "#nova_senha_input"
            },
            hidden_cad4: {
                required: true
            }
        },
        messages: {
            nova_senha_input: {
                minlength: MSG_MIN_6
            },
            repita_senha_input: {
                equalTo: MSG_SAME_PASS
            },
            hidden_cad4: {
                required: MSG_SEXO
            }
        }

    });

    $('#contact_form').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.next('b').append(error);
            element.next('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {

            var dados = $('#contact_form').serialize();

            $.post("controller/home_ajax_requests.php",
                    dados,
                    function(data) {

                        if (data == 'S') {
                            $.fallr({
                                content: MSG_CONTATO_SEND,
                                zIndex: 1000
                            });
                        } else {
                            $.fallr({
                                content: MSG_CONTATO_ERRO + data,
                                icon: 'error',
                                zIndex: 1000
                            });
                        }

                    }
            );
        },
        rules: {
            nome_contato: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                nome: "^[a-zA-Z������������������������ ]+$"
            },
            email_contato: {
                required: true,
                email: true
            },
            hidden_contato: {
                required: true
            },
            msg_contato: {
                required: true
            }
        },
        messages: {
            hidden_contato: {
                required: MSG_SEL_ASS
            }
        }

    });

    $('#forgot_pass_form').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.next('b').append(error);
            element.next('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {

            var dados = $('#forgot_pass_form').serialize();

            $.post("controller/home_ajax_requests.php",
                    dados,
                    function(data) {

                        if (data == 0) {
                            $.fallr({
                                content: MSG_FORGOT_ERRADO,
                                icon: 'error',
                                zIndex: 1000
                            });
                        } else if (data == 'S') {
                            $.fallr({
                                content: MSG_FORGOT_SEND,
                                zIndex: 1000
                            });
                        } else {
                            $.fallr({
                                content: MSG_FORGOT_ERRO + data,
                                icon: 'error',
                                zIndex: 1000
                            });
                        }

                    }
            );
        },
        rules: {
            forgot_input: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                usuario_email: "^[0-9a-zA-Z@.]+$"
            },
            hidden_forgot: {
                required: true
            }
        },
        messages: {
            hidden_forgot: {
                required: MSG_ESCOLA
            }
        }

    });

    $('#new_pass_form').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.next('b').append(error);
            element.next('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {

            var dados = $('#new_pass_form').serialize();

            $.post("controller/home_ajax_requests.php",
                    dados,
                    function(data) {

                        $.fallr({
                            content: MSG_UPD_PASS,
                            zIndex: 1000
                        });

                        $('#fallr-button-button1').click(function() {
                            setTimeout(function() {
                                location.assign(BASE_URL);
                            }, 300);
                        });

                    }
            );
        },
        rules: {
            new_pass_input: {
                required: true,
                minlength: 6
            },
            rep_new_pass: {
                required: true,
                equalTo: "#new_pass_input"
            }
        },
        messages: {
            new_pass_input: {
                minlength: MSG_MIN_6
            },
            rep_new_pass: {
                equalTo: MSG_SAME_PASS
            }
        }

    });

    if (getCookie("lang") == "pt") {
        $('#change_portugues').addClass('atual_lang');
        $('#change_english').addClass('other_lang');
        $('#change_english').click(function() {
            var d = new Date();
            d.setTime(d.getTime() + (1825 * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = "lang = en;" + expires;
            location.reload(true);
        });
    } else {
        $('#change_english').addClass('atual_lang');
        $('#change_portugues').addClass('other_lang');
        $('#change_portugues').click(function() {
            var d = new Date();
            d.setTime(d.getTime() + (1825 * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = "lang = pt;" + expires;
            location.reload(true);
        });
    }

});
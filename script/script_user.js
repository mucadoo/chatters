var BASE_URL = "http://localhost";

var files_sala = new Array();

$(function(e) {
    var x = document.documentElement.clientHeight;
    var y = e(".header").outerHeight();
    e("#parallax_wrapper").css("height", x - y + "px");
    e("#parallax_wrapper").css("left", 50 + "%");
    e(".scene_1").plaxify({"xRange": 0, "yRange": 0, "invert": true}),
            e(".scene_2").plaxify({"xRange": 70, "yRange": 20, "invert": true}),
            e(".scene_3").plaxify({"xRange": 0, "yRange": 40, "invert": true}),
            e.plax.enable();
});

function criaValidacao(nome, mensagem) {
    $.validator.addMethod(
            nome,
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            }, mensagem);
}

function resizePanel() {
    width = $(window).width();
    height = $(window).height();
    $('#wrapper2, .item').css({width: width, height: height});
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}

function criarBox(box_id) {
    $(box_id).drags({
        handle: ".header_box"
    });
    $(box_id).find('.header_box').children('.minimize_span').click(function() {
        var $this = $(this);
        var $box = $this.parents(box_id);
        var $content = $box.find('.content_box');
        var $actions = $box.find('.actions');
        if ($content.is(':visible')) {
            if ($content.hasClass('with-actions')) {
                $actions.slideToggle(200, function() {
                    $content.slideToggle('normal', function() {
                        $box.toggleClass('closed');
                        $(window).resize();
                    });
                });
            } else {
                $content.slideToggle('normal', function() {
                    $box.toggleClass('closed');
                    $(window).resize();
                });
            }
        } else {
            $content.slideToggle('normal', function() {
                $actions.slideToggle(200, function() {
                    $(window).resize();
                });
            });
            $box.toggleClass('closed');
        }
    });

    $(box_id).find('.header_box').children('.close_span').click(function() {
        var $this = $(this);
        $this.parents(box_id).parent().fadeOut("slow", function() {
            $this.parents(box_id).parent().remove();
        });
    });

    var this_id = $(box_id).attr('id');

    $(box_id).find('input[type=file]').on('change', prepareUploadsala);
    function prepareUploadsala(event) {
        files_sala[this_id.substr(4)] = event.target.files;
    }

    $(box_id).find('input[type=file]').change(function() {
        $(box_id).find('.sala_input').focus();
        var this_val = $(this).val();
        $(this).parent().siblings('input').val(this_val);
    });
}

function clickListaMsg(item) {
    item.click(function() {
        var alvo = $('#dropdown-toggle1');
        $('#dropdown-menu1').slideUp();
        alvo.find('i').removeClass("open1");
        var $this = $(this);
        if (!$this.hasClass('no-msgs')) {
            var href = $this.attr('href');
            $.post("controller/user_ajax_requests.php",
                    'id_user_chat=' + href + '&cod_logado=' + getCookie("cod_logado") + '&esc_logado=' + getCookie("esc_logado"),
                    function(data) {
                        var posicao = data.indexOf('///');
                        var box_html = data.substr(0, posicao);
                        var content_html = data.substr(posicao + 3);
                        $('.boxes').prepend(box_html);
                        criarBox('#chat' + href);
                        $('#chat' + href).find('.content_box').prepend(content_html);
                        $('#chat' + href).find('.chat_input').focus();
                        $.post('controller/user_ajax_requests.php', {
                            id_remetente: href,
                            cod_logado: getCookie("cod_logado"),
                            esc_logado: getCookie("esc_logado")
                        }, function(data) {
                            $('#dropdown-menu1').find('.list-group').empty();
                            $('#dropdown-menu1').find('.list-group').append(data);
                            $('#dropdown_li1 .list-group-item').each(function() {
                                clickListaMsg($(this));
                            });
                            $('#dropdown-toggle1').find('b').remove();
                            if ($('.msg-no-vis').length > 0) {
                                $('#dropdown-toggle1').append('<b class="badge badge-notes bg-danger">' + $('.msg-no-vis').length + '</b>');
                            }
                        });
                    }
            );
        }
        return false;
    });
}

function clickListaNot(item) {
    item.click(function() {
        var alvo = $('#dropdown-toggle2');
        $('#dropdown-menu2').slideUp();
        alvo.find('i').removeClass("open2");
        var $this = $(this);
        if (!$this.hasClass('no-notify') && !$this.hasClass('not-pedido')) {
            var tamanho_sala = $('#dropdown_li4').find('header').attr('id');
            var href = $this.attr('href');
            var split = href.split('/');
            if (tamanho_sala == 1 || tamanho_sala == 2) {
                $.post("controller/user_ajax_requests.php",
                    'id_sala=' + split[0] + '&tamanho=' + tamanho_sala + '&cod_logado=' + getCookie("cod_logado") + '&esc_logado=' + getCookie("esc_logado"),
                    function(data) {
                        var posicao = data.indexOf('///');
                        var box_html = data.substr(0, posicao);
                        var content_html = data.substr(posicao + 3);
                        $('.boxes').prepend(box_html);
                        criarBox('#sala' + split[0]);
                        $('#sala' + split[0]).find('.content_box').prepend(content_html);
                        $('#sala' + split[0]).find('.sala_input').focus();
                        $.post('controller/user_ajax_requests.php', {
                            id_not: split[2],
                            tipo_not: split[3],
                            cod_logado: getCookie("cod_logado"),
                            esc_logado: getCookie("esc_logado")
                        }, function(data) {
                            $('#dropdown-menu2').find('.list-group').empty();
                            $('#dropdown-menu2').find('.list-group').append(data);
                            $('#dropdown_li2 .list-group-item').each(function() {
                                clickListaNot($(this));
                            });
                            $('#dropdown-toggle2').find('b').remove();
                            if ($('.not-no-vis').length > 0) {
                                $('#dropdown-toggle2').append('<b class="badge badge-notes bg-danger">' + $('.not-no-vis').length + '</b>');
                            }
                        });
                    });
            }
        } else if ($this.hasClass('not-pedido')) {
            var href = $this.attr('href');
            var split = href.split('/');
            $.fallr('show', {
                buttons: {
                    button1: {text: 'Yes', danger: true, onclick: function() {
                            $.fallr('hide');
                            $.post('controller/user_ajax_requests.php', {
                                    id_not_pedido: split[2],
                                    cod_logado: getCookie("cod_logado"),
                                    esc_logado: getCookie("esc_logado")
                                },
                                function(data) {
                                    setTimeout(function() {
                                        $.fallr({
                                            content: 'User successfully added!',
                                            zIndex: 21000
                                        });
                                    }, 400);
                                });
                        }},
                    button2: {text: 'Cancel', onclick: function() {
                            $.fallr('hide');
                        }}
                },
                content: 'Do you want to accept this request?',
                icon: 'error',
                zIndex: 21000
            });
        }
        return false;
    });
}

function clickListaSala(item) {
    item.click(function() {
        var alvo = $('#dropdown-toggle4');
        $('#dropdown-menu4').slideUp();
        alvo.find('i').removeClass("open4");

        var tamanho_sala = $(this).parent().siblings('header').attr('id');
        var href = $(this).attr('href');
        if (tamanho_sala == 1 || tamanho_sala == 2) {
            $.post("controller/user_ajax_requests.php",
                    'id_sala=' + href + '&tamanho=' + tamanho_sala + '&cod_logado=' + getCookie("cod_logado") + '&esc_logado=' + getCookie("esc_logado"),
                    function(data) {
                        var posicao = data.indexOf('///');
                        var box_html = data.substr(0, posicao);
                        var content_html = data.substr(posicao + 3);
                        $('.boxes').prepend(box_html);
                        criarBox('#sala' + href);
                        $('#sala' + href).find('.content_box').prepend(content_html);
                        $('#sala' + href).find('.sala_input').focus();
                    }
            );
        }
        return false;
    });
}

function clickUserPesq(item) {
    item.click(function() {
        $('#dropdown-menu6').fadeOut();
        var $this = $(this);
        var chat_perfil = $('#dropdown-menu5').find('header').attr('id');
        var href = $this.attr('href');
        if (chat_perfil == 1) {
            $.post("controller/user_ajax_requests.php",
                    'id_user_chat=' + href + '&cod_logado=' + getCookie("cod_logado") + '&esc_logado=' + getCookie("esc_logado"),
                    function(data) {
                        var posicao = data.indexOf('///');
                        var box_html = data.substr(0, posicao);
                        var content_html = data.substr(posicao + 3);
                        $('.boxes').prepend(box_html);
                        criarBox('#chat' + href);
                        $('#chat' + href).find('.content_box').prepend(content_html);
                        $('#chat' + href).find('.chat_input').focus();
                        $.post('controller/user_ajax_requests.php', {
                            id_remetente: href,
                            cod_logado: getCookie("cod_logado"),
                            esc_logado: getCookie("esc_logado")
                        }, function(data) {
                            $('#dropdown-menu1').find('.list-group').empty();
                            $('#dropdown-menu1').find('.list-group').append(data);
                            $('#dropdown_li1 .list-group-item').each(function() {
                                clickListaMsg($(this));
                            });
                            $('#dropdown-toggle1').find('b').remove();
                            if ($('.msg-no-vis').length > 0) {
                                $('#dropdown-toggle1').append('<b class="badge badge-notes bg-danger">' + $('.msg-no-vis').length + '</b>');
                            }
                        });
                    }
            );
        } else if (chat_perfil == 2) {
            $.post('controller/user_ajax_requests.php', {
                actions_perfil: 0,
                cod_user_perfil: href,
                esc_logado: getCookie("esc_logado")
            }, function(data) {
                $('#perfil_modal .about').remove();
                $('#perfil_modal .personal-info').remove();
                $('#perfil_modal .form-buttons').hide();
                $('#perfil_modal .form-section').append(data);
            });
            $('#header').css('z-index', '-10');
            var maskHeight = $(document).height();
            var maskWidth = $(window).width();
            $('.mask_modal').css({'width': maskWidth, 'height': maskHeight});
            $('.mask_modal').fadeIn(500);
            $('#perfil_modal').fadeIn(1000);
        }
        return false;
    });
}

function clickSalaPesq(item) {
    item.click(function() {
        $('#dropdown-menu6').fadeOut();

        var tamanho_sala = $('#dropdown-menu4').find('header').attr('id');
        var href = $(this).attr('href');

        if (tamanho_sala == 1 || tamanho_sala == 2) {
            $.post("controller/user_ajax_requests.php", {
                    id_sala: href,
                    tamanho: tamanho_sala,
                    cod_logado: getCookie("cod_logado"),
                    esc_logado: getCookie("esc_logado")
                },
                function(data) {
                    if (data != 0) {
                        var posicao = data.indexOf('///');
                        var box_html = data.substr(0, posicao);
                        var content_html = data.substr(posicao + 3);
                        $('.boxes').prepend(box_html);
                        criarBox('#sala' + href);
                        $('#sala' + href).find('.content_box').prepend(content_html);
                        $('#sala' + href).find('.sala_input').focus();
                    } else {
                        $.fallr('show', {
                            buttons: {
                                button1: {text: 'Yes', danger: true, onclick: function() {
                                        $.fallr('hide');
                                        $.post('controller/user_ajax_requests.php', {
                                            quero_sala: href,
                                            cod_logado: getCookie("cod_logado"),
                                            esc_logado: getCookie("esc_logado")
                                        }, function(data) {
                                            setTimeout(function() {
                                                $.fallr({
                                                    content: 'Request successfully sent!',
                                                    zIndex: 21000
                                                });
                                            }, 400);
                                        });
                                    }},
                                button2: {text: 'Cancel', onclick: function() {
                                        $.fallr('hide');
                                    }}
                            },
                            content: 'You do not belong to this room, do you want to send a request?',
                            icon: 'error',
                            zIndex: 21000
                        });
                    }
                });
        }
        return false;
    });
}

function clickUserList(item) {
    item.click(function() {
        var alvo = $('#dropdown-toggle5');
        $('#dropdown-menu5').slideUp();
        alvo.find('i').removeClass("open5");
        var $this = $(this);
        if (!$this.hasClass('no-users')) {
            var chat_perfil = $this.parent().siblings('header').attr('id');
            var href = $this.attr('href');
            if (chat_perfil == 1) {
                $.post("controller/user_ajax_requests.php",
                        'id_user_chat=' + href + '&cod_logado=' + getCookie("cod_logado") + '&esc_logado=' + getCookie("esc_logado"),
                        function(data) {
                            var posicao = data.indexOf('///');
                            var box_html = data.substr(0, posicao);
                            var content_html = data.substr(posicao + 3);
                            $('.boxes').prepend(box_html);
                            criarBox('#chat' + href);
                            $('#chat' + href).find('.content_box').prepend(content_html);
                            $('#chat' + href).find('.chat_input').focus();
                            $.post('controller/user_ajax_requests.php', {
                                id_remetente: href,
                                cod_logado: getCookie("cod_logado"),
                                esc_logado: getCookie("esc_logado")
                            }, function(data) {
                                $('#dropdown-menu1').find('.list-group').empty();
                                $('#dropdown-menu1').find('.list-group').append(data);
                                $('#dropdown_li1 .list-group-item').each(function() {
                                    clickListaMsg($(this));
                                });
                                $('#dropdown-toggle1').find('b').remove();
                                if ($('.msg-no-vis').length > 0) {
                                    $('#dropdown-toggle1').append('<b class="badge badge-notes bg-danger">' + $('.msg-no-vis').length + '</b>');
                                }
                            });
                        }
                );
            } else if (chat_perfil == 2) {
                $.post('controller/user_ajax_requests.php', {
                    actions_perfil: 0,
                    cod_user_perfil: href,
                    esc_logado: getCookie("esc_logado")
                }, function(data) {
                    $('#perfil_modal .about').remove();
                    $('#perfil_modal .personal-info').remove();
                    $('#perfil_modal .form-buttons').hide();
                    $('#perfil_modal .form-section').append(data);
                });
                $('#header').css('z-index', '-10');
                var maskHeight = $(document).height();
                var maskWidth = $(window).width();
                $('.mask_modal').css({'width': maskWidth, 'height': maskHeight});
                $('.mask_modal').fadeIn(500);
                $('#perfil_modal').fadeIn(1000);
            }
        }
        return false;
    });
}

setInterval(function() {

    if (getCookie("cod_logado") && getCookie("esc_logado")) {

        $.post("controller/user_ajax_requests.php",
                'user_logado=' + getCookie("cod_logado") + '&esc_logado=' + getCookie("esc_logado"),
                function(data) {
                    if (data == 0) {
                        document.cookie = "cod_logado=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
                        document.cookie = "esc_logado=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
                        location.assign(BASE_URL);
                    } else {
                        var split = data.split('///');

                        $('#dropdown-menu1').find('.list-group').empty();
                        $('#dropdown-menu1').find('.list-group').append(split[1]);
                        $('#dropdown_li1 .list-group-item').each(function() {
                            clickListaMsg($(this));
                        });
                        $('#dropdown-toggle1').find('b').remove();
                        if ($('.msg-no-vis').length > 0) {
                            $('#dropdown-toggle1').append('<b class="badge badge-notes bg-danger">' + $('.msg-no-vis').length + '</b>');
                        }
                        if ($('.msg-alert').length > 0) {
                            var classe = '';
                            if ($('#icone_mensagem').hasClass('open1')) {
                                var classe = 'open1';
                                $('#icone_mensagem').removeClass('open1');
                            }
                            $('#icone_mensagem').addClass('open1-alert');
                            setTimeout(function() {
                                $('#icone_mensagem').removeClass('open1-alert');
                                setTimeout(function() {
                                    $('#icone_mensagem').addClass('open1-alert');
                                    setTimeout(function() {
                                        $('#icone_mensagem').removeClass('open1-alert');
                                        setTimeout(function() {
                                            $('#icone_mensagem').addClass('open1-alert');
                                            setTimeout(function() {
                                                $('#icone_mensagem').removeClass('open1-alert');
                                                $('#icone_mensagem').addClass(classe);
                                            }, 500);
                                        }, 500);
                                    }, 500);
                                }, 500);
                            }, 500);
                            $('.list-group-item').removeClass('msg-alert');
                        }

                        $('#dropdown-menu2').find('.list-group').empty();
                        $('#dropdown-menu2').find('.list-group').append(split[0]);
                        $('#dropdown_li2 .list-group-item').each(function() {
                            clickListaNot($(this));
                        });
                        $('#dropdown-toggle2').find('b').remove();
                        if ($('.not-no-vis').length > 0) {
                            $('#dropdown-toggle2').append('<b class="badge badge-notes bg-danger">' + $('.not-no-vis').length + '</b>');
                        }
                        if ($('.not-alert').length > 0) {
                            var classe = '';
                            if ($('#icone_notificacao').hasClass('open2')) {
                                var classe = 'open2';
                                $('#icone_notificacao').removeClass('open2');
                            }
                            $('#icone_notificacao').addClass('open2-alert');
                            setTimeout(function() {
                                $('#icone_notificacao').removeClass('open2-alert');
                                setTimeout(function() {
                                    $('#icone_notificacao').addClass('open2-alert');
                                    setTimeout(function() {
                                        $('#icone_notificacao').removeClass('open2-alert');
                                        setTimeout(function() {
                                            $('#icone_notificacao').addClass('open2-alert');
                                            setTimeout(function() {
                                                $('#icone_notificacao').removeClass('open2-alert');
                                                $('#icone_notificacao').addClass(classe);
                                            }, 500);
                                        }, 500);
                                    }, 500);
                                }, 500);
                            }, 500);
                            $('.list-group-item').removeClass('not-alert');
                        }
                    }
                }
        );
    } else {
        location.assign(BASE_URL);
    }

    $('.size1').each(function() {
        var this_box = $(this);
        var id = this_box.attr('id');
        var destino = id.substr(4);
        $.post('controller/user_ajax_requests.php', {
            usuario_dest: destino,
            cod_logado: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            var posicao = data.lastIndexOf('///');
            var content_html = data.substr(0, posicao);
            var icone_status = data.substr(posicao + 3);
            this_box.find('.img_online_box').attr('src', icone_status);
            this_box.find('.content_box').empty();
            this_box.find('.content_box').prepend(content_html);
        });
    });

    $('.size2, .size3').each(function() {
        var this_box = $(this);
        var id = this_box.attr('id');
        var destino = id.substr(4);
        $.post('controller/user_ajax_requests.php', {
            sala_dest: destino,
            cod_logado: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            this_box.find('.content_box').empty();
            this_box.find('.content_box').prepend(data);
        });
    });

    $('.img_online_list').each(function() {
        var this_img = $(this);
        var id = $(this).parent().parent().attr('href');
        $.post('controller/user_ajax_requests.php', {
            check_users_online: id,
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            this_img.attr('src', data);
        });
    });

    $.post('controller/user_ajax_requests.php', {
        upd_salas: getCookie("cod_logado"),
        esc_logado: getCookie("esc_logado")
    }, function(data) {
        $('#dropdown-menu4').find('.list-group').empty();
        $('#dropdown-menu4').find('.list-group').append(data);
        $('#dropdown_li4 .list-group-item').each(function() {
            clickListaSala($(this));
        });
    });

    $.post('controller/user_ajax_requests.php', {
        upd_users: getCookie("cod_logado"),
        esc_logado: getCookie("esc_logado")
    }, function(data) {
        $('#dropdown-menu5').find('.list-group').empty();
        $('#dropdown-menu5').find('.list-group').append(data);
        $('#dropdown_li5 .list-group-item').each(function() {
            clickUserList($(this));
        });
    });

}, 5000);

$(document).ready(function() {

    if (getCookie("cod_janelas")) {

    } else {
        $.post("controller/user_ajax_requests.php", {
            salas_default: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            var split = data.split('///');
            var $this = split[0];
            $.post("controller/user_ajax_requests.php", {
                id_sala: $this,
                tamanho: 1,
                cod_logado: getCookie("cod_logado"),
                esc_logado: getCookie("esc_logado")
            },
            function(data) {
                var posicao = data.indexOf('///');
                var box_html = data.substr(0, posicao);
                var content_html = data.substr(posicao + 3);
                $('.boxes').prepend(box_html);
                criarBox('#sala' + $this);
                $('#sala' + $this).find('.content_box').prepend(content_html);
                $('#sala' + $this).find('.sala_input').focus();
                $('#sala' + $this).css('margin-left', '-40px');
            });
        });
        $.post("controller/user_ajax_requests.php", {
            salas_default: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            var split = data.split('///');
            var $this = split[1];
            $.post("controller/user_ajax_requests.php", {
                id_sala: $this,
                tamanho: 1,
                cod_logado: getCookie("cod_logado"),
                esc_logado: getCookie("esc_logado")
            },
            function(data) {
                var posicao = data.indexOf('///');
                var box_html = data.substr(0, posicao);
                var content_html = data.substr(posicao + 3);
                $('.boxes').prepend(box_html);
                criarBox('#sala' + $this);
                $('#sala' + $this).find('.content_box').prepend(content_html);
                $('#sala' + $this).find('.sala_input').focus();
                $('#sala' + $this).css('position', 'absolute');
                $('#sala' + $this).css('left', '280px');
            });
        });
        $.post("controller/user_ajax_requests.php", {
            salas_default: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            var split = data.split('///');
            var $this = split[2];
            $.post("controller/user_ajax_requests.php", {
                id_sala: $this,
                tamanho: 1,
                cod_logado: getCookie("cod_logado"),
                esc_logado: getCookie("esc_logado")
            },
            function(data) {
                var posicao = data.indexOf('///');
                var box_html = data.substr(0, posicao);
                var content_html = data.substr(posicao + 3);
                $('.boxes').prepend(box_html);
                criarBox('#sala' + $this);
                $('#sala' + $this).find('.content_box').prepend(content_html);
                $('#sala' + $this).find('.sala_input').focus();
                $('#sala' + $this).css('position', 'absolute');
                $('#sala' + $this).css('left', '600px');
            });
        });
    }

    if ($('#pesquisa').val() != '') {
        $.post('controller/user_ajax_requests.php', {
            pesquisa: $('#pesquisa').val(),
            cod_logado: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            $('#dropdown-menu6').empty();
            $('#dropdown-menu6').append(data);
        });
    }

    resizePanel();

    $(window).resize(function() {
        resizePanel();
    });

    $('select').chosen();
    $('.chzn-container').css('width', '100%');
    $('.chzn-drop').css('width', '100%');
    $('.chzn-drop').find('input').css('width', '100%');

    $('input:radio[name="tipo_sala_radio"]').change(function() {
        $(this).parent().parent().parent().siblings('#hidden_radio_sala').val($(this).val());
    });

    $('#perfil_form #pais_sel').chosen().change(function() {
        if ($(this).val() != 0) {
            $.post('controller/user_ajax_requests.php', {
                id_pais: $(this).val(),
                esc_logado: getCookie("esc_logado")
            }, function(data) {
                $('#perfil_form #state_sel').empty();
                $('#perfil_form #state_sel').append(data);
                $('#perfil_form #state_sel').trigger('liszt:updated');
            });
        } else {
            $('#perfil_form #state_sel').empty();
            $('#perfil_form #state_sel').append("<option></option><option value='0'>N�o informar</option>");
            $('#perfil_form #state_sel').trigger('liszt:updated');
            $('#perfil_form #city_sel').empty();
            $('#perfil_form #city_sel').append("<option></option><option value='0'>N�o informar</option>");
            $('#perfil_form #city_sel').trigger('liszt:updated');
        }

    });

    $('#perfil_form #state_sel').chosen().change(function() {
        if ($(this).val() != 0) {
            $.post('controller/user_ajax_requests.php', {
                id_estado: $(this).val(),
                esc_logado: getCookie("esc_logado")
            }, function(data) {
                $('#perfil_form #city_sel').empty();
                $('#perfil_form #city_sel').append(data);
                $('#perfil_form #city_sel').trigger('liszt:updated');
            });
        } else {
            $('#perfil_form #city_sel').empty();
            $('#perfil_form #city_sel').append("<option></option><option value='0'>N�o informar</option>");
            $('#perfil_form #city_sel').trigger('liszt:updated');
        }

    });

    $('#sala_form #cursos_sala_sel').chosen().change(function() {
        $(this).siblings('input').val($(this).val());
        $.post('controller/user_ajax_requests.php', {
            id_curso_sala: $(this).val(),
            cod_logado: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            $('#sala_form .option-group:eq(1)').empty();
            $('#sala_form .option-group:eq(1)').append(data);
        });
    });

    criaValidacao('inicia_letras', MSG_INICIA_LETRAS);
    criaValidacao('nome_sala', 'Only letters and numbers');
    criaValidacao('nome', 'Only letters');

    jQuery.extend(jQuery.validator.messages, {
        required: MSG_REQUIRED,
        email: MSG_EMAIL,
        number: MSG_NUMEROS
    });

    $('input[type=file]').change(function() {
        var this_val = $(this).val();
        var posicao = this_val.lastIndexOf("\\");
        var this_val = this_val.substr(posicao + 1);
        $(this).parent().siblings('input').val(this_val);
    });

    var files;
    $('#sala_form input[type=file]').on('change', prepareUpload1);
    function prepareUpload1(event) {
        files = event.target.files;
    }

    $('#sala_form').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.siblings('b').append(error);
            element.siblings('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {
            var data = new FormData();
            $.each(files, function(key, value) {
                data.append(key, value);
            });
            var dados = $('#sala_form').serialize().replace('&users_sala', '&users_add');
            $.ajax({
                url: 'controller/user_ajax_requests.php',
                type: 'POST',
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data == 'T') {
                        $('#input_icone_sala').next('b').append('<em>' + 'The file is not an image' + '</em>');
                        $('#input_icone_sala').next('b').css('display', 'block');
                        $('#input_icone_sala').focus();
                        $('#input_icone_sala').parent().click(function() {
                            $('#input_icone_sala').next('b').css('display', 'none');
                            $('#input_icone_sala').next('b').find('em').remove();
                        });
                    } else if (data == 'S') {
                        $('#input_icone_sala').next('b').append('<em>' + 'File too large (max. 1mb)' + '</em>');
                        $('#input_icone_sala').next('b').css('display', 'block');
                        $('#input_icone_sala').focus();
                        $('#input_icone_sala').parent().click(function() {
                            $('#input_icone_sala').next('b').css('display', 'none');
                            $('#input_icone_sala').next('b').find('em').remove();
                        });
                    } else {
                        dados = dados.replace('&users_sala=', '/');
                        dados += '&extensao=' + data;
                        $.post("controller/user_ajax_requests.php",
                            dados,
                            function(dado) {
                                $.post('controller/user_ajax_requests.php', {
                                    upd_salas: getCookie("cod_logado"),
                                    esc_logado: getCookie("esc_logado")
                                }, function(data) {
                                    $('#dropdown-menu4').find('.list-group').empty();
                                    $('#dropdown-menu4').find('.list-group').append(data);
                                    $('#dropdown_li4 .list-group-item').each(function() {
                                        clickListaSala($(this));
                                    });
                                });
                                $.fallr({
                                    content: 'Room successfully registered! Check your list of rooms.',
                                    zIndex: 21000
                                });
                                $('#fallr-button-button1').click(function() {
                                    setTimeout(function() {
                                        $('#header').css('z-index', '999');
                                    }, 1000);
                                    $('.mask_modal').fadeOut(1000);
                                    $('#modal_box').fadeOut(500);
                                    $('#sala_form').each(function() {
                                        this.reset();
                                    });
                                    $('#sala_form select').trigger('liszt:updated');
                                });
                            });
                    }
                }
            });
        },
        rules: {
            nome_sala: {
                required: true,
                inicia_letras: "^[a-zA-Z]",
                nome_sala: "^[a-zA-Z0-9������������������������ ]+$"
            },
            hidden_salas: {
                required: true
            },
            hidden_radio_sala: {
                required: true
            },
            input_icone_sala: {
                required: true
            }
        },
        messages: {
            hidden_salas: {
                required: "Select the course"
            },
            hidden_radio_sala: {
                required: "Select a room type"
            },
            input_icone_sala: {
                required: "Select an image"
            }
        }
    });

    var files2;
    $('#perfil_form input[type=file]').on('change', prepareUpload2);
    function prepareUpload2(event) {
        files2 = event.target.files;
    }

    $('#perfil_form').validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            element.siblings('b').append(error);
            element.siblings('b').css('display', 'block');
        },
        success: function(label) {
            label.parent().css('display', 'none');
            label.remove();
        },
        submitHandler: function() {
            var dados = $('#perfil_form').serialize();
            if ($('#input_img_perfil').val() != '') {
                var data = new FormData();
                $.each(files2, function(key, value) {
                    data.append(key, value);
                });
                $.ajax({
                    url: 'controller/user_ajax_requests.php',
                    type: 'POST',
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data == 'T') {
                            $('#input_img_perfil').next('b').append('<em>' + 'The file is not an image' + '</em>');
                            $('#input_img_perfil').next('b').css('display', 'block');
                            $('#input_img_perfil').focus();
                            $('#input_img_perfil').parent().click(function() {
                                $('#input_img_perfil').next('b').css('display', 'none');
                                $('#input_img_perfil').next('b').find('em').remove();
                            });
                        } else if (data == 'S') {
                            $('#input_img_perfil').next('b').append('<em>' + 'File too large (max. 1mb)' + '</em>');
                            $('#input_img_perfil').next('b').css('display', 'block');
                            $('#input_img_perfil').focus();
                            $('#input_img_perfil').parent().click(function() {
                                $('#input_img_perfil').next('b').css('display', 'none');
                                $('#input_img_perfil').next('b').find('em').remove();
                            });
                        } else {
                            dados += '&extensao=' + data;
                            $.post("controller/user_ajax_requests.php",
                                dados,
                                function(dado) {
                                    $.post('controller/user_ajax_requests.php', {
                                        actions_perfil: 1,
                                        cod_logado: getCookie("cod_logado"),
                                        esc_logado: getCookie("esc_logado")
                                    }, function(data) {
                                        $('#perfil_modal .about').remove();
                                        $('#perfil_modal .personal-info').remove();
                                        $('#perfil_modal .form-buttons').show();
                                        $('#perfil_modal .form-section').append(data);
                                    });
                                    $.post('controller/user_ajax_requests.php', {
                                        foto_nome_user: 1,
                                        cod_logado: getCookie("cod_logado"),
                                        esc_logado: getCookie("esc_logado")
                                    }, function(data) {
                                        $('#dropdown-toggle3').empty();
                                        $('#dropdown-toggle3').append(data);
                                    });
                                    $.fallr({
                                        content: 'Profile successfully updated!',
                                        zIndex: 21000
                                    });
                                    $('#fallr-button-button1').click(function() {
                                        $('#edit_perfil_modal').fadeOut(500, function() {
                                            $('#perfil_modal').fadeIn(500);
                                            $('#perfil_form').each(function() {
                                                this.reset();
                                            });
                                        });
                                    });
                                });
                        }
                    }
                });
            } else {
                $.post("controller/user_ajax_requests.php",
                    dados,
                    function(dado) {
                        $.post('controller/user_ajax_requests.php', {
                            actions_perfil: 1,
                            cod_logado: getCookie("cod_logado"),
                            esc_logado: getCookie("esc_logado")
                        }, function(data) {
                            $('#perfil_modal .about').remove();
                            $('#perfil_modal .personal-info').remove();
                            $('#perfil_modal .form-buttons').show();
                            $('#perfil_modal .form-section').append(data);
                        });
                        $.post('controller/user_ajax_requests.php', {
                            foto_nome_user: 1,
                            cod_logado: getCookie("cod_logado"),
                            esc_logado: getCookie("esc_logado")
                        }, function(data) {
                            $('#dropdown-toggle3').empty();
                            $('#dropdown-toggle3').append(data);
                        });
                        $.fallr({
                            content: 'Profile successfully updated!',
                            zIndex: 21000
                        });
                        $('#fallr-button-button1').click(function() {
                            $('#edit_perfil_modal').fadeOut(500, function() {
                                $('#perfil_modal').fadeIn(500);
                                $('#perfil_form').each(function() {
                                    this.reset();
                                });
                            });
                        });
                    });
            }
        },
        rules: {
            nome_perfil: {
                required: true,
                nome: "^[a-zA-Z������������������������ ]+$"
            },
            sobrenome_perfil: {
                required: true,
                nome: "^[a-zA-Z������������������������ ]+$"
            },
            apelido_perfil: {
                nome: "^[a-zA-Z0-9������������������������ ]+$"
            },
            sobre_perfil: {
                nome: "^[a-zA-Z0-9������������������������ ]+$"
            },
            ocupacao_perfil: {
                nome: "^[a-zA-Z������������������������ ]+$"
            },
            email_perfil: {
                email: true
            },
            site_perfil: {
                url: true
            }
        },
        messages: {
            site_perfil: {
                url: 'Please enter a valid URL (http://...)'
            }
        }
    });

    if ($('.msg-no-vis').length != 0) {
        $('#dropdown-toggle1').append('<b class="badge badge-notes bg-danger">' + $('.msg-no-vis').length + '</b>');
    }
    if ($('.not-no-vis').length != 0) {
        $('#dropdown-toggle2').append('<b class="badge badge-notes bg-danger">' + $('.not-no-vis').length + '</b>');
    }
    $('.list-group-item').removeClass('.msg-alert');
    $('.list-group-item').removeClass('.not-alert');

    $('#dropdown-menu1').offset({left: ($('#dropdown_li1').offset().left - 5)});
    $('#dropdown-menu2').offset({left: ($('#dropdown_li2').offset().left - 5)});
    $('#dropdown-menu4').offset({left: ($('#dropdown_li4').offset().left - 5)});
    $('#dropdown-menu5').offset({left: ($('#dropdown_li5').offset().left - 5)});
    $('#dropdown-menu6').offset({left: ($('#pesquisa').offset().left + 22)});

    $(document).mousedown(function(e) {

        if (e.which === 1) {

            var alvo = $('#div_pesquisa');
            if ($('#pesquisa').is(e.target) && $('#pesquisa').val() != '') {
                $('#dropdown-menu6').fadeIn();
            } else if (alvo.has(e.target).length !== 0) {
            } else {
                $('#dropdown-menu6').fadeOut();
            }

            var alvo = $('#dropdown-toggle1');
            if ($('#dropdown-menu1').has(e.target).length !== 0) {
            } else if (!alvo.is(e.target) && alvo.has(e.target).length === 0) {
                $('#dropdown-menu1').slideUp();
                alvo.find('i').removeClass("open1");
            } else {
                $('#dropdown-menu1').slideToggle();
                alvo.find('i').toggleClass("open1");
            }

            var alvo = $('#dropdown-toggle2');
            if ($('#dropdown-menu2').has(e.target).length !== 0) {
            } else if (!alvo.is(e.target) && alvo.has(e.target).length === 0) {
                $('#dropdown-menu2').slideUp();
                alvo.find('i').removeClass("open2");
            } else {
                $('#dropdown-menu2').slideToggle();
                alvo.find('i').toggleClass("open2");
            }

            var alvo = $('#dropdown-toggle4');
            if ($('#dropdown-menu4').has(e.target).length !== 0) {
            } else if (!alvo.is(e.target) && alvo.has(e.target).length === 0) {
                $('#dropdown-menu4').slideUp();
                alvo.find('i').removeClass("open4");
            } else {
                $('#dropdown-menu4').slideToggle();
                alvo.find('i').toggleClass("open4");
            }

            var alvo = $('#dropdown-toggle5');
            if ($('#dropdown-menu5').has(e.target).length !== 0) {
            } else if (!alvo.is(e.target) && alvo.has(e.target).length === 0) {
                $('#dropdown-menu5').slideUp();
                alvo.find('i').removeClass("open5");
            } else {
                $('#dropdown-menu5').slideToggle();
                alvo.find('i').toggleClass("open5");
            }

            var alvo = $('#dropdown-toggle3');
            if ($('#dropdown-menu3').has(e.target).length !== 0) {
            } else if (!alvo.is(e.target) && alvo.has(e.target).length === 0) {
                $('#dropdown-menu3').slideUp();
                $('#dropdown_li3').removeClass("open3");
                $('#dropdown_li3').find('b').removeClass("caret_open");
            }
            else {
                $('#dropdown-menu3').slideToggle();
                $('#dropdown_li3').toggleClass("open3");
                $('#dropdown_li3').find('b').toggleClass("caret_open");
            }
        }
    });

    $('#link_box_3').click(function() {
        if (!$(this).hasClass('size1_selected')) {
            $(this).parent().attr("id", "1");
            $(this).addClass('size1_selected');
            $('#link_box_2').removeClass("size2_selected");
        }
    });

    $('#link_box_2').click(function() {
        if (!$(this).hasClass('size2_selected')) {
            $(this).parent().attr("id", "2");
            $(this).addClass('size2_selected');
            $('#link_box_3').removeClass("size1_selected");
        }
    });

    $('#link_box_5').click(function() {
        if (!$(this).hasClass('chat_selected')) {
            $(this).parent().attr("id", "1");
            $(this).addClass('chat_selected');
            $('#link_box_4').removeClass("profile_selected");
        }
    });

    $('#link_box_4').click(function() {
        if (!$(this).hasClass('profile_selected')) {
            $(this).parent().attr("id", "2");
            $(this).addClass('profile_selected');
            $('#link_box_5').removeClass("chat_selected");
        }
    });

    $('#link_box_6').click(function() {
        $.post('controller/user_ajax_requests.php', {
            cod_upd_nots: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            $('#dropdown-menu2').find('.list-group').empty();
            $('#dropdown-menu2').find('.list-group').append(data);
            $('#dropdown_li2 .list-group-item').each(function() {
                clickListaNot($(this));
            });
            $('#dropdown-toggle2').find('b').remove();
            if ($('.not-no-vis').length > 0) {
                $('#dropdown-toggle2').append('<b class="badge badge-notes bg-danger">' + $('.not-no-vis').length + '</b>');
            }

            if ($('.not-alert').length > 0) {
                var classe = '';
                if ($('#icone_notificacao').hasClass('open2')) {
                    var classe = 'open2';
                    $('#icone_notificacao').removeClass('open2');
                }
                $('#icone_notificacao').addClass('open2-alert');
                setTimeout(function() {
                    $('#icone_notificacao').removeClass('open2-alert');
                    setTimeout(function() {
                        $('#icone_notificacao').addClass('open2-alert');
                        setTimeout(function() {
                            $('#icone_notificacao').removeClass('open2-alert');
                            setTimeout(function() {
                                $('#icone_notificacao').addClass('open2-alert');
                                setTimeout(function() {
                                    $('#icone_notificacao').removeClass('open2-alert');
                                    $('#icone_notificacao').addClass(classe);
                                }, 500);
                            }, 500);
                        }, 500);
                    }, 500);
                }, 500);
                $('.list-group-item').removeClass('not-alert');
            }
        });
    });

    $('#link_box_1').click(function() {
        var alvo = $('#dropdown-toggle4');
        $('#dropdown-menu4').slideUp();
        alvo.find('i').removeClass("open4");

        $('#header').css('z-index', '-10');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        $('.mask_modal').css({'width': maskWidth, 'height': maskHeight});
        $('.mask_modal').fadeIn(500);
        $('#modal_box').fadeIn(1000);
        setTimeout(function() {
            $('#nome_sala').focus();
        }, 1000);
    });

    $('#my_profile_link').click(function() {
        $('#dropdown-menu3').slideUp();
        $('#dropdown_li3').removeClass("open3");
        $('#dropdown_li3').find('b').removeClass("caret_open");

        $.post('controller/user_ajax_requests.php', {
            actions_perfil: 1,
            cod_logado: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            $('#perfil_modal .about').remove();
            $('#perfil_modal .personal-info').remove();
            $('#perfil_modal .form-buttons').show();
            $('#perfil_modal .form-section').append(data);
        });

        $('#header').css('z-index', '-10');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        $('.mask_modal').css({'width': maskWidth, 'height': maskHeight});
        $('.mask_modal').fadeIn(500);
        $('#perfil_modal').fadeIn(1000);
    });

    $('#edit_perfil_modal button:eq(0)').click(function() {
        $('#edit_perfil_modal').fadeOut(500, function() {
            $('#perfil_modal').fadeIn(500);
        });
        return false;
    });

    $('#perfil_modal .btn').click(function() {
        $.post('controller/user_ajax_requests.php', {
            cod_sel_perfil: getCookie("cod_logado"),
            esc_logado: getCookie("esc_logado")
        }, function(data) {
            var dados = data.split('///');
            $('#nome_perfil').val(dados[0]);
            $('#sobrenome_perfil').val(dados[1]);
            $('#apelido_perfil').val(dados[2]);
            $('#sobre_perfil').val(dados[4]);
            var split_data = dados[5].split('-');
            $('#dia_sel').val(split_data[2]);
            $('#dia_sel option[value=' + split_data[2] + ']').select();
            $('#dia_sel').trigger('liszt:updated');
            $('#mes_sel').val(split_data[1]);
            $('#mes_sel option[value=' + split_data[1] + ']').select();
            $('#mes_sel').trigger('liszt:updated');
            $('#ano_sel').val(split_data[0]);
            $('#ano_sel option[value=' + split_data[0] + ']').select();
            $('#ano_sel').trigger('liszt:updated');
            $('#sexo_sel').val(dados[7]);
            $('#sexo_sel option[value=' + dados[7] + ']').select();
            $('#sexo_sel').trigger('liszt:updated');
            $('#ocupacao_perfil').val(dados[6]);
            $('#email_perfil').val(dados[11]);
            $('#site_perfil').val(dados[12]);
            $('#pais_sel').val(dados[8]);
            $('#pais_sel option[value=' + dados[8] + ']').select();
            $('#pais_sel').trigger('liszt:updated');
            $('#state_sel').val(dados[9]);
            $('#state_sel option[value=' + dados[9] + ']').select();
            $('#state_sel').trigger('liszt:updated');
            $('#city_sel').val(dados[10]);
            $('#city_sel option[value=' + dados[10] + ']').select();
            $('#city_sel').trigger('liszt:updated');
        });
        $('#perfil_modal').fadeOut(500, function() {
            $('#edit_perfil_modal').fadeIn(500);
            $('#nome_perfil').focus();
        });
    });

    $('#pesquisa').keydown(function() {
        setTimeout(function() {
            if ($('#pesquisa').val() == '') {
                $('#dropdown-menu6').fadeOut();
            } else {
                $.post('controller/user_ajax_requests.php', {
                    pesquisa: $('#pesquisa').val(),
                    cod_logado: getCookie("cod_logado"),
                    esc_logado: getCookie("esc_logado")
                }, function(data) {
                    $('#dropdown-menu6').empty();
                    $('#dropdown-menu6').append(data);
                    $('#dropdown-menu6 .list-group-item').each(function() {
                        if ($(this).hasClass("pesq_user")) {
                            clickUserPesq($(this));
                        } else {
                            clickSalaPesq($(this));
                        }
                    });
                });
                $('#dropdown-menu6').fadeIn();
            }
        }, 100);
    });

    $('.mask_modal').click(function() {
        setTimeout(function() {
            $('#header').css('z-index', '999');
        }, 1000);
        $(this).fadeOut(1000);
        $('#modal_box').fadeOut(500);
        $('#perfil_modal').fadeOut(500);
        $('#edit_perfil_modal').fadeOut(500);
    });

    $('.close_modal').click(function() {
        setTimeout(function() {
            $('#header').css('z-index', '999');
        }, 1000);
        $('.mask_modal').fadeOut(1000);
        $('#modal_box').fadeOut(500);
        $('#perfil_modal').fadeOut(500);
        $('#edit_perfil_modal').fadeOut(500);
    });

    $('#dropdown_li5 .list-group-item').each(function() {
        clickUserList($(this));
    });

    $('#dropdown_li4 .list-group-item').each(function() {
        clickListaSala($(this));
    });

    $('#dropdown_li1 .list-group-item').each(function() {
        clickListaMsg($(this));
    });

    $('#dropdown_li2 .list-group-item').each(function() {
        clickListaNot($(this));
    });

    $('body').delegate('.chat_input', 'keydown', function(e) {
        var campo = $(this);
        var mensagem = campo.val();
        var this_box = campo.parent().parent().parent().parent();
        var id = campo.parent().parent().parent().parent().attr('id');
        var to = id.substr(4);
        if (e.keyCode == 13) {
            if (mensagem != '') {
                campo.val('');
                $.post('controller/user_ajax_requests.php', {
                    mensagem: mensagem,
                    para_usuario: to,
                    cod_logado: getCookie("cod_logado"),
                    esc_logado: getCookie("esc_logado")
                }, function(data) {
                    this_box.find('.content_box').empty();
                    this_box.find('.content_box').prepend(data);
                });
            }
        }
    });

    $('body').delegate('.sala_input', 'keydown', function(e) {
        var campo = $(this);
        var mensagem = campo.val();
        var this_box = campo.parent().parent().parent().parent();
        var id = campo.parent().parent().parent().parent().attr('id');
        var to = id.substr(4);
        var input_file_sala = this_box.find('#input_file_sala');

        if (e.keyCode == 13) {
            if (input_file_sala.val() != '') {
                var data = new FormData();
                $.each(files_sala[to], function(key, value) {
                    data.append(key, value);
                });
                $.ajax({
                    url: 'controller/user_ajax_requests.php?sala',
                    type: 'POST',
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data == 'T') {
                            input_file_sala.next('b').append('<em>' + 'Somente arquivos de imagem ou texto' + '</em>');
                            input_file_sala.next('b').css('display', 'block');
                            input_file_sala.focus();
                            input_file_sala.focusout(function() {
                                input_file_sala.next('b').css('display', 'none');
                                input_file_sala.next('b').find('em').fadeOut("slow", function() {
                                    input_file_sala.next('b').find('em').remove();
                                });
                            });
                        } else if (data == 'S') {
                            input_file_sala.next('b').append('<em>' + 'Arquivo muito grande(max. 10mb)' + '</em>');
                            input_file_sala.next('b').css('display', 'block');
                            input_file_sala.focus();
                            input_file_sala.focusout(function() {
                                input_file_sala.next('b').css('display', 'none');
                                input_file_sala.next('b').find('em').fadeOut("slow", function() {
                                    input_file_sala.next('b').find('em').remove();
                                });
                            });
                        } else {
                            campo.val('');
                            input_file_sala.val('');
                            $.post('controller/user_ajax_requests.php', {
                                post: mensagem,
                                sala_post: to,
                                extensao: data,
                                cod_logado: getCookie("cod_logado"),
                                esc_logado: getCookie("esc_logado")
                            }, function(data) {
                                this_box.find('.content_box').empty();
                                this_box.find('.content_box').prepend(data);
                            });
                        }
                    }
                });
            } else if (mensagem != '') {
                campo.val('');
                $.post('controller/user_ajax_requests.php', {
                    post: mensagem,
                    sala_post: to,
                    cod_logado: getCookie("cod_logado"),
                    esc_logado: getCookie("esc_logado")
                }, function(data) {
                    this_box.find('.content_box').empty();
                    this_box.find('.content_box').prepend(data);
                });
            }
        }
    });

});
(function($) {
    $.fn.drags = function(opt) {

        var originalZ = null,
                thisDrag = this,
                pos = null,
                isEnabled = true,
                dragger, $el, $parents;


        function sortConstraint(x) {
            if (x != null) {
                x.sort();
            }
        }

        function toggleChildEvents(isActive) {

            $this = $(thisDrag);
            var onOff = isActive ? "on" : "off";

            $drag = (opt.handle === "") ?
                    $this.toggleClass('draggable', isActive) :
                    $this.toggleClass('active-handle', isActive).parent().toggleClass('draggable', isActive);

            if (originalZ == null) {
                originalZ = $drag.css('z-index');
            }

            $parents = $drag.css('z-index', isActive ? 1001 : originalZ).parents();
            $parents[onOff]("mousemove", mouseMove);

            $el[onOff]("mouseup", mouseUp);
            $el.css('cursor', isActive || opt.keepCursorOnEnd ? opt.cursor : "default");
        }

        function mouseEnd() {

            $drag.css('z-index', originalZ);
            toggleChildEvents(false);

            if (isEnabled) {
                var movedLeft = (pos.current.left - pos.start.left) < 0;

                if (opt.handle === "") {
                    $(this).removeClass('draggable');
                } else {
                    $(this).removeClass('active-handle').parent().removeClass('draggable');
                }

                if (opt.autoEnd) {
                    dragger.start(false);
                }

                opt.onEndMove({movedLeft: movedLeft})
            }

        }

        function mouseDown(e) {

            if (isEnabled)
            {
                toggleChildEvents(true);
                pos.current = $drag.offset();
                pos.start = pos.current;
                pos.click = {y: e.pageY, x: e.pageX};
            }
            e.preventDefault();     // disable selection 
        }

        function mouseMove(e) {

            pos.current = {
                top: (opt.axis == "x" ? pos.start.top : pos.start.top + e.pageY - pos.click.y),
                left: (opt.axis == "y" ? pos.start.left : pos.start.left + e.pageX - pos.click.x)
            };

            var endMove = (opt.xMaxMove != null && Math.abs(pos.current.left - pos.start.left) > opt.xMaxMove);

            if (opt.xConstrain != null) {
                var xDiff = pos.current.left - pos.reference.left;
                if (xDiff < opt.xConstrain[0]) {
                    pos.current.left = pos.reference.left + opt.xConstrain[0];
                    endMove = true;
                } else if (xDiff > opt.xConstrain[1]) {
                    pos.current.left = pos.reference.left + +opt.xConstrain[1];
                    endMove = true;
                }
            }

            endMove = (endMove || opt.yMaxMove != null && Math.abs(pos.current.top - pos.start.top) > opt.yMaxMove);

            if (opt.yConstrain != null) {
                var yDiff = pos.current.top - pos.reference.top;
                if (yDiff < opt.yConstrain[0]) {
                    pos.current.top = pos.reference.top + opt.yConstrain[0];
                    endMove = true;
                } else if (yDiff > opt.yConstrain[1]) {
                    pos.current.top = pos.reference.top + +opt.yConstrain[1];
                    endMove = true;
                }
            }

            $d = $('.draggable');
            if (endMove) {
                mouseEnd();
            } else {
                $d.offset(pos.current);
            }
        }

        function mouseUp(e) {

            if (opt.onClick != null)
            {
                var d = Math.sqrt(Math.pow(pos.current.left - pos.start.left, 2) + Math.pow(pos.current.top - pos.start.top, 2));
                if (d < 4) {
                    isEnabled = opt.onClick();
                    return;
                }
            }
            $(this).removeClass('draggable').css('z-index', originalZ);
            mouseEnd();
        }

        dragger = {
            start: function(start, newPos) {

                if (pos == null) {
                    var cc = $el.offset();
                    pos = {start: cc, current: cc, reference: cc};
                }

                if (typeof newPos != "undefined") {
                    pos = $.extend(pos, newPos);
                }

                $el[start ? "on" : "off"]("mousedown", mouseDown);
                if (!start) {
                    toggleChildEvents(false);
                }
            },
            setEnabled: function(enabled) {
                isEnabled = enabled;
                if (isEnabled) {
                    pos.current = $drag.offset();
                } else {
                    toggleChildEvents(isEnabled);
                }
            }
        }

        opt = $.extend({
            handle: "",
            cursor: "move",
            axis: null,
            autoStart: true,
            autoEnd: false,
            xMaxMove: null,
            keepCursorOnEnd: false,
            yMaxMove: null,
            xConstrain: null,
            yConstrain: null,
            onClick: null,
            onEndMove: function() {
            }
        }, opt);

        sortConstraint(opt.xConstrain);
        sortConstraint(opt.yConstrain);

        $el = (opt.handle === "") ? $el = this : this.find(opt.handle);

        $el.dragger = dragger;
        if (opt.autoStart) {
            dragger.start(true);
        }
        return $el

    }
})(jQuery);
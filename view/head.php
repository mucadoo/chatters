<!DOCTYPE html>
<html <?php
if (isset($page_admin)) {
    ?>class="no-js" <?php
    }
    ?>lang="<?php echo sigla ?>">
    <head>

        <meta charset="UTF-8">
        <meta name="description" content="<?php echo $description ?>">
        <meta name="keywords" content="<?php echo $keywords ?>">
        <meta name="author" content="Agência Advanced">
        <meta name="reply-to" content="contato@agenciaadvanced.herobo.com">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <title><?php echo $page; ?></title>

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo BASE_URL ?>/media/favicon.png">

        <?php
        if (isset($page_home)) {
            ?>
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/home_login.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/home_login2.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/font-awesome.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/jquery.chosen.css">

            <script src="<?php echo BASE_URL ?>/script/libraries/jquery-1.11.0.min.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/jquery-migrate-1.2.1.min.js"></script>
            <script src="<?php echo BASE_URL ?>/script/plugins/plax-1.2.min.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.scrollTo-1.4.2.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.chosen-0.9.5.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.validate-1.9.0.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.fallr-1.2.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.scrollpane-3.0.4.js"></script>
            <script src="<?php echo BASE_URL ?>/config/lang/<?php echo sigla ?>.js"></script>
            <script defer src="<?php echo BASE_URL ?>/script/script_home.js"></script>

            <?php
        } else if (isset($page_user)) {
            ?>
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/home_login.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/home_login2.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/font-awesome.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/jquery.chosen.css">

            <script src="<?php echo BASE_URL ?>/script/libraries/jquery-1.11.0.min.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/jquery-migrate-1.2.1.min.js"></script>
            <script src="<?php echo BASE_URL ?>/script/plugins/plax-1.2.min.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.drags.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.chosen-0.9.5.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.validate-1.9.0.js"></script>
            <script src="<?php echo BASE_URL ?>/script/libraries/plugins/jquery.fallr-1.2.js"></script>
            <script src="<?php echo BASE_URL ?>/config/lang/<?php echo sigla ?>.js"></script>
            <script defer src="<?php echo BASE_URL ?>/script/script_user.js"></script>

            <?php
        } else if (isset($page_admin)) {
            ?><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/960gs.fluid.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/h5bp.normalize.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/h5bp.non-semantic.helper.classes.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/h5bp.print.styles.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/admin1.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/admin2.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/admin3.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/home_login2.css">
            <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/style/libraries/jquery-ui-1.8.16.custom.css">

            <script src="<?php echo BASE_URL ?>/script/plugins/modernizr-2.0.6.min.js"></script>
            <?php
        }
        ?>

    </head>
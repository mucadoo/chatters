<?php
require_once 'model/UsuarioDAO.class.php';
$objUsuarioDAO = new UsuarioDAO();
require_once 'model/SalaDAO.class.php';
$objSalaDAO = new SalaDAO();
require_once 'model/NotificacaoDAO.class.php';
$objNotificacaoDAO = new NotificacaoDAO();
require_once 'model/ConversaDAO.class.php';
$objConversaDAO = new ConversaDAO();
?>
<div id="header" class="header">
    <div class="container">
        <img class="logo" src="media/logo.png" alt="">
        <?php if (isset($page_home)) { ?>
            <div class="main-menu">
                <a href="#login_page" class="a_scroll menu_selected"><?php echo login ?></a>
                <a href="#sign_up_page" class="a_scroll"><?php echo cadastro ?></a>
                <a href="#about_page" class="a_scroll"><?php echo sobre ?></a>
                <a href="#terms_page" class="a_scroll"><?php echo termos ?></a>
                <a href="#contact_page" class="a_scroll"><?php echo contato ?></a>
                <a href="#forgot_pass_page" class="a_scroll"></a>
                <a href="#new_pass_page" class="a_scroll"></a>
            </div>
        <?php } else if (isset($page_admin)) {
            ?>
            <div class="main-menu" style="margin-left: 300px">
                <a style="display:none" href="?page=escolas" class="a_scroll <?php echo $selected['escolas'] ?>"><?php echo escolas ?></a>
                <a href="?page=curso_sala" class="a_scroll <?php echo $selected['curso_sala'] ?>"><?php echo salas ?></a>
                <a href="?page=usuarios" class="a_scroll <?php echo $selected['usuarios'] ?>"><?php echo usuarios ?></a>
                <a style="display:none" href="?page=historico" class="a_scroll <?php echo $selected['historico'] ?>"><?php echo historico ?></a>
            </div>
            <ul class="nav navbar-nav navbar-avatar pull-right">
                <a href="index.php?sair"><div id="sair_admin" title="Logout"></div></a>
            </ul>
        <?php } else if (isset($page_user)) { ?>
            <div id="div_pesquisa" style="width: 400px; float: left">
                <div class="chatters-form">
                    <label style="margin: 12px 0 0 50px" class="lbl-ui append-icon">
                        <input style="height: 30px" type="text" id="pesquisa" class="input" placeholder="Search users and rooms">
                        <span style="margin: -2px 0 0 -2px"><i class="fa fa-search"></i></span>
                    </label>
                </div>
                <section id="dropdown-menu6" class="dropdown-menu panel panel-large arrow-top m-l-small"></section>
            </div>
            <ul class="nav navbar-nav navbar-avatar pull-right">
                <li id="dropdown_li3" class="dropdown">
                    <a id="dropdown-toggle3" class="dropdown-toggle" data-toggle="dropdown">
                        <?php echo $objUsuarioDAO->selNomeFoto($conexao, decriptNumberCookie($_COOKIE['cod_logado'])) ?>
                    </a>
                    <ul id="dropdown-menu3" class="dropdown-menu">
                        <li><a id="my_profile_link"><?php echo perfil ?></a></li>
                        <li><a><?php echo configuracoes ?></a></li>
                        <li class="divider"></li>
                        <li><a><?php echo ajuda ?></a></li>
                        <li><a href="?sair"><?php echo sair ?></a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-avatar pull-right" style="margin-right: 20px">
                <li>
                    <div id="dropdown_li4" class="m-t m-b-small">
                        <a id="dropdown-toggle4" class="dropdown-toggle" data-toggle="dropdown">
                            <i id="icone_sala" class="icon-comment-alt icon-xlarge text-default"></i>
                        </a>
                        <section id="dropdown-menu4" class="dropdown-menu panel panel-large arrow-top m-l-small">
                            <header id="1" class="panel-heading bg-white">
                                <span class="h5" style="float: left">
                                    <strong><?php echo salas ?></strong>
                                </span>
                                <span id="link_box_1" title="Create room" style="margin-right: 5px" class="box_icons"></span>
                                <span id="link_box_2" title="Window size - Large" class="box_icons"></span>
                                <span id="link_box_3" title="Window size - Small" class="box_icons size1_selected"></span>
                            </header>
                            <div class="scroll-list list-group list-group-flush m-t-n">
                                <?php echo $objSalaDAO->selSalasToList($conexao, decriptNumberCookie($_COOKIE['cod_logado'])) ?>
                            </div>
                        </section>
                    </div>
                </li>
                <li>
                    <div id="dropdown_li5" class="m-t m-b-small">
                        <a id="dropdown-toggle5" class="dropdown-toggle" data-toggle="dropdown">
                            <i id="icone_usuario" class="icon-comment-alt icon-xlarge text-default"></i>
                        </a>
                        <section id="dropdown-menu5" class="dropdown-menu panel panel-large arrow-top m-l-small">
                            <header id="1" class="panel-heading bg-white">
                                <span class="h5" style="float: left">
                                    <strong><?php echo usuarios ?></strong>
                                </span>
                                <span id="link_box_4" title="Profile" style="margin-right: 5px" class="box_icons"></span>
                                <span id="link_box_5" title="Chat" class="box_icons chat_selected"></span>
                            </header>
                            <div class="scroll-list list-group list-group-flush m-t-n">
                                <?php echo $objUsuarioDAO->selUsersToList($conexao, decriptNumberCookie($_COOKIE['cod_logado'])) ?>
                            </div>
                        </section>
                    </div>
                </li>
                <li>
                    <div id="dropdown_li2" class="m-t m-b-small">
                        <a id="dropdown-toggle2" class="dropdown-toggle" data-toggle="dropdown">
                            <i id="icone_notificacao" class="icon-comment-alt icon-xlarge text-default"></i>
                        </a>
                        <section id="dropdown-menu2" class="dropdown-menu panel panel-large arrow-top m-l-small">
                            <header class="panel-heading bg-white">
                                <span class="h5">
                                    <strong><?php echo notificacoes ?></strong>
                                </span>
                                <span id="link_box_6" title="Mark all as read" style="margin-right: 5px" class="box_icons"></span>
                            </header>
                            <div class="scroll-list list-group list-group-flush m-t-n">
                                <?php echo $objNotificacaoDAO->selNotifytoHTML($conexao, decriptNumberCookie($_COOKIE['cod_logado'])) ?>
                            </div>
                        </section>
                    </div>
                </li>
                <li>
                    <div id="dropdown_li1" class="m-t m-b-small">
                        <a id="dropdown-toggle1" class="dropdown-toggle" data-toggle="dropdown">
                            <i id="icone_mensagem" class="icon-comment-alt icon-xlarge text-default"></i>
                        </a>
                        <section id="dropdown-menu1" class="dropdown-menu panel panel-large arrow-top m-l-small">
                            <header class="panel-heading bg-white">
                                <span class="h5">
                                    <strong><?php echo mensagens ?></strong>
                                </span>
                            </header>
                            <div class="scroll-list list-group list-group-flush m-t-n">
                                <?php echo $objConversaDAO->selMsgHTML($conexao, decriptNumberCookie($_COOKIE['cod_logado'])) ?>
                            </div>
                        </section>
                    </div>
                </li>
            </ul>
        <?php } ?>
    </div>
</div>
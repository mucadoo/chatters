<?php
require_once 'model/CursoDAO.class.php';
require_once 'model/SalaDAO.class.php';

$objUsuarioDAO = new UsuarioDAO();
$objCursoDAO = new CursoDAO();
$objSalaDAO = new SalaDAO();

$acao_get = isset($_GET['acao']) ? $_GET['acao'] : 0;
$id = isset($_GET['id']) ? $_GET['id'] : 0;

$acao_post = isset($_POST['acao']) ? $_POST['acao'] : 0;

$cpf_aluno = isset($_POST['cpf_aluno']) ? $_POST['cpf_aluno'] : "";
$num_matricula_aluno = isset($_POST['num_matricula_aluno']) ? $_POST['num_matricula_aluno'] : "";
$curso_aluno = isset($_POST['curso_aluno']) ? $_POST['curso_aluno'] : "";
$turma_aluno = isset($_POST['turma_aluno']) ? $_POST['turma_aluno'] : "";
$pre_senha_aluno = isset($_POST['pre_senha_aluno']) ? $_POST['pre_senha_aluno'] : "";

$cpf_professor = isset($_POST['cpf_professor']) ? $_POST['cpf_professor'] : "";
$num_registro_professor = isset($_POST['num_registro_professor']) ? $_POST['num_registro_professor'] : "";
$pre_senha_professor = isset($_POST['pre_senha_professor']) ? $_POST['pre_senha_professor'] : "";
$cursos_turmas_professor = isset($_POST['cursos_turmas_professor']) ? $_POST['cursos_turmas_professor'] : "";

$nome_usuario_adm = isset($_POST['nome_usuario_adm']) ? $_POST['nome_usuario_adm'] : "";
$email_adm = isset($_POST['email_adm']) ? $_POST['email_adm'] : "";
$pre_senha_adm = isset($_POST['pre_senha_adm']) ? $_POST['pre_senha_adm'] : "";

if ($acao_post == 14) {
    $pontos = array(".", "-");
    $cpf_aluno = str_replace($pontos, "", $cpf_aluno);
    $objUsuarioDAO->addUsuario($conexao, $cpf_aluno, $num_matricula_aluno, $pre_senha_aluno, $turma_aluno, $curso_aluno, 4, decriptNumberCookie($_COOKIE['esc_logado']));
} else if ($acao_post == 13) {
    $pontos = array(".", "-");
    $cpf_professor = str_replace($pontos, "", $cpf_professor);
    $objUsuarioDAO->addUsuario($conexao, $cpf_professor, $num_registro_professor, $pre_senha_professor, $cursos_turmas_professor, $cursos_turmas_professor, 3, decriptNumberCookie($_COOKIE['esc_logado']));
} else if ($acao_post == 12) {
    $objUsuarioDAO->addAdminnistrador($conexao, $pre_senha_adm, $nome_usuario_adm, $email_adm, decriptNumberCookie($_COOKIE['esc_logado']));
} else if ($acao_get == 3) {
    $objUsuarioDAO->apagaUsuario($conexao, $id);
}
?>
<div role="main" class="container_12" id="content-wrapper" style="width: 1000px; margin: 0 auto; z-index: 2">

    <div class="main_content" style="height: 100%; width: 960px; margin: 0 auto; padding: 20px 0 20px 0">

        <h2 class="grid_12">Registration</h2>
        <div class="clean"></div>

        <div class="grid_12">
            <div class="box closed">
                <div class="header">
                    <img src="media/icones/ui-text-field-format.png" alt="" width="16" height="16">
                    <h3>Student Pre-Registration</h3>
                    <span></span>
                </div>
                <form id="pre_cadastro_aluno" class="validate" novalidate action="?page=usuarios" method="post">
                    <input type="hidden" name="acao" value="14">
                    <div class="content">
                        <div class="_25">
                            <p>
                                <label for="cpf_professor">CPF</label>
                                <input id="cpf_aluno" name="cpf_aluno" type="text" placeholder="999.999.999-99" class="required"/>
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <label for="num_matricula_aluno"> Registration Number</label>
                                <input id="num_matricula_aluno" name="num_matricula_aluno" type="text" class="required"/>
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <label style="margin-bottom: 9px">
                                    Course
                                </label>
                                <select id="sel_curso" data-placeholder="Choose a course..." name="curso_aluno" class="required">
                                    <option></option>
                                    <?php echo $objCursoDAO->comboBoxCursos($conexao) ?>
                                </select>
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <label style="margin-bottom: 9px">
                                    Class
                                </label>
                                <select id="sel_turma" data-placeholder="Choose a class..." name="turma_aluno" class="required">
                                    <option></option>
                                </select>
                            </p>
                        </div>
                        <div id="turmas_hidden" style="display: none">
                            <?php echo $objSalaDAO->comboBoxTurmas($conexao); ?>
                        </div>
                        <div class="clear"></div>
                        <div class="_25">
                            <p>
                                <label for="pre_senha_aluno">Pre-Password</label>
                                <input id="pre_senha_aluno" name="pre_senha_aluno" type="text" required />
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <input  id="gerar_senha_aluno" type="button" value="Generate Pre-Password" style="width:150px; text-align: center; margin-top: 37px">
                            </p>
                        </div>
                    </div>
                    <div class="actions" style="display: none">
                        <div class="actions-left">
                            <input type="reset" value="Reset"/>
                        </div>
                        <div class="actions-right">
                            <input type="submit" value="Register"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="clear"></div>

        <div class="grid_12">
            <div class="box closed">
                <div class="header">
                    <img src="media/icones/ui-text-field-format.png" alt="" width="16" height="16">
                    <h3>Teacher Pre-Registration</h3>
                    <span></span>
                </div>
                <form id="pre_cadastro_prof" class="validate" novalidate action="?page=usuarios" method="post">
                    <input type="hidden" name="acao" value="13">
                    <div class="content">
                        <div class="_25">
                            <p>
                                <label for="cpf_professor">CPF</label>
                                <input id="cpf_professor" name="cpf_professor" type="text" placeholder="999.999.999-99" class="required" />
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <label for="num_registro_professor">Registration Number</label>
                                <input id="num_registro_professor" name="num_registro_professor" type="text" class="required" />
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <label for="pre_senha_professor">Pre-Password</label>
                                <input id="pre_senha_professor" name="pre_senha_professor" type="text" required />
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <input id="gerar_senha_professor" type="button" value="Generate Pre-Password" style="width:150px; text-align: center; margin-top: 37px">
                            </p>
                        </div>
                        <div class="grid_12" style="margin-bottom: 10px">
                            <div id="tab-panel-1">
                                <div class="header" style="background: transparent; border: none">
                                    <h3 style="color: #666; font-family: 'PT Sans', sans-serif;font-size: 12px; text-shadow: none">Courses</h3>
                                    <?php echo $objSalaDAO->selectTurmasProfessor($conexao); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="actions" style="display: none">
                        <div class="actions-left">
                            <input type="reset" value="Reset"/>
                        </div>
                        <div class="actions-right">
                            <input type="submit" value="Register"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="clear"></div>

        <div class="grid_12">
            <div class="box closed">
                <div class="header">
                    <img src="media/icones/ui-text-field-format.png" alt="" width="16" height="16">
                    <h3>Administrator Registration</h3>
                    <span></span>
                </div>
                <form id="pre_cadastro_adm" class="validate" novalidate action="?page=usuarios" method="post">
                    <input type="hidden" name="acao" value="12">
                    <div class="content">
                        <div class="_25">
                            <p>
                                <label for="nome_usuario_adm">Username</label>
                                <input id="nome_usuario_adm" name="nome_usuario_adm" type="text" class="required" />
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <label for="email_adm">Email</label>
                                <input id="email_adm" name="email_adm" type="email" placeholder="example@example.com" class="required" />
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <label for="pre_senha_adm">Pre-Password</label>
                                <input id="pre_senha_adm" name="pre_senha_adm" type="text" required />
                            </p>
                        </div>
                        <div class="_25">
                            <p>
                                <input id="gerar_senha_adm" type="button" value="Generate Pre-Password" style="width:150px; text-align: center; margin-top: 37px">
                            </p>
                        </div>
                    </div>
                    <div class="actions" style="display: none">
                        <div class="actions-left">
                            <input type="reset" value="Reset"/>
                        </div>
                        <div class="actions-right">
                            <input value="Register" type="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="clear"></div>

    </div>

    <div class="main_content" style="height: 100%; width: 960px; margin: 0 auto; padding: 20px 0 20px 0">

        <h2 class="grid_12">List</h2>
        <div class="clear"></div>

        <div class="grid_12">
            <div class="box closed">
                <div class="header">
                    <img src="media/icones/shadeless-table-excel.png" width="16" height="16">
                    <h3>Students</h3><span></span>
                </div>
                <div class="content">
                    <table id="alunos" class="table">
                        <?php echo $objUsuarioDAO->selectUsuarioToHTML($conexao, 4); ?>
                    </table>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="grid_12">
            <div class="box closed">
                <div class="header">
                    <img src="media/icones/shadeless-table-excel.png" width="16" height="16">
                    <h3>Teachers</h3><span></span>
                </div>
                <div class="content">
                    <table id="professores" class="table">
                        <?php echo $objUsuarioDAO->selectUsuarioToHTML($conexao, 3); ?>
                    </table>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div style="display:none" class="grid_12">
            <div class="box closed">
                <div class="header">
                    <img src="media/icones/shadeless-table-excel.png" width="16" height="16">
                    <h3>Administrators</h3><span></span>
                </div>
                <div class="content">
                    <table id="administradores" class="table">
                        <?php echo $objUsuarioDAO->selectUsuarioToHTML($conexao, 2); ?>
                    </table>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="clean"></div>

    </div>
    <div class="clean"></div>

</div>
<div class="clear"></div>
<?php
require_once 'model/CursoDAO.class.php';
require_once 'model/SalaDAO.class.php';

$objCursoDAO = new CursoDAO();
$objSalaDAO = new SalaDAO();

if (isset($_POST['nome_curso'])) {
    $objCursoDAO->insCurso($conexao, $_POST['nome_curso'], decriptNumberCookie($_COOKIE['esc_logado']));
    $objSalaDAO->insSalasDefault($conexao);
} else if (isset($_POST['nome_turma'])) {
    $objSalaDAO->insTurma($conexao, $_POST['sel_cursos'], $_POST['nome_turma']);
}
?>
<div role="main" class="container_12" id="content-wrapper" style="width: 1000px; margin: 0 auto; z-index: 2">

    <div class="main_content" style="height: 100%; width: 960px; margin: 0 auto; padding: 20px 0 20px 0">

        <h2 class="grid_12">Registration</h2>
        <div class="clean"></div>

        <div class="grid_3">
            <div class="box">
                <div class="header">
                    <img src="media/icones/ui-text-field-format.png" alt="" width="16" height="16">
                    <h3>Course Registration</h3>
                    <span></span>
                </div>
                <form id="cad_curso" class="validate" novalidate action="?page=curso_sala" method="post">
                    <div class="content">
                        <div class="_100">
                            <p>
                                <label for="nome_curso">Name:</label>
                                <input id="nome_curso" name="nome_curso" type="text" class="required">
                            </p>
                        </div>
                    </div>
                    <div class="actions">
                        <div class="actions-left">
                            <input type="reset" value="Redefinir"/>
                        </div>
                        <div class="actions-right">
                            <input value="Cadastrar" type="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="grid_9">
            <div class="box">
                <div class="header">
                    <img src="media/icones/ui-text-field-format.png" alt="" width="16" height="16">
                    <h3>Class Registration</h3>
                    <span></span>
                </div>
                <form id="cad_turma" class="validate" novalidate action="?page=curso_sala" method="post">
                    <div class="content">
                        <div class="_50">
                            <p>
                                <label for="nome_turma">Name:</label>
                                <input id="nome_turma" name="nome_turma" type="text" class="required">
                            </p>
                        </div>
                        <div class="_50">
                            <p>
                                <label style="margin-bottom: 9px">Course:</label>
                                <select id="sel_cursos" name="sel_cursos" data-placeholder="Escolha um curso..." class="required">
                                    <option></option>
                                    <?php echo $objCursoDAO->comboBoxCursos($conexao) ?>
                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="actions">
                        <div class="actions-left">
                            <input type="reset" value="Redefinir"/>
                        </div>
                        <div class="actions-right">
                            <input value="Cadastrar" type="submit" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="clear"></div>

        <h2 class="grid_12" style="margin-top: 30px">List</h2>
        <div class="clear"></div>

        <div class="grid_12">
            <div class="box closed">
                <div class="header">
                    <img src="media/icones/shadeless-table-excel.png" width="16" height="16">
                    <h3>Courses</h3><span></span>
                </div>
                <div class="content">
                    <table id="cursos" class="table">
                        <?php echo $objCursoDAO->selCursosToForm($conexao) ?>
                    </table>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="grid_12">
            <div class="box closed">
                <div class="header">
                    <img src="media/icones/shadeless-table-excel.png" width="16" height="16">
                    <h3>Rooms</h3><span></span>
                </div>
                <div class="content">
                    <table id="salas" class="table">
                        <?php echo $objSalaDAO->selSalasToForm($conexao) ?>
                    </table>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="clean"></div>

    </div>
    <div class="clean"></div>

</div>
<div class="clear"></div>

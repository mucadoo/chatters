<?php
$description = "Chatters - Talking Between Us!";
$keywords = "rede, social, escolar, estudantil, chatters, falando, entre, nós, talking, between, us, user";
$page = "Chatters - Painel Administrativo";
$page_admin = TRUE;
include("view/head.php");

if (isset($_GET['page'])) {
    $pagina = $_GET['page'];
    $url = "view/admin/pages/$pagina";
} else {
    $pagina = "curso_sala";
    $url = "view/admin/pages/curso_sala";
}

$selected = Array(
    'escolas' => "",
    'curso_sala' => "",
    'usuarios' => "",
    'historico' => ""
);

$selected[$pagina] = 'menu_selected';
?>

<body style="z-index: -2">

    <div id="height-wrapper">

        <?php
        include_once 'view/header.php';
        include_once $url . ".php";
        ?>

    </div>

    <?php include("view/footer.php"); ?>

</body>
</html>
<div id="modal_box" class="chatters-form wrapper">
    <form id="sala_form" enctype="multipart/form-data">
        <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
        <div class="form-enclose">
            <div class="form-section">
                <span class="close_modal"></span>
                <section>
                    <label class="cxb"><strong><a class="ext">Enter the information for your new room</a></strong></label>
                    <div class="tagline"><span class="fa fa-long-arrow-down"></span></div>
                </section>
                <div class="row">
                    <div class="col6 first">
                        <section>
                            <label for="nome_sala" class="lbl-text">Room name:</label>
                            <label class="lbl-ui append-icon">
                                <input type="text" name="nome_sala" id="nome_sala" class="input" placeholder="Enter the room name">
                                <b style="display: none" class="tooltip right"></b>
                                <span><i class="fa fa-sort-alpha-asc"></i></span>
                            </label>
                        </section>
                        <section>
                            <label for="cursos_sala_sel" class="lbl-text">Room course:</label>
                            <select name="cursos_sala_sel" id="cursos_sala_sel" data-placeholder="Select the course">
                                <option></option>
                                <?php echo $objCursoDAO->cmbCursosUser($conexao, decriptNumberCookie($_COOKIE['cod_logado'])) ?>
                            </select>
                            <input style="position: absolute; z-index: -50" class="input" type="text" name="hidden_salas" id="hidden_salas">
                            <b style="display: none" class="tooltip right seltip"></b>
                        </section>
                        <section>
                            <label for="file" class="lbl-text">Room Icon (JPEG, PNG, GIF):</label>
                            <label for="file" class="lbl-ui file-input">
                                <span class="button">
                                    <input type="file" id="file" name="file">Choose
                                </span>
                                <input type="text" id="input_icone_sala" name="input_icone_sala" class="input" placeholder="Choose a file" readonly="">
                                <b style="display: none" class="tooltip right"></b>
                            </label>
                        </section>
                    </div>
                    <div class="col6 last">
                        <section>
                            <label for="opts2" class="lbl-text spacer">Room type:</label>
                            <input style="position: absolute; z-index: -50;" class="input" type="text" name="hidden_radio_sala" id="hidden_radio_sala">
                            <b style="display: none" class="tooltip right seltip radiotip_sala"></b>
                            <div class="option-group">
                                <span style="cursor: pointer" class="goption">
                                    <label class="options">
                                        <input type="radio" id="radio_visivel" name="tipo_sala_radio" value="V">
                                        <span class="radio"></span>
                                    </label>
                                    <label for="radio_visivel">Visible</label>
                                </span>
                                <span style="cursor: pointer" class="goption">
                                    <label class="options">
                                        <input type="radio" id="radio_secreta" name="tipo_sala_radio" value="I">
                                        <span class="radio"></span>
                                    </label>
                                    <label for="radio_secreta">Secret</label>
                                </span>
                            </div>
                        </section>
                        <section style="margin-top: -10px">
                            <label for="opts1" class="lbl-text spacer">Participating users:</label>
                            <div class="option-group" style="overflow-y: auto; height: 100px;">
                                <label>Select a course first!</label>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-buttons align-right">
            <section>
                <button class="btn btn-info">Create<i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
            </section>
        </div>
    </form>
</div>
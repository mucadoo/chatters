<div id="perfil_modal" class="chatters-form wrapper wide" style="max-width: 900px;">
    <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
    <div class='form-enclose'>
        <div class='form-section' style='height: 250px; padding: 0'>
            <span class='close_modal'></span>
        </div>
    </div>
    <div class="form-buttons align-right">
        <section>
            <button class="btn btn-info">Edit<i class="fa fa-edit span_btn_icon"></i></button>
        </section>
    </div>
</div>
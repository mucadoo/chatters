<div id="edit_perfil_modal" class="chatters-form wrapper wide">
    <form id="perfil_form" enctype="multipart/form-data">
        <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
        <div class="form-enclose">
            <div class="form-section">
                <span class="close_modal"></span>
                <section>
                    <label class="cxb"><strong><a class="ext">Update your profile information below</a></strong></label>
                    <div class="tagline"><span class="fa fa-long-arrow-down"></span></div>
                </section>
                <div class="row">
                    <div class="col6 first">
                        <section>
                            <div class="row">
                                <div class="col6 first">
                                    <label for="nome_perfil" class="lbl-text">Name:</label>
                                    <label class="lbl-ui append-icon">
                                        <input type="text" name="nome_perfil" id="nome_perfil" class="input" placeholder="Enter your name">
                                        <b style="display: none" class="tooltip right"></b>
                                        <span><i class="fa fa-user"></i></span>
                                    </label>
                                </div>
                                <div class="col6 last colspacer-two">
                                    <label for="sobrenome_perfil" class="lbl-text">Last Name:</label>
                                    <label class="lbl-ui append-icon">
                                        <input type="text" name="sobrenome_perfil" id="sobrenome_perfil" class="input" placeholder="Enter your last name">
                                        <b style="display: none" class="tooltip right"></b>
                                        <span><i class="fa fa-user"></i></span>
                                    </label>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="row">
                                <div class="col6 first">
                                    <label for="apelido_perfil" class="lbl-text">Nickname:</label>
                                    <label class="lbl-ui append-icon">
                                        <input type="text" name="apelido_perfil" id="apelido_perfil" class="input" placeholder="Enter your nickname">
                                        <b style="display: none" class="tooltip right"></b>
                                        <span><i class="fa fa-user"></i></span>
                                    </label>
                                </div>
                                <div class="col6 last colspacer-two">
                                    <label for="file_perfil" class="lbl-text">Profile Picture:</label>
                                    <label for="file_perfil" class="lbl-ui file-input">
                    <span class="button">
                        <input type="file" id="file_perfil" style="width: 180px" name="file_perfil">Choose
                    </span>
                                        <input type="text" id="input_img_perfil" name="input_img_perfil" class="input" placeholder="Choose a file" readonly="">
                                        <b style="display: none" class="tooltip right"></b>
                                    </label>
                                </div>
                            </div>
                        </section>
                        <section>
                            <label for="sobre_perfil" class="lbl-text">About You:</label>
                            <label class="lbl-ui append-icon art">
                                <textarea id="sobre_perfil" name="sobre_perfil" class="textarea mini no-resize" placeholder="Enter your text"></textarea>
                                <b style="display: none" class="tooltip right"></b>
                                <span><i class="fa fa-comments"></i></span>
                            </label>
                        </section>
                    </div>
                    <div class="col6 last">
                        <section style="margin-bottom: 15px">
                            <label for="msg" class="lbl-text"><?php echo label_nascimento ?></label>
                            <div class="row">
                                <div class="col4 first">
                                    <label for="dia_sel" class="lbl-text">
                                        <select style="width: 100%" id="dia_sel" name="dia_sel" data-placeholder="<?php echo placeholder_dia ?>">
                                            <option></option>
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                            <option value="05">5</option>
                                            <option value="06">6</option>
                                            <option value="07">7</option>
                                            <option value="08">8</option>
                                            <option value="09">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="col4 colspacer-one">
                                    <label for="mes_sel" class="lbl-text">
                                        <select style="width: 100%" id="mes_sel" name="mes_sel" data-placeholder="<?php echo placeholder_mes ?>">
                                            <option></option>
                                            <option value="01"><?php echo janeiro ?></option>
                                            <option value="02"><?php echo fevereiro ?></option>
                                            <option value="03"><?php echo marco ?></option>
                                            <option value="04"><?php echo abril ?></option>
                                            <option value="05"><?php echo maio ?></option>
                                            <option value="06"><?php echo junho ?></option>
                                            <option value="07"><?php echo julho ?></option>
                                            <option value="08"><?php echo agosto ?></option>
                                            <option value="09"><?php echo setembro ?></option>
                                            <option value="10"><?php echo outubro ?></option>
                                            <option value="11"><?php echo novembro ?></option>
                                            <option value="12"><?php echo dezembro ?></option>
                                        </select>
                                    </label>
                                </div>
                                <div class="col4 last colspacer-one">
                                    <label for="ano_sel" class="lbl-text">
                                        <select style="width: 100%" id="ano_sel" name="ano_sel" data-placeholder="<?php echo placeholder_ano ?>">
                                            <option></option>
                                            <option value="2005">2005</option>
                                            <option value="2004">2004</option>
                                            <option value="2003">2003</option>
                                            <option value="2002">2002</option>
                                            <option value="2001">2001</option>
                                            <option value="2000">2000</option>
                                            <option value="1999">1999</option>
                                            <option value="1998">1998</option>
                                            <option value="1997">1997</option>
                                            <option value="1996">1996</option>
                                            <option value="1995">1995</option>
                                            <option value="1994">1994</option>
                                            <option value="1993">1993</option>
                                            <option value="1992">1992</option>
                                            <option value="1991">1991</option>
                                            <option value="1990">1990</option>
                                            <option value="1989">1989</option>
                                            <option value="1988">1988</option>
                                            <option value="1987">1987</option>
                                            <option value="1986">1986</option>
                                            <option value="1985">1985</option>
                                            <option value="1984">1984</option>
                                            <option value="1983">1983</option>
                                            <option value="1982">1982</option>
                                            <option value="1981">1981</option>
                                            <option value="1980">1980</option>
                                            <option value="1979">1979</option>
                                            <option value="1978">1978</option>
                                            <option value="1977">1977</option>
                                            <option value="1976">1976</option>
                                            <option value="1975">1975</option>
                                            <option value="1974">1974</option>
                                            <option value="1973">1973</option>
                                            <option value="1972">1972</option>
                                            <option value="1971">1971</option>
                                            <option value="1970">1970</option>
                                            <option value="1969">1969</option>
                                            <option value="1968">1968</option>
                                            <option value="1967">1967</option>
                                            <option value="1966">1966</option>
                                            <option value="1965">1965</option>
                                            <option value="1964">1964</option>
                                            <option value="1963">1963</option>
                                            <option value="1962">1962</option>
                                            <option value="1961">1961</option>
                                            <option value="1960">1960</option>
                                            <option value="1959">1959</option>
                                            <option value="1958">1958</option>
                                            <option value="1957">1957</option>
                                            <option value="1956">1956</option>
                                            <option value="1955">1955</option>
                                            <option value="1954">1954</option>
                                            <option value="1953">1953</option>
                                            <option value="1952">1952</option>
                                            <option value="1951">1951</option>
                                            <option value="1950">1950</option>
                                            <option value="1949">1949</option>
                                            <option value="1948">1948</option>
                                            <option value="1947">1947</option>
                                            <option value="1946">1946</option>
                                            <option value="1945">1945</option>
                                            <option value="1944">1944</option>
                                            <option value="1943">1943</option>
                                            <option value="1942">1942</option>
                                            <option value="1941">1941</option>
                                            <option value="1940">1940</option>
                                            <option value="1939">1939</option>
                                            <option value="1938">1938</option>
                                            <option value="1937">1937</option>
                                            <option value="1936">1936</option>
                                            <option value="1935">1935</option>
                                            <option value="1934">1934</option>
                                            <option value="1933">1933</option>
                                            <option value="1932">1932</option>
                                            <option value="1931">1931</option>
                                            <option value="1930">1930</option>
                                            <option value="1929">1929</option>
                                            <option value="1928">1928</option>
                                            <option value="1927">1927</option>
                                            <option value="1926">1926</option>
                                            <option value="1925">1925</option>
                                            <option value="1924">1924</option>
                                            <option value="1923">1923</option>
                                            <option value="1922">1922</option>
                                            <option value="1921">1921</option>
                                            <option value="1920">1920</option>
                                            <option value="1919">1919</option>
                                            <option value="1918">1918</option>
                                            <option value="1917">1917</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </section>
                        <section style="margin-bottom: 15px">
                            <div class="row">
                                <div class="col6 first">
                                    <label for="msg" class="lbl-text">Gender:</label>
                                    <label for="sexo_sel" class="lbl-text">
                                        <select style="width: 100%" id="sexo_sel" name="sexo_sel" data-placeholder="Choose a gender">
                                            <option></option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="col6 last colspacer-two">
                                    <label for="ocupacao_perfil" class="lbl-text">Occupation:</label>
                                    <label class="lbl-ui append-icon">
                                        <input type="text" name="ocupacao_perfil" id="ocupacao_perfil" class="input" placeholder="Enter your occupation">
                                        <b style="display: none" class="tooltip left-bottom"></b>
                                        <span><i class="fa fa-briefcase"></i></span>
                                    </label>
                                </div>
                            </div>
                        </section>
                        <section style="margin-bottom: 15px">
                            <div class="row">
                                <div class="col4 first">
                                    <label for="msg" class="lbl-text">Country:</label>
                                    <label for="pais_sel" class="lbl-text">
                                        <select style="width: 100%" id="pais_sel" name="pais_sel" data-placeholder="Choose a country">
                                            <option></option>
                                            <option value="0">Prefer not to say</option>
                                            <?php echo $objLocalDAO->comboBoxPaises($conexao); ?>
                                        </select>
                                    </label>
                                </div>
                                <div class="col4 colspacer-one">
                                    <label for="msg" class="lbl-text">State:</label>
                                    <label for="state_sel" class="lbl-text">
                                        <select style="width: 100%" id="state_sel" name="state_sel" data-placeholder="Choose a state">
                                            <option></option>
                                            <option value="0">Prefer not to say</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="col4 last colspacer-one">
                                    <label for="msg" class="lbl-text">City:</label>
                                    <label for="city_sel" class="lbl-text">
                                        <select style="width: 100%" id="city_sel" name="city_sel" data-placeholder="Choose a city">
                                            <option></option>
                                            <option></option><option value='0'>Prefer not to say</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="row">
                                <div class="col6 first">
                                    <label for="email_perfil" class="lbl-text">Email:</label>
                                    <label class="lbl-ui append-icon">
                                        <input type="text" name="email_perfil" id="email_perfil" class="input" placeholder="Enter your email">
                                        <b style="display: none" class="tooltip right"></b>
                                        <span><i class="fa fa-envelope-o"></i></span>
                                    </label>
                                </div>
                                <div class="col6 last colspacer-two">
                                    <label for="site_perfil" class="lbl-text">Website:</label>
                                    <label class="lbl-ui append-icon">
                                        <input type="text" name="site_perfil" id="site_perfil" class="input" placeholder="Enter your website">
                                        <b style="display: none" class="tooltip left-top"></b>
                                        <span><i class="fa fa-globe"></i></span>
                                    </label>
                                </div>
                            </div>
                        </section>
                    </div>	
                </div>
            </div>
        </div>
        <div class="form-buttons align-right">
            <section>
                <button class="btn btn-info" style="float: left"><i class="fa fa-arrow-circle-left left_btn_icon"></i>Back</button>
                <button class="btn btn-info">Update<i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
            </section>
        </div>
    </form>
</div>
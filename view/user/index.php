<?php
$description = "Chatters - Talking Between Us!";
$keywords = "chatters, rede, social, escolar, estudantil, falando, entre, nós, talking, between, us, login, entrar, cadastrar, cadastro";
$page = "Chatters";
$page_user = TRUE;
include("view/head.php");
require_once 'model/UsuarioDAO.class.php';
$objUsuarioDAO = new UsuarioDAO();
require_once 'model/CursoDAO.class.php';
$objCursoDAO = new CursoDAO();
require_once 'model/LocalDAO.class.php';
$objLocalDAO = new LocalDAO();
?>
<body id="normal_page" class="pagina">
    <div id="pagewrap">
        <?php
        include("view/header.php");
        ?>
        <div id="main-content">
            <div class="duck-animation" style="background-image:url(media/aviao.png);"></div>
        </div>
        <div id="wrapper" class="clearfix">
            <div id="parallax_wrapper">
                <div id="content">
                    <div id="wrapper2">
                        <div class="container2 boxes">
                            <div class="modals">
                                <?php
                                include("view/user/pages/sala.php");
                                include("view/user/pages/perfil.php");
                                include("view/user/pages/editar_perfil.php");
                                ?>
                            </div>
                            <div class="mask_modal"></div>
                        </div>
                    </div>
                </div>
                <span class="scene scene_1"></span>
                <span class="scene scene_2"></span>
                <span class="scene scene_3"></span>
            </div>
        </div>

    </div>

    <?php include("view/footer.php"); ?>

</body>
</html>
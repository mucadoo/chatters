<?php
if (isset($page_home)) {
    ?>
    <div id="footer">
        <div class="container">
            <ul class="copyright_info">
                <li>&copy; <?= date("Y"); ?> Chatters</li>
                <li id="change_english">English<div id="us_flag"></div></li>
                <li id="change_portugues">Portuguese<div id="br_flag"></div></li>
            </ul>
            <ul class="links">
                <li>
                    <a target="blank" title="Twitter" class="social_links" href="http://www.twitter.com/chatters">
                        <div id="twitter_link"></div>
                    </a>
                </li>
                <li id="separador_links"></li>
                <li>
                    <a target="blank" title="Facebook" class="social_links" href="http://www.facebook.com/chatters">
                        <div id="facebook_link"></div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <?php
} else if (isset($page_user)) {
    ?>
    <div id="footer">
        <div class="container">
            <ul class="copyright_info">
                <li>&copy; <?= date("Y"); ?> Chatters</li>
            </ul>
        </div>
    </div>
    <?php
} else if (isset($page_admin)) {
    ?>
    <div id="footer" style="z-index: 1">
        <div class="container">
            <ul class="copyright_info">
                <li>&copy; <?= date("Y"); ?> Chatters</li>
            </ul>
        </div>
    </div>

    <script src='<?php echo BASE_URL ?>/script/libraries/jquery-1.11.0.min.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/jquery-migrate-1.2.1.min.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/jquery-ui-1.8.16.min.js'></script>
    <script src='<?php echo BASE_URL ?>/script/plugins_admin.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.ba-resize.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.easing-1.3.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery-ui.touch-punch-0.1.0.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.fallr-1.2.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.chosen-0.9.5.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.dataTables-1.8.2.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.validate-1.9.0.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.checkbox-1.3.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.fileinput.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.placeholder-1.8.5.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.miniColors-0.1.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery.text-overflow.js'></script>
    <script src='<?php echo BASE_URL ?>/script/libraries/plugins/jquery-ui.timepicker-0.9.7.js'></script>
    <script defer src='<?php echo BASE_URL ?>/script/script_admin.js'></script>
    <?php
}
?>
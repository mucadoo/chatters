<?php
$description = "Chatters - Talking Between Us!";
$keywords = "chatters, rede, social, escolar, estudantil, falando, entre, nós, talking, between, us, login, entrar, cadastrar, cadastro";
$page = home_title;
$page_home = TRUE;
include("view/head.php");

if (isset($_COOKIE['cod_cadastro'])) {

    $dados = explode("///", $_COOKIE['cod_cadastro']);

    if (count($dados) == 3) {
        $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($dados[1]));

        if ($verifica_escola != FALSE) {

            $conexao = Conecta::getConexao("config/bd/geral.ini", $verifica_escola);

            $verifica_usuario = $objUsuarioDAO->verificaCookieAut($conexao, $dados[0], TRUE);

            $conexao = Conecta::getConexao("config/bd/geral.ini");

            if ($verifica_usuario == 3 || $verifica_usuario == 4) {
                if ($dados[2] == 'U') {
                    $cadastro_andando = TRUE;
                } else {
                    setcookie('cod_cadastro', 0, time() - 3600);
                }
            } else if ($verifica_usuario == 2) {
                if ($dados[2] == 'A') {
                    $cadastro_andando = TRUE;
                } else {
                    setcookie('cod_cadastro', 0, time() - 3600);
                }
            } else {
                setcookie('cod_cadastro', 0, time() - 3600);
            }
        } else {
            setcookie('cod_cadastro', 0, time() - 3600);
        }
    } else {
        setcookie('cod_cadastro', 0, time() - 3600);
    }
}
?>

<body id="normal_page" class="pagina">
    <div id="pagewrap">
        <?php
        include("view/header.php");
        ?>
        <div id="main-content">
            <div class="duck-animation" style="background-image:url(media/aviao.png);"></div>
        </div>
        <div id="wrapper" class="clearfix">
            <div id="parallax_wrapper">
                <div id="content">
                    <div id="wrapper2">
                        <div id="mask">
                            <?php
                            include("view/home/pages/login.php");
                            include("view/home/pages/sign_up.php");
                            include("view/home/pages/about.php");
                            include("view/home/pages/terms.php");
                            include("view/home/pages/contact.php");
                            ?>
                            <div class="clear"></div>
                            <?php
                            include("view/home/pages/forgot_pass.php");
                            if (isset($recuperacao_andando)) {
                                include("view/home/pages/new_pass.php");
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <span class="scene scene_1"></span>
                <span class="scene scene_2"></span>
                <span class="scene scene_3"></span>
            </div>
        </div>

    </div>

    <?php include("view/footer.php"); ?>

</body>
</html>
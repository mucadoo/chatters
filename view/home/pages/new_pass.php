<div class="item" id="new_pass_page">

    <div class="content">

        <div class="container2">

            <div class="chatters-form wrapper mini">
                <form id="new_pass_form">
                    <input type="hidden" name="id_cad_usuario" id="id_cad_usuario" value="<?php echo $_GET['user'] ?>">
                    <input type="hidden" name="esc_cad_usuario" id="esc_cad_usuario" value="<?php echo $_GET['esc'] ?>">
                    <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
                    <div class="form-enclose">
                        <div class="form-section">
                            <section>
                                <label class="cxb"><strong><a class="ext"><?php echo label_new_pass ?></a></strong></label>
                                <div class="tagline"><span class="fa fa-long-arrow-down"></span></div>
                            </section>
                            <section>
                                <label for="new_pass_input" class="lbl-text"><?php echo label_nova_senha ?></label>
                                <label class="lbl-ui append-icon">
                                    <input type="password" name="new_pass_input" id="new_pass_input" class="input" placeholder="<?php echo placeholder_nova_senha ?>">
                                    <b style="display: none" class="tooltip right"></b>
                                    <span><i class="fa fa-lock"></i></span>
                                </label>
                            </section>
                            <section>
                                <label for="rep_new_pass" class="lbl-text"><?php echo label_repita_senha ?></label>
                                <label class="lbl-ui append-icon">
                                    <input type="password" name="rep_new_pass" id="rep_new_pass" class="input" placeholder="<?php echo placeholder_repita_senha ?>">
                                    <b style="display: none" class="tooltip right"></b>
                                    <span><i class="fa fa-lock"></i></span>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="form-buttons">
                        <section>
                            <button class="btn btn-info"><?php echo concluir ?><i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
                        </section>              
                    </div>
                </form>
            </div>

        </div>

    </div>

</div>
<div class="item" id="login_page">

    <div class="content">

        <div class="container2">

            <div class="chatters-form wrapper mini">
                <form id="login_form">
                    <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
                    <div class="form-enclose">
                        <div class="form-section">
                            <section>
                                <label for="login_input" class="lbl-text"><?php echo label_login ?></label>
                                <label class="lbl-ui append-icon">
                                    <input type="text" name="login_input" id="login_input" class="input" placeholder="<?php echo placeholder_login ?>" autofocus>
                                    <b style="display: none" class="tooltip right"></b>
                                    <span><i class="fa fa-user"></i></span>
                                </label>                           
                            </section> 
                            <section>
                                <label for="senha_input" class="lbl-text"><?php echo label_senha ?></label>
                                <label class="lbl-ui append-icon">
                                    <input type="password" name="senha_input" id="senha_input" class="input" placeholder="<?php echo placeholder_senha ?>">
                                    <b style="display: none" class="tooltip right"></b>
                                    <span><i class="fa fa-lock"></i></span>
                                </label>
                            </section>
                            <section>
                                <label for="escolas_login" class="lbl-text"><?php echo label_escola ?></label>
                                <select style="width: 100%;" name="escolas_login" id="escolas_login" data-placeholder="<?php echo placeholder_escola ?>">
                                    <option></option>
                                    <?php echo $objEscolaDAO->comboBoxEscolas($conexao) ?>
                                </select>
                                <input style="position: absolute; visibility: hidden" class="input" type="text" name="hidden_login" id="hidden_login">
                                <b style="display: none" class="tooltip right seltip"></b>
                            </section>
                            <section>
                                <span class="gtoggle">
                                    <label class="toggle-switch blue">
                                        <input type="checkbox" name="lembrar_senha" id="lembrar_senha">
                                        <label style="cursor: pointer" for="lembrar_senha" data-on="<?php echo sim ?>" data-off="<?php echo nao ?>"></label>
                                    </label>                                
                                    <label style="cursor: pointer" for="lembrar_senha"><?php echo lembrarme ?> <strong><a href="#forgot_pass_page" class="glink ext a_scroll"><?php echo esqueceu_senha ?></a></strong></label>
                                </span>
                            </section>
                        </div>
                    </div>
                    <div class="form-buttons">
                        <section>
                            <button type="submit" class="btn btn-info"><?php echo entrar ?><i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
                        </section>                
                    </div>
                </form>
            </div>

        </div>

    </div>

</div>
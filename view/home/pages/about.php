<div class="item" id="about_page">

    <div class="content">

        <div class="container2">

            <div class="chatters-form wrapper wide ddx">
                <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
                <div class="form-enclose">
                    <div class="form-section">
                        <section>
                            <div class="row">
                                <div class="col6 first">
                                    <article>
                                        <h3><?php echo sobre1 ?></h3>
                                        <p><?php echo sobre2 ?></p>
                                    </article>
                                </div>                                                                
                                <div class="col6 last">
                                    <div class="res-image">
                                        <img style="margin: 10px 0 0 30px" src="media/trans_chatters_completo_slogan.png">
                                    </div>
                                </div>	
                            </div>                         
                        </section>
                        <div style="margin-top: -30px">
                            <h3 class="demo-header2 ftx"><span><?php echo sobre3 ?></span></h3>
                        </div>
                        <section style="margin-top: -10px">
                            <div class="row">
                                <div class="col6 first">
                                    <article>
                                        <p><?php echo sobre4 ?></p>
                                    </article>
                                </div>
                                <div class="col6 last">
                                    <article>
                                        <p><?php echo sobre5 ?></p>
                                    </article>
                                </div>
                            </div>
                        </section> 
                        <section style="margin-top: -25px">
                            <div class="row">
                                <div class="col6 first">
                                    <article>
                                        <p><?php echo sobre6 ?></p>
                                    </article>
                                </div>                                                                   
                                <div class="col6 last">
                                    <article>
                                        <p><?php echo sobre7 ?></p>
                                    </article>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
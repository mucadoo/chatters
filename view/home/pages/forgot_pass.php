<div class="item" id="forgot_pass_page">

    <div class="content">

        <div class="container2">

            <div class="chatters-form wrapper mini">
                <form id="forgot_pass_form">
                    <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
                    <div class="form-enclose">
                        <div class="form-section">
                            <section>
                                <label class="cxb"><strong><a class="ext"><?php echo label_forgot ?></a></strong></label>
                                <div class="tagline"><span class="fa fa-long-arrow-down"></span></div>
                            </section>
                            <section>
                                <label for="forgot_input" class="lbl-text"><?php echo label_login ?></label>
                                <label class="lbl-ui append-icon">
                                    <input type="text" name="forgot_input" id="forgot_input" class="input" placeholder="<?php echo placeholder_login ?>">
                                    <b style="display: none" class="tooltip right"></b>
                                    <span><i class="fa fa-user"></i></span>
                                </label>                           
                            </section>
                            <section>
                                <label for="escolas_forgot" class="lbl-text"><?php echo label_escola ?></label>
                                <select style="width: 100%;" name="escolas_forgot" id="escolas_forgot" data-placeholder="<?php echo placeholder_escola ?>">
                                    <option></option>
                                    <?php echo $objEscolaDAO->comboBoxEscolas($conexao) ?>
                                </select>
                                <input style="position: absolute; z-index: -50" class="input" type="text" name="hidden_forgot" id="hidden_forgot">
                                <b style="display: none" class="tooltip right seltip"></b>
                            </section>
                        </div>
                    </div>
                    <div class="form-buttons">
                        <section>
                            <button class="btn btn-info"><?php echo enviar ?><i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
                        </section>              
                    </div>
                </form>
            </div>

        </div>

    </div>

</div>
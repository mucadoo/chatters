<div class="item" id="sign_up_page">

    <div class="content">

        <div class="container2">

            <div id="container_cad_form1" class="chatters-form wrapper mini<?php
            if (isset($cadastro_andando)) {
                if ($dados[2] == "A") {
                    echo ' cadastro_andando_a"';
                } else if ($dados[2] == "U") {
                    echo ' cadastro_andando_u"';
                }
                echo 'style="display: none';
            }
            ?>">
                <form id="sign_up_form">
                    <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
                    <div class="form-enclose">
                        <div class="form-section">
                            <section>
                                <label class="cxb"><strong><a class="ext"><?php echo label_cadastro ?></a></strong></label>
                                <div class="tagline"><span class="fa fa-long-arrow-down"></span></div>
                            </section>
                            <section>
                                <label for="cpf_input" class="lbl-text">CPF:</label>
                                <label class="lbl-ui append-icon">
                                    <input type="text" name="cpf_input" id="cpf_input" class="input" placeholder="<?php echo placeholder_cpf ?>">
                                    <b style="display: none" class="tooltip right"></b>
                                    <span><i class="fa fa-sort-numeric-asc"></i></span>
                                </label>                           
                            </section> 
                            <section>
                                <label for="num_reg_input" class="lbl-text"><?php echo label_num_reg ?></label>
                                <label class="lbl-ui append-icon">
                                    <input type="text" name="num_reg_input" id="num_reg_input" class="input" placeholder="<?php echo placeholder_num_reg ?>">
                                    <b style="display: none" class="tooltip right"></b>
                                    <span><i class="fa fa-sort-numeric-asc"></i></span>
                                </label>                           
                            </section> 
                            <section>
                                <label for="pre_senha_input" class="lbl-text"><?php echo label_pre_senha ?></label>
                                <label class="lbl-ui append-icon">
                                    <input type="password" name="pre_senha_input" id="pre_senha_input" class="input" placeholder="<?php echo placeholder_pre_senha ?>">
                                    <b style="display: none" class="tooltip right"></b>
                                    <span><i class="fa fa-lock"></i></span>
                                </label>
                            </section>
                            <section>
                                <label for="escolas_cadastro" class="lbl-text"><?php echo label_escola ?></label>
                                <select style="width: 100%" name="escolas_cadastro" id="escolas_cadastro" data-placeholder="<?php echo placeholder_escola ?>">
                                    <option></option>
                                    <?php echo $objEscolaDAO->comboBoxEscolas($conexao) ?>
                                </select>
                                <input style="position: absolute; visibility: hidden" class="input" type="text" name="hidden_cad" id="hidden_cad">
                                <b style="display: none" class="tooltip right seltip"></b>
                            </section>
                        </div>
                    </div>
                    <div class="form-buttons">
                        <section>
                            <button class="btn btn-info"><?php echo continuar ?><i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
                        </section>              
                    </div>
                </form>
            </div>

            <div <?php
            if (!isset($cadastro_andando)) {
                echo 'style="display: none" ';
            }
            ?>id="container_cad_form2" class="chatters-form wrapper wide">
                <form id="conc_sign_up_form">
                    <?php
                    if (isset($cadastro_andando)) {
                        echo '<input type="hidden" name="id_cad_usuario" id="id_cad_usuario" value="' . $dados[0] . '">
                              <input type="hidden" name="esc_cad_usuario" id="esc_cad_usuario" value="' . $dados[1] . '">';
                    }
                    ?>
                    <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
                    <div class="form-enclose">
                        <div class="form-section">
                            <div class="row">
                                <div class="col6 first">
                                    <fieldset>
                                        <legend><?php echo informacoes_login ?></legend>
                                        <section>
                                            <label for="nome_usuario_input" class="lbl-text"><?php echo label_nome_usuario ?></label>
                                            <label class="lbl-ui append-icon">
                                                <input type="text" name="nome_usuario_input" id="nome_usuario_input" class="input" placeholder="<?php echo placeholder_nome_usuario ?>">
                                                <b style="display: none" class="tooltip right"></b>
                                                <span><i class="fa fa-user"></i></span>
                                            </label>
                                        </section>
                                        <section>
                                            <label for="email_input" class="lbl-text">E-mail:</label>
                                            <label class="lbl-ui append-icon">
                                                <input type="text" name="email_input" id="email_input" class="input" placeholder="<?php echo placeholder_email ?>">
                                                <b style="display: none" class="tooltip right"></b>
                                                <span><i class="fa fa-envelope"></i></span>
                                            </label>
                                        </section>
                                        <section>
                                            <label for="senha_input" class="lbl-text"><?php echo label_senha ?></label>
                                            <label class="lbl-ui append-icon">
                                                <input type="password" name="senha_input" id="senha_input" class="input" placeholder="<?php echo placeholder_senha ?>">
                                                <b style="display: none" class="tooltip right"></b>
                                                <span><i class="fa fa-lock"></i></span>
                                            </label>
                                        </section>
                                        <section>
                                            <label for="repeat_senha_input" class="lbl-text"><?php echo label_repita_senha ?></label>
                                            <label class="lbl-ui append-icon">
                                                <input type="password" name="repeat_senha_input" id="repeat_senha_input" class="input" placeholder="<?php echo placeholder_repita_senha ?>">
                                                <b style="display: none" class="tooltip right"></b>
                                                <span><i class="fa fa-lock"></i></span>
                                            </label>
                                        </section>
                                    </fieldset>
                                </div>
                                <div class="col6 last">
                                    <fieldset>
                                        <legend><?php echo informacoes_perfil ?></legend>
                                        <section>
                                            <label for="nome_input" class="lbl-text"><?php echo label_nome ?></label>
                                            <label class="lbl-ui append-icon">
                                                <input type="text" name="nome_input" id="nome_input" class="input" placeholder="<?php echo placeholder_nome ?>">
                                                <b style="display: none" class="tooltip right"></b>
                                                <span><i class="fa fa-user"></i></span>
                                            </label>
                                        </section>
                                        <section>
                                            <label for="sobrenome_input" class="lbl-text"><?php echo label_sobrenome ?></label>
                                            <label class="lbl-ui append-icon">
                                                <input type="text" name="sobrenome_input" id="sobrenome_input" class="input" placeholder="<?php echo placeholder_sobrenome ?>">
                                                <b style="display: none" class="tooltip right"></b>
                                                <span><i class="fa fa-user"></i></span>
                                            </label>
                                        </section>
                                        <section>
                                            <label for="msg" class="lbl-text"><?php echo label_nascimento ?></label>
                                            <input style="position: absolute; z-index: -50;" class="input" type="text" name="hidden_dia" id="hidden_dia">
                                            <b style="display: none" class="tooltip right seltip"></b>
                                            <input style="position: absolute; z-index: -50;" class="input" type="text" name="hidden_mes" id="hidden_mes">
                                            <b style="display: none" class="tooltip right seltip"></b>
                                            <input style="position: absolute; z-index: -50;" class="input" type="text" name="hidden_ano" id="hidden_ano">
                                            <b style="display: none" class="tooltip right seltip"></b>
                                            <div class="row">
                                                <input type="hidden" name="data_nasc_completa" id="data_nasc_completa" value="">
                                                <div class="col4 first">
                                                    <label for="dia_sel" class="lbl-text">
                                                        <select style="width: 100%" id="dia_sel" name="dia_sel" data-placeholder="<?php echo placeholder_dia ?>">
                                                            <option></option>
                                                            <option value="01">1</option>
                                                            <option value="02">2</option>
                                                            <option value="03">3</option>
                                                            <option value="04">4</option>
                                                            <option value="05">5</option>
                                                            <option value="06">6</option>
                                                            <option value="07">7</option>
                                                            <option value="08">8</option>
                                                            <option value="09">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                            <option value="19">19</option>
                                                            <option value="20">20</option>
                                                            <option value="21">21</option>
                                                            <option value="22">22</option>
                                                            <option value="23">23</option>
                                                            <option value="24">24</option>
                                                            <option value="25">25</option>
                                                            <option value="26">26</option>
                                                            <option value="27">27</option>
                                                            <option value="28">28</option>
                                                            <option value="29">29</option>
                                                            <option value="30">30</option>
                                                            <option value="31">31</option>
                                                        </select>
                                                    </label>
                                                </div>
                                                <div class="col4 colspacer-one">
                                                    <label for="mes_sel" class="lbl-text">
                                                        <select style="width: 100%" id="mes_sel" name="mes_sel" data-placeholder="<?php echo placeholder_mes ?>">
                                                            <option></option>
                                                            <option value="01"><?php echo janeiro ?></option>
                                                            <option value="02"><?php echo fevereiro ?></option>
                                                            <option value="03"><?php echo marco ?></option>
                                                            <option value="04"><?php echo abril ?></option>
                                                            <option value="05"><?php echo maio ?></option>
                                                            <option value="06"><?php echo junho ?></option>
                                                            <option value="07"><?php echo julho ?></option>
                                                            <option value="08"><?php echo agosto ?></option>
                                                            <option value="09"><?php echo setembro ?></option>
                                                            <option value="10"><?php echo outubro ?></option>
                                                            <option value="11"><?php echo novembro ?></option>
                                                            <option value="12"><?php echo dezembro ?></option>
                                                        </select>
                                                    </label>
                                                </div>
                                                <div class="col4 last colspacer-one">
                                                    <label for="ano_sel" class="lbl-text">
                                                        <select style="width: 100%" id="ano_sel" name="ano_sel" data-placeholder="<?php echo placeholder_ano ?>">
                                                            <option></option>
                                                            <option value="2005">2005</option>
                                                            <option value="2004">2004</option>
                                                            <option value="2003">2003</option>
                                                            <option value="2002">2002</option>
                                                            <option value="2001">2001</option>
                                                            <option value="2000">2000</option>
                                                            <option value="1999">1999</option>
                                                            <option value="1998">1998</option>
                                                            <option value="1997">1997</option>
                                                            <option value="1996">1996</option>
                                                            <option value="1995">1995</option>
                                                            <option value="1994">1994</option>
                                                            <option value="1993">1993</option>
                                                            <option value="1992">1992</option>
                                                            <option value="1991">1991</option>
                                                            <option value="1990">1990</option>
                                                            <option value="1989">1989</option>
                                                            <option value="1988">1988</option>
                                                            <option value="1987">1987</option>
                                                            <option value="1986">1986</option>
                                                            <option value="1985">1985</option>
                                                            <option value="1984">1984</option>
                                                            <option value="1983">1983</option>
                                                            <option value="1982">1982</option>
                                                            <option value="1981">1981</option>
                                                            <option value="1980">1980</option>
                                                            <option value="1979">1979</option>
                                                            <option value="1978">1978</option>
                                                            <option value="1977">1977</option>
                                                            <option value="1976">1976</option>
                                                            <option value="1975">1975</option>
                                                            <option value="1974">1974</option>
                                                            <option value="1973">1973</option>
                                                            <option value="1972">1972</option>
                                                            <option value="1971">1971</option>
                                                            <option value="1970">1970</option>
                                                            <option value="1969">1969</option>
                                                            <option value="1968">1968</option>
                                                            <option value="1967">1967</option>
                                                            <option value="1966">1966</option>
                                                            <option value="1965">1965</option>
                                                            <option value="1964">1964</option>
                                                            <option value="1963">1963</option>
                                                            <option value="1962">1962</option>
                                                            <option value="1961">1961</option>
                                                            <option value="1960">1960</option>
                                                            <option value="1959">1959</option>
                                                            <option value="1958">1958</option>
                                                            <option value="1957">1957</option>
                                                            <option value="1956">1956</option>
                                                            <option value="1955">1955</option>
                                                            <option value="1954">1954</option>
                                                            <option value="1953">1953</option>
                                                            <option value="1952">1952</option>
                                                            <option value="1951">1951</option>
                                                            <option value="1950">1950</option>
                                                            <option value="1949">1949</option>
                                                            <option value="1948">1948</option>
                                                            <option value="1947">1947</option>
                                                            <option value="1946">1946</option>
                                                            <option value="1945">1945</option>
                                                            <option value="1944">1944</option>
                                                            <option value="1943">1943</option>
                                                            <option value="1942">1942</option>
                                                            <option value="1941">1941</option>
                                                            <option value="1940">1940</option>
                                                            <option value="1939">1939</option>
                                                            <option value="1938">1938</option>
                                                            <option value="1937">1937</option>
                                                            <option value="1936">1936</option>
                                                            <option value="1935">1935</option>
                                                            <option value="1934">1934</option>
                                                            <option value="1933">1933</option>
                                                            <option value="1932">1932</option>
                                                            <option value="1931">1931</option>
                                                            <option value="1930">1930</option>
                                                            <option value="1929">1929</option>
                                                            <option value="1928">1928</option>
                                                            <option value="1927">1927</option>
                                                            <option value="1926">1926</option>
                                                            <option value="1925">1925</option>
                                                            <option value="1924">1924</option>
                                                            <option value="1923">1923</option>
                                                            <option value="1922">1922</option>
                                                            <option value="1921">1921</option>
                                                            <option value="1920">1920</option>
                                                            <option value="1919">1919</option>
                                                            <option value="1918">1918</option>
                                                            <option value="1917">1917</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="opts2" class="lbl-text spacer"><?php echo label_sexo ?></label>
                                            <input style="position: absolute; z-index: -50;" class="input" type="text" name="hidden_cad2" id="hidden_cad2">
                                            <b style="display: none" class="tooltip right seltip radiotip"></b>
                                            <div class="option-group">
                                                <span  class="goption">
                                                    <label class="options">
                                                        <input style="cursor: pointer" type="radio" id="m_sexo_radio" name="sexo_radio" value="M">
                                                        <span class="radio"></span>
                                                    </label>
                                                    <label style="cursor: pointer" for="m_sexo_radio"><?php echo label_masc ?></label>
                                                </span>
                                                <span class="goption">
                                                    <label class="options">
                                                        <input style="cursor: pointer" type="radio" id="f_sexo_radio" name="sexo_radio" value="F">
                                                        <span class="radio"></span>
                                                    </label>
                                                    <label style="cursor: pointer" for="f_sexo_radio"><?php echo label_fem ?></label>
                                                </span>                                                    
                                            </div>                         
                                        </section>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-buttons">
                        <section>
                            <label style="margin-top: 5px" class="options">
                                <input style="cursor: pointer" type="checkbox" id="agree_terms" name="agree_terms" value="1">
                                <span style="cursor: pointer" class="checkbox"></span>
                                <input style="position: absolute; z-index: -50;" class="input" type="text" name="hidden_cad3" id="hidden_cad3">
                                <b style="display: none" class="tooltip right checktip"></b>
                            </label>
                            <label for="check1"><?php echo label_concordo ?></label>
                            <button style="margin-left: 410px" class="btn btn-info"><?php echo concluir ?><i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
                        </section>
                    </div>
                </form>
            </div>

            <div <?php
            if (!isset($cadastro_andando)) {
                echo 'style="display: none" ';
            }
            ?>id="container_cad_form3" class="chatters-form wrapper">
                <form id="conc_sign_up_form2">
                    <?php
                    if (isset($cadastro_andando)) {
                        echo '<input type="hidden" name="id_cad_usuario" id="id_cad_usuario" value="' . $dados[0] . '">
                              <input type="hidden" name="esc_cad_usuario" id="esc_cad_usuario" value="' . $dados[1] . '">';
                    }
                    ?>
                    <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
                    <div class="form-enclose">
                        <div class="form-section">
                            <section>
                                <label class="cxb"><strong><a class="ext"><?php echo label_cadastro_adm ?></a></strong></label>
                                <div class="tagline"><span class="fa fa-long-arrow-down"></span></div>
                            </section>
                            <div class="row">
                                <div class="col6 first">
                                    <section>
                                        <label for="nome_adm_input" class="lbl-text"><?php echo label_nome ?></label>
                                        <label class="lbl-ui append-icon">
                                            <input type="text" name="nome_adm_input" id="nome_adm_input" class="input" placeholder="<?php echo placeholder_nome ?>">
                                            <b style="display: none" class="tooltip right"></b>
                                            <span><i class="fa fa-user"></i></span>
                                        </label>                           
                                    </section>
                                    <section>
                                        <label for="sobrenome_adm_input" class="lbl-text"><?php echo label_sobrenome ?></label>
                                        <label class="lbl-ui append-icon">
                                            <input type="text" name="sobrenome_adm_input" id="sobrenome_adm_input" class="input" placeholder="<?php echo placeholder_sobrenome ?>">
                                            <b style="display: none" class="tooltip right"></b>
                                            <span><i class="fa fa-user"></i></span>
                                        </label>                           
                                    </section>
                                    <section>
                                        <label for="opts2" class="lbl-text spacer"><?php echo label_sexo ?></label>
                                        <input style="position: absolute; z-index: -50;" class="input" type="text" name="hidden_cad4" id="hidden_cad4">
                                        <b style="display: none" class="tooltip right seltip adm_radiotip"></b>
                                        <div style="margin-top: -5px" class="option-group">
                                            <span  class="goption">
                                                <label class="options">
                                                    <input style="cursor: pointer" type="radio" id="m_sexo_radio_adm" name="sexo_radio_adm" value="M">
                                                    <span class="radio"></span>
                                                </label>
                                                <label style="cursor: pointer" for="m_sexo_radio_adm"><?php echo label_masc ?></label>
                                            </span>
                                            <span class="goption">
                                                <label class="options">
                                                    <input style="cursor: pointer" type="radio" id="f_sexo_radio_adm" name="sexo_radio_adm" value="F">
                                                    <span class="radio"></span>
                                                </label>
                                                <label style="cursor: pointer" for="f_sexo_radio_adm"><?php echo label_fem ?></label>
                                            </span>                                                    
                                        </div>                         
                                    </section>
                                </div>
                                <div class="col6 last">
                                    <section>
                                        <label for="nova_senha_input" class="lbl-text"><?php echo label_nova_senha ?></label>
                                        <label class="lbl-ui append-icon">
                                            <input type="password" name="nova_senha_input" id="nova_senha_input" class="input" placeholder="<?php echo placeholder_nova_senha ?>">
                                            <b style="display: none" class="tooltip right"></b>
                                            <span><i class="fa fa-lock"></i></span>
                                        </label>
                                    </section>
                                    <section>
                                        <label for="repita_senha_input" class="lbl-text"><?php echo label_repita_senha ?></label>
                                        <label class="lbl-ui append-icon">
                                            <input type="password" name="repita_senha_input" id="repita_senha_input" class="input" placeholder="<?php echo placeholder_repita_senha ?>">
                                            <b style="display: none" class="tooltip right"></b>
                                            <span><i class="fa fa-lock"></i></span>
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-buttons align-right" style="margin-top: -15px">
                        <section>
                            <button class="btn btn-info"><?php echo concluir ?><i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
                        </section>
                    </div>
                </form>
            </div>

        </div>

    </div>

</div>
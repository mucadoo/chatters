<div class="item" id="contact_page">

    <div class="content">

        <div class="container2">

            <div class="chatters-form wrapper">
                <form id="contact_form">
                    <div class="form-title" style="height: 3px; padding:0; margin:0"></div>
                    <div class="form-enclose">
                        <div class="form-section">
                            <section>
                                <label class="cxb"><strong><a class="ext"><?php echo label_contato ?></a></strong></label>
                                <div class="tagline"><span class="fa fa-long-arrow-down"></span></div>
                            </section>
                            <div class="row">
                                <div class="col6 first">
                                    <section>
                                        <label for="nome_contato" class="lbl-text"><?php echo label_nome ?></label>
                                        <label class="lbl-ui append-icon">
                                            <input type="text" name="nome_contato" id="nome_contato" class="input" placeholder="<?php echo placeholder_nome ?>">
                                            <b style="display: none" class="tooltip right"></b>
                                            <span><i class="fa fa-user"></i></span>
                                        </label>
                                    </section>
                                    <section>
                                        <label for="email_contato" class="lbl-text">Email: </label>
                                        <label class="lbl-ui append-icon">
                                            <input type="text" name="email_contato" id="email_contato" class="input" placeholder="<?php echo placeholder_email ?>">
                                            <b style="display: none" class="tooltip right"></b>
                                            <span><i class="fa fa-user"></i></span>
                                        </label>
                                    </section>
                                    <section>
                                        <label for="assunto_contato" class="lbl-text"><?php echo label_assunto ?></label>
                                        <select style="width: 100%;" name="assunto_contato" id="assunto_contato" data-placeholder="<?php echo placeholder_assunto ?>">
                                            <option></option>
                                            <option value="Contact"><?php echo assunto1 ?></option>
                                            <option value="Suggestion"><?php echo assunto2 ?></option>
                                            <option value="Bug"><?php echo assunto3 ?></option>
                                            <option value="Other"><?php echo assunto4 ?></option>
                                        </select>
                                        <input style="position: absolute; z-index: -50" class="input" type="text" name="hidden_contato" id="hidden_contato">
                                        <b style="display: none" class="tooltip right seltip"></b>
                                    </section>
                                </div>
                                <div class="col6 last">
                                    <section>
                                        <label for="msg_contato" class="lbl-text"><?php echo label_mensagem ?></label>
                                        <label class="lbl-ui append-icon art">
                                            <textarea id="msg_contato" name="msg_contato" class="textarea no-resize" placeholder="<?php echo placeholder_mensagem ?>"></textarea>
                                            <b style="display: none" class="tooltip right"></b>
                                            <span><i class="fa fa-comments"></i></span>
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-buttons align-right">
                        <section>
                            <button class="btn btn-info"><?php echo enviar ?><i class="fa fa-arrow-circle-right span_btn_icon"></i></button>
                        </section>
                    </div>
                </form>
            </div>

        </div>

    </div>

</div>
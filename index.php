<?php

define("BASE_URL", "http://localhost");

date_default_timezone_set('Europe/Paris');

require_once 'controller/Conecta.class.php';
$conexao = Conecta::getConexao("config/bd/geral.ini");

require_once 'model/EscolaDAO.class.php';
$objEscolaDAO = new EscolaDAO();

require_once 'model/UsuarioDAO.class.php';
$objUsuarioDAO = new UsuarioDAO();

require_once 'controller/Bcrypt.class.php';
require_once 'controller/funcoes.php';

if (isset($_GET['sair'])) {
    setcookie('cod_logado', 0, time() - 3600);
    setcookie('esc_logado', 0, time() - 3600);
    header('location:' . BASE_URL);
}

if (!isset($_COOKIE["lang"])) {
    $ip_data = locateIp();
    if ($ip_data) {
        require_once "config/lang/$ip_data.php";
        setcookie("lang", $ip_data, time() + 60 * 60 * 24 * 30 * 12 * 5);
    } else {
        require_once "config/lang/en.php";
    }
} else if (file_exists("config/lang/" . $_COOKIE['lang'] . ".php")) {
    require_once "config/lang/" . $_COOKIE['lang'] . ".php";
} else {
    setcookie("lang", 0, time() - 3600);
    require_once "config/lang/en.php";
}

if (isset($_GET['use']) && isset($_GET['esc']) && isset($_GET['cod'])) {

    setcookie('cod_cadastro', 0, time() - 3600);
    setcookie('cod_logado', 0, time() - 3600);
    setcookie('esc_logado', 0, time() - 3600);

    $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_GET['esc']));

    if ($verifica_escola != FALSE) {

        $verifica_usuario = $objUsuarioDAO->verificaUser($conexao, decriptNumberCookie($_GET['use']));

        if ($verifica_usuario != FALSE) {

            $verifica_cod_rec = $objUsuarioDAO->checkForgotCod($conexao, decriptNumberCookie($_GET['use']), $_GET['cod']);

            if ($verifica_cod_rec != FALSE) {
                $recuperacao_andando = TRUE;
                include 'view/home/index.php';
            } else {
                header('location:' . BASE_URL);
            }
        } else {
            header('location:' . BASE_URL);
        }
    } else {
        header('location:' . BASE_URL);
    }
} else {
    if (isset($_COOKIE['esc_logado'])) {

        $verifica_escola = $objEscolaDAO->verificaEscola($conexao, decriptNumberCookie($_COOKIE['esc_logado']));

        if ($verifica_escola != FALSE) {

            if (isset($_COOKIE['cod_logado'])) {

                $verifica_usuario = $objUsuarioDAO->verificaCookieAut($conexao, $_COOKIE['cod_logado']);

                if ($verifica_usuario != FALSE) {
                    setcookie('cod_cadastro', 0, time() - 3600);
                    if (isset($_GET['download'])) {
                        $arquivo = $_GET["download"];
                        if (isset($arquivo) && file_exists($arquivo)) {
                            switch (strtolower(substr(strrchr(basename($arquivo), "."), 1))) {
                                case "pdf": $tipo = "application/pdf";
                                    break;
                                case "doc": $tipo = "application/msword";
                                    break;
                                case "docx": $tipo = "application/msword";
                                    break;
                                case "xls": $tipo = "application/vnd.ms-excel";
                                    break;
                                case "xlsx": $tipo = "application/vnd.ms-excel";
                                    break;
                                case "ppt": $tipo = "application/vnd.ms-powerpoint";
                                    break;
                                case "pptx": $tipo = "application/vnd.ms-powerpoint";
                                    break;
                                case "gif": $tipo = "image/gif";
                                    break;
                                case "png": $tipo = "image/png";
                                    break;
                                case "jpg": $tipo = "image/jpg";
                                    break;
                                case "txt": $tipo = "text/plain";
                                    break;
                                case "rtf": $tipo = "text/rtf";
                                    break;
                                case "php":
                                case "htaccess":
                                case "ini":
                            }
                            header("Content-Type: " . $tipo);
                            header("Content-Length: " . filesize($arquivo));
                            header("Content-Disposition: attachment; filename=" . basename($arquivo));
                            readfile($arquivo);
                            exit;
                        }
                    } else {
                        if ($verifica_usuario == 1 || $verifica_usuario == 2) {
                            include 'view/admin/index.php';
                        } else {
                            include 'view/user/index.php';
                        }
                    }
                } else {
                    $conexao = Conecta::getConexao("config/bd/geral.ini");
                    setcookie('cod_logado', 0, time() - 3600);
                    setcookie('esc_logado', 0, time() - 3600);
                    include 'view/home/index.php';
                }
            } else {
                setcookie('cod_logado', 0, time() - 3600);
                setcookie('esc_logado', 0, time() - 3600);
                include 'view/home/index.php';
            }
        } else {
            setcookie('cod_logado', 0, time() - 3600);
            setcookie('esc_logado', 0, time() - 3600);
            include 'view/home/index.php';
        }
    } else {
        include 'view/home/index.php';
    }
}
?>